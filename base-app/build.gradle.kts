plugins {
    id("com.android.library")
    kotlin("android")
}

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

android {
    compileSdkVersion(Sdk.compile)
    defaultConfig {
        minSdkVersion(Sdk.min)
        targetSdkVersion(Sdk.target)
        versionCode = App.code
        versionName = App.name
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isUseProguard = true
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    (kotlinOptions as org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions).apply {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    kotlin("kotlin-stdlib-jdk7", Versions.kotlin)
    api("org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}")

    api(project(":base"))
    implementation(project(":wallet"))
    implementation(project(":node"))

    implementation("androidx.appcompat:appcompat:${Versions.appcompat}")
    implementation("androidx.fragment:fragment-ktx:${Versions.fragment}")

    api("org.koin:koin-android:${Versions.koin}")
    implementation("ru.terrakok.cicerone:cicerone:${Versions.cicerone}")

    junit
}
