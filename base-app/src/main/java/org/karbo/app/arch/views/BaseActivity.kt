package org.karbo.app.arch.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.arch.KoinFragmentFactory
import org.karbo.app.navigation.Navigator
import org.koin.android.ext.android.getKoin
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.core.qualifier.TypeQualifier
import ru.terrakok.cicerone.NavigatorHolder

abstract class BaseActivity : AppCompatActivity() {
    protected abstract val viewModel: BaseViewModel

    private val navHolder by inject<NavigatorHolder>()

    protected open val containerId = 0
    val scope = getKoin().getOrCreateScope(this::class.java.simpleName, TypeQualifier(this::class))

    override fun onBackPressed() = viewModel.backPressed()

    override fun onCreate(savedInstanceState: Bundle?) {
        supportFragmentManager.fragmentFactory = KoinFragmentFactory
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) viewModel.screenOpened()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navHolder.setNavigator(Navigator(this, containerId))
    }

    override fun onPause() {
        navHolder.removeNavigator()
        super.onPause()
    }

    override fun onDestroy() {
        if (isFinishing) {
            viewModel.destroy()
            scope.close()
        }
        super.onDestroy()
    }

    inline fun <reified T : Any> inject(
        qualifier: Qualifier? = null,
        noinline parameters: ParametersDefinition? = null
    ) = lazy(LazyThreadSafetyMode.NONE) { scope.get<T>(qualifier, parameters) }
}
