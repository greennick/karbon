package org.karbo.app.arch.views

import android.content.Context
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.utils.log
import org.koin.android.ext.android.getKoin
import org.koin.core.qualifier.TypeQualifier
import org.koin.core.scope.Scope

abstract class BaseFragment(layoutId: Int = 0) : Fragment(layoutId) {
    protected abstract val viewModel: BaseViewModel

    protected val scope: Scope = getKoin().getOrCreateScope(this::class.java.simpleName, TypeQualifier(this::class))

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() = viewModel.backPressed()
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) viewModel.screenOpened()
    }

    override fun onDestroy() {
        if (requireActivity().isFinishing || !isStateSaved) {
            viewModel.destroy()
            scope.close()
            log { "ViewModel for ${hashCode()} $tag is cleared" }
        }
        super.onDestroy()
    }
}
