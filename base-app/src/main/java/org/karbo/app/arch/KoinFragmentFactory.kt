package org.karbo.app.arch

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import org.karbo.app.utils.loge
import org.koin.core.KoinComponent

object KoinFragmentFactory : FragmentFactory(), KoinComponent {

    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        val clazz = loadFragmentClass(classLoader, className)
        return try {
            getKoin().get(clazz.kotlin, null, null)
        } catch (e: Exception) {
            loge(e) { "can't get fragment $className from Koin" }
            super.instantiate(classLoader, className)
        }
    }
}
