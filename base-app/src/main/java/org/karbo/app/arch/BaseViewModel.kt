package org.karbo.app.arch

import kotlinx.coroutines.*
import org.karbo.app.navigation.Router

abstract class BaseViewModel(private val router: Router) : CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext = Dispatchers.Main + job

    open fun screenOpened() {}

    open fun backPressed() = router.exit()

    fun destroy() {
        job.cancelChildren()
        onDestroy()
    }

    protected open fun onDestroy() {}
}
