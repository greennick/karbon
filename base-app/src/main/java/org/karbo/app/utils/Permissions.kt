@file:Suppress("unused")

package org.karbo.app.utils

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

private const val readStorage = Manifest.permission.READ_EXTERNAL_STORAGE
private const val writeStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
private const val camera = Manifest.permission.CAMERA

const val REQUEST_CODE_STORAGE = 101
const val REQUEST_CODE_CAMERA = 102

fun Activity.hasPermission(permission: String) =
    ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

fun Activity.shouldShowRationaleFor(permission: String) =
    ActivityCompat.shouldShowRequestPermissionRationale(this, permission)

fun Activity.requestPermission(requestCode: Int, vararg permissions: String) =
    ActivityCompat.requestPermissions(this, permissions, requestCode)

val Activity.hasStoragePermission
    get() = hasPermission(readStorage) && hasPermission(writeStorage)

val Activity.storageDeniedForever
    get() = !shouldShowRationaleFor(readStorage) &&
            !shouldShowRationaleFor(writeStorage)

val Activity.hasCameraPermission get() = hasPermission(camera)

val Activity.cameraDeniedForever get() = !shouldShowRationaleFor(camera)

fun Activity.requestStorage(requestCode: Int = REQUEST_CODE_STORAGE) =
    requestPermission(requestCode, readStorage, writeStorage)

fun Activity.requestCamera(requestCode: Int = REQUEST_CODE_CAMERA) =
    requestPermission(requestCode, camera)
