@file:Suppress("unused")

package org.karbo.app.utils

import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.view.View
import androidx.activity.ComponentActivity
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver

inline fun ComponentActivity.alertDialog(
    dismissOnEvent: Lifecycle.Event? = Lifecycle.Event.ON_DESTROY,
    dsl: AlertDialogBuilder.() -> Unit = {}
): AlertDialog {
    val dialog = AlertDialogBuilder(this)
        .apply(dsl)
        .let(AlertDialogBuilder::build)

    if (dismissOnEvent != null) {
        lifecycle.addObserver(LifecycleEventObserver { _, event ->
            if (dismissOnEvent == event) dialog.dismiss()
        })
    }

    return dialog
}

inline fun Fragment.alertDialog(
    dismissOnEvent: Lifecycle.Event? = Lifecycle.Event.ON_DESTROY,
    dsl: AlertDialogBuilder.() -> Unit = {}
): AlertDialog {
    val dialog = AlertDialogBuilder(requireContext())
        .apply(dsl)
        .let(AlertDialogBuilder::build)

    if (dismissOnEvent != null) {
        viewLifecycleOwner.lifecycle.addObserver(LifecycleEventObserver { _, event ->
            if (dismissOnEvent == event) dialog.dismiss()
        })
    }

    return dialog
}

@Suppress("MemberVisibilityCanBePrivate")
class AlertDialogBuilder(private val context: Context) {
    private val dialogBuilder = AlertDialog.Builder(context)

    var show: Boolean = false

    var cancelable: Boolean = true
        set(value) {
            dialogBuilder.setCancelable(value)
            field = value
        }

    var title: CharSequence = ""
        set(value) {
            dialogBuilder.setTitle(value)
            field = value
        }

    @StringRes
    var titleId: Int = 0
        set(value) {
            if (value == 0) return

            title = context.getString(value)
            field = value
        }

    var titleView: View? = null
        set(value) {
            dialogBuilder.setCustomTitle(value)
            field = value
        }

    var view: View? = null
        set(value) {
            dialogBuilder.setView(value)
            field = value
        }

    var message: CharSequence = ""
        set(value) {
            dialogBuilder.setMessage(value)
        }

    @StringRes
    var messageId: Int = 0
        set(value) {
            if (value == 0) return

            message = context.getString(value)
            field = value
        }

    var icon: Drawable? = null
        set(value) {
            dialogBuilder.setIcon(value)
            field = value
        }

    @DrawableRes
    var iconId: Int = 0
        set(value) {
            if (value == 0) return

            dialogBuilder.setIcon(iconId)
            field = value
        }

    var positive: CharSequence = ""
        set(value) {
            dialogBuilder.setPositiveButton(value, null)
            field = value
        }

    @StringRes
    var positiveId: Int = 0
        set(value) {
            if (value == 0) return

            positive = context.getString(value)
            field = value
        }

    var negative: CharSequence = ""
        set(value) {
            dialogBuilder.setNegativeButton(value, null)
            field = value
        }

    @StringRes
    var negativeId: Int = 0
        set(value) {
            if (value == 0) return

            negative = context.getString(value)
            field = value
        }

    var neutral: CharSequence = ""
        set(value) {
            dialogBuilder.setNeutralButton(value, null)
            field = value
        }

    @StringRes
    var neutralId: Int = 0
        set(value) {
            if (value == 0) return

            neutral = context.getString(value)
            field = value
        }

    fun positiveDialog(text: CharSequence, listener: DialogInterface.() -> Unit) {
        dialogBuilder.setPositiveButton(text) { dialog, _ ->
            listener(dialog)
        }
    }

    fun positiveDialog(@StringRes textId: Int, listener: DialogInterface.() -> Unit) {
        positiveDialog(context.getText(textId), listener)
    }

    fun positive(text: CharSequence, listener: () -> Unit) {
        dialogBuilder.setPositiveButton(text) { _, _ ->
            listener()
        }
    }

    fun positive(@StringRes textId: Int, listener: () -> Unit) {
        positive(context.getText(textId), listener)
    }

    fun negativeDialog(text: CharSequence, listener: DialogInterface.() -> Unit) {
        dialogBuilder.setNegativeButton(text) { dialog, _ ->
            listener(dialog)
        }
    }

    fun negativeDialog(@StringRes textId: Int, listener: DialogInterface.() -> Unit) {
        negativeDialog(context.getText(textId), listener)
    }

    fun negative(text: CharSequence, listener: () -> Unit) {
        dialogBuilder.setNegativeButton(text) { _, _ ->
            listener()
        }
    }

    fun negative(@StringRes textId: Int, listener: () -> Unit) {
        negative(context.getText(textId), listener)
    }

    fun neutralDialog(text: CharSequence, listener: DialogInterface.() -> Unit) {
        dialogBuilder.setNeutralButton(text) { dialog, _ ->
            listener(dialog)
        }
    }

    fun neutralDialog(@StringRes textId: Int, listener: DialogInterface.() -> Unit) {
        neutralDialog(context.getText(textId), listener)
    }

    fun neutral(text: CharSequence, listener: () -> Unit) {
        dialogBuilder.setNeutralButton(text) { _, _ ->
            listener()
        }
    }

    fun neutral(@StringRes textId: Int, listener: () -> Unit) {
        neutral(context.getText(textId), listener)
    }

    infix fun onCancel(onCancel: () -> Unit) {
        dialogBuilder.setOnCancelListener { onCancel() }
    }

    infix fun onDismiss(onDismiss: () -> Unit) {
        dialogBuilder.setOnDismissListener { onDismiss() }
    }

    infix fun onCancelDialog(onCancel: DialogInterface.() -> Unit) {
        dialogBuilder.setOnCancelListener(onCancel)
    }

    infix fun onDismissDialog(onDismiss: DialogInterface.() -> Unit) {
        dialogBuilder.setOnDismissListener(onDismiss)
    }

    fun build(): AlertDialog = dialogBuilder.create()
        .also { if (show) it.show() }
}
