package org.karbo.app.utils

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

fun Context.toast(text: String, duration: Int = Toast.LENGTH_LONG) =
    Toast.makeText(this, text, duration).show()

fun Context.toast(@StringRes textId: Int, duration: Int = Toast.LENGTH_LONG) =
    toast(getString(textId), duration)

fun Fragment.toast(@StringRes textId: Int, duration: Int = Toast.LENGTH_LONG) =
    Toast.makeText(requireContext(), textId, duration).show()
