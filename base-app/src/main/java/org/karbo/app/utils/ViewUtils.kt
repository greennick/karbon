package org.karbo.app.utils

/*
 * Created by Green-Nick on 20.11.2017.
 */

import android.content.Context
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm?.hideSoftInputFromWindow(windowToken, 0)
}

fun Toolbar.navigationClicked(action: (() -> Unit)?) {
    if (action == null) {
        setNavigationOnClickListener(null)
    } else {
        setNavigationOnClickListener { action() }
    }
}

fun Fragment.inflate(@LayoutRes layoutId: Int, root: ViewGroup? = null, attachToRoot: Boolean = false): View =
    requireContext().inflate(layoutId, root, attachToRoot)

fun Context.inflate(@LayoutRes layoutId: Int, root: ViewGroup? = null, attachToRoot: Boolean = false): View =
    LayoutInflater.from(this).inflate(layoutId, root, attachToRoot)

fun inflate(@LayoutRes layoutId: Int, root: ViewGroup, attachToRoot: Boolean = false): View =
    root.context.inflate(layoutId, root, attachToRoot)
