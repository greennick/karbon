package org.karbo.app.utils

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

@ExperimentalTime
fun CoroutineScope.repeatEvery(
    tick: Duration,
    delay: Duration = Duration.ZERO,
    context: CoroutineContext = EmptyCoroutineContext,
    block: suspend CoroutineScope.() -> Unit
) = launch(context) {
    if (delay != Duration.ZERO) {
        delay(delay.toLongMilliseconds())
    }
    while (isActive) {
        block()
        delay(tick.toLongMilliseconds())
    }
}

fun <T> Flow<T>.collectIn(
    scope: CoroutineScope,
    context: CoroutineContext = EmptyCoroutineContext,
    action: suspend (value: T) -> Unit
) = scope.launch(context) {
    collect(action)
}
