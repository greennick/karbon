package org.karbo.app.utils

import android.content.Context
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

@ColorInt
fun Context.color(@ColorRes id: Int) = ContextCompat.getColor(this, id)

@ColorInt
fun Fragment.color(@ColorRes id: Int) = ContextCompat.getColor(requireContext(), id)
