package org.karbo.app.data

import org.karbo.node.models.Node

class ConnectedNodeHolder {
    var node: Node? = null
}
