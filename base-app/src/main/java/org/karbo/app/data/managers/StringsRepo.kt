package org.karbo.app.data.managers

import android.content.Context

interface StringsRepo {

    fun string(id: Int): String

    fun string(id: Int, vararg formatArgs: Any): String

    fun strings(id: Int): List<String>
}

class StringsRepoImpl(private val context: Context) : StringsRepo {
    override fun string(id: Int): String = context.getString(id)

    override fun string(id: Int, vararg formatArgs: Any): String =
        context.getString(id, *formatArgs)

    override fun strings(id: Int): List<String> = context.resources.getStringArray(id).asList()
}
