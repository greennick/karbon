package org.karbo.app.data.managers

class ReceivedLinkHandler {
    private var link: String = ""

    fun saveLink(link: String) {
        this.link = link
    }

    fun consumeSavedLink(): LinkAction = when {
        link.isEmpty() -> LinkAction.Nothing
        link.contains("shortcut/scan_qr") -> LinkAction.ScanQr
        link.contains("shortcut/receive") -> LinkAction.Receive
        link.contains("shortcut/send") -> LinkAction.Send()
        else -> LinkAction.Send(link)
    }.also { link = "" }
}

sealed class LinkAction {
    object Nothing : LinkAction()

    object ScanQr : LinkAction()

    object Receive : LinkAction()

    class Send(val request: String = "") : LinkAction()
}
