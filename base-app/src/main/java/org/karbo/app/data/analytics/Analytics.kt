package org.karbo.app.data.analytics

import org.karbo.node.models.Node

/*
 * Created by greennick on 03.02.2018.
 */
interface Analytics {

    fun connectedToNode(node: Node) {}

    fun restoredByMnemonic() {}

    fun addressReceivedFromOut() {}

    fun addressReceivedByNfc() {}

    fun addressSentByNfc() {}

    fun qrCodeScanned() {}

    fun fundsSent() {}

    fun txHashCopied() {}

    fun paymentIdCopied() {}

    fun faqClicked() {}

    fun donateClicked() {}
}
