package org.karbo.app.data.passwords

class PasswordHolder {
    private var password: CharArray? = null

    fun save(password: String) {
        this.password = password.toCharArray()
    }

    fun check(password: String): Boolean {
        val local: CharArray = this.password ?: return false
        if (password.length != local.size) return false

        local.forEachIndexed { index, char ->
            if (password[index] != char) return false
        }

        return true
    }

    fun reset() {
        password?.fill(' ')
        password = null
    }
}
