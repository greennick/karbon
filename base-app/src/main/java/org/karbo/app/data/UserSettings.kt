package org.karbo.app.data

interface UserSettings {

    fun allowSendingAnalytics(): Boolean

    fun useWalletKiller(): Boolean

    fun useCustomNode(): Boolean

    fun customNodeHost(): String

    fun customNodePort(): String
}
