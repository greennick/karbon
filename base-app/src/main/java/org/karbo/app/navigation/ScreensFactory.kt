package org.karbo.app.navigation

import org.karbo.wallet.models.Transaction
import org.karbo.wallet.models.WalletFile
import ru.terrakok.cicerone.Screen

interface ScreensFactory {

    fun launch(): Screen

    fun login(): Screen

    fun walletCreation(): Screen

    fun loading(wallet: WalletFile, password: String, seed: String = ""): Screen

    fun main(wallet: WalletFile): Screen

    fun mnemonicSeed(firstOpening: Boolean = false): Screen

    fun exportWallet(wallet: WalletFile): Screen

    fun settings(): Screen

    fun qrScanner(): Screen

    fun receiving(): Screen

    fun sending(sendAddress: String = ""): Screen

    fun shareText(subject: String, text: String): Screen

    fun applicationSettings(): Screen

    fun faq(): Screen

    fun email(): Screen

    fun transactionDetails(transaction: Transaction): Screen
}
