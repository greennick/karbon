package org.karbo.app.navigation

import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Screen

class Router : Router() {

    fun showMessage(stringId: Int, duration: Duration = Duration.LONG) {
        executeCommands(ShowMessageId(stringId, duration))
    }

    fun showMessage(message: String, duration: Duration = Duration.LONG) {
        executeCommands(ShowMessage(message, duration))
    }

    fun showChooser(title: String, screen: Screen) {
        executeCommands(OpenChooser(title, screen))
    }

    fun showDialog(screen: Screen) {
        executeCommands(ShowDialog(screen))
    }
}
