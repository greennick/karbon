package org.karbo.app.navigation

import android.content.Intent
import android.widget.Toast
import androidx.fragment.app.*
import org.karbo.app.utils.toast
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

class Navigator(
    activity: FragmentActivity,
    container: Int
) : SupportAppNavigator(activity, container) {

    override fun applyCommand(command: Command) {
        when (command) {
            is ShowMessage -> showToast(command)
            is ShowMessageId -> showToast(command)
            is OpenChooser -> showChooser(command)
            is ShowDialog -> showDialog(command)
            else -> super.applyCommand(command)
        }
    }

    private fun showToast(command: ShowMessage) {
        val duration = if (command.duration == Duration.LONG) {
            Toast.LENGTH_LONG
        } else {
            Toast.LENGTH_SHORT
        }
        activity.toast(command.message, duration)
    }

    private fun showToast(command: ShowMessageId) {
        val duration = if (command.duration == Duration.LONG) {
            Toast.LENGTH_LONG
        } else {
            Toast.LENGTH_SHORT
        }
        activity.toast(command.messageId, duration)
    }

    private fun showChooser(command: OpenChooser) {
        val screen = command.screen as SupportAppScreen
        val chooser = Intent.createChooser(
            screen.getActivityIntent(activity),
            command.title
        )
        if (chooser.resolveActivity(activity.packageManager) != null) {
            activity.startActivity(chooser)
        }
    }

    private fun showDialog(command: ShowDialog) {
        val dialogFragment = (command.screen as SupportAppScreen).fragment as? DialogFragment

        dialogFragment?.show(fragmentManager, dialogFragment.tag)
    }

    override fun setupFragmentTransaction(
        command: Command,
        currentFragment: Fragment?,
        nextFragment: Fragment?,
        fragmentTransaction: FragmentTransaction
    ) {
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
    }
}
