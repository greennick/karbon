package org.karbo.app.navigation

import ru.terrakok.cicerone.Screen
import ru.terrakok.cicerone.commands.Command

class ShowMessage(val message: String, val duration: Duration) : Command

class ShowMessageId(val messageId: Int, val duration: Duration) : Command

enum class Duration {
    SHORT, LONG
}

class OpenChooser(val title: String, val screen: Screen) : Command

class ShowDialog(val screen: Screen) : Command
