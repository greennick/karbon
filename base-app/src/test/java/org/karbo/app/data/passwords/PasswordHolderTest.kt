package org.karbovanets.karbon.data.passwords

import org.junit.Test
import org.karbo.app.data.passwords.PasswordHolder

class PasswordHolderTest {

    @Test
    fun `checking without saving`() {
        val password = "athtj-2n35v0qifnv"
        val holder = PasswordHolder()
        assertFalse(holder.check(password))
    }

    @Test
    fun `checking correct password`() {
        val password = "phj3g0fhnnv"

        with(PasswordHolder()) {
            save(password)
            assert(check(password))
        }
    }

    @Test
    fun `checking wrong password`() {
        val password = "v2bnsrbd-2nv02ifnv"
        val wrong = "0we-94hvs-df34fsd"

        with(PasswordHolder()) {
            save(password)
            assertFalse(check(wrong))
        }
    }

    @Test
    fun `checking empty input`() {
        val password = "vbnowdgjh,t564ifnv"

        with(PasswordHolder()) {
            save(password)
            assertFalse(check(""))
        }
    }

    @Test
    fun `checking correct password after resetting`() {
        val password = "vbnow3850-2nv02ifnv"

        with(PasswordHolder()) {
            save(password)
            reset()

            assertFalse(check(password))
        }
    }

    @Test
    fun `checking wrong password after resetting`() {
        val password = "vbnxdgbs453nv"
        val wrong = "0d5h6e3fgdf"

        with(PasswordHolder()) {
            save(password)
            reset()

            assertFalse(check(wrong))
        }
    }

    @Test
    fun `checking empty input after resetting`() {
        val password = "vbnsfdbsatdn342ifnv"

        with(PasswordHolder()) {
            save(password)
            assertFalse(check(""))
        }
    }

    private fun assertFalse(value: Boolean) = assert(!value)
}