package org.karbo.app.utils

import org.karbo.base.Logger

class ReleaseLogger : Logger {

    override fun log(message: String) {
        println("Karbo wallet: $message")
    }
}

@Suppress("UNUSED_PARAMETER")
inline fun log(tag: String? = null, message: () -> Any?) {
}

@Suppress("UNUSED_PARAMETER")
inline fun loge(tag: String? = null, message: () -> Any?) {
}

@Suppress("UNUSED_PARAMETER")
inline fun loge(error: Throwable, message: () -> Any?) {
}
