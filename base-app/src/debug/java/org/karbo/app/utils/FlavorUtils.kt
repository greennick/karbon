package org.karbo.app.utils

inline fun doIfDebug(action: () -> Unit) {
    action()
}
