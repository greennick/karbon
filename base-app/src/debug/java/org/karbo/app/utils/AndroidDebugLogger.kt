package org.karbo.app.utils

import android.os.Build
import android.util.Log
import org.karbo.base.Logger
import java.util.regex.Pattern

private const val MAX_TAG_LENGTH = 23
private const val CALL_STACK_INDEX = 3
private const val INLINE_CALL_STACK_INDEX = 2
private val ANONYMOUS_CLASS = Pattern.compile("(\\$\\d+)+$")

class AndroidDebugLogger : Logger {

    override fun log(message: String) {
        Log.i(tag, message)
    }

    override fun d(message: String) {
        Log.d(tag, message)
    }

    override fun e(message: String) {
        Log.e(tag, message)
    }

    override fun e(message: String, error: Throwable) {
        Log.e(tag, message, error)
    }

    private val tag: String
        get() {
            val stackTraceTag = createStackElementTag()
            // Tag length limit was removed in API 24.
            return if (stackTraceTag.length <= MAX_TAG_LENGTH || Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                stackTraceTag
            } else {
                stackTraceTag.substring(0, MAX_TAG_LENGTH)
            }
        }

    private fun createStackElementTag(): String {
        val stackTrace = Throwable().stackTrace
        check(stackTrace.size > CALL_STACK_INDEX) { "Synthetic stacktrace didn't have enough elements: are you using proguard?" }
        val element = stackTrace[CALL_STACK_INDEX]
        var tag = "${element.className.substringAfterLast('.')}.${element.methodName}"
        val m = ANONYMOUS_CLASS.matcher(tag)
        if (m.find()) {
            tag = m.replaceAll("")
        }
        return tag
    }
}

inline fun log(tag: String? = null, message: () -> Any?) {
    Log.d(tag ?: inlineTag, message().toString())
}

inline fun loge(tag: String? = null, message: () -> Any?) {
    Log.e(tag ?: inlineTag, message().toString())
}

inline fun loge(error: Throwable, message: () -> Any?) {
    Log.e(inlineTag, message().toString(), error)
}

val inlineTag: String
    get() {
        val stackTraceTag = createInlineStackElementTag()
        // Tag length limit was removed in API 24.
        return if (stackTraceTag.length <= MAX_TAG_LENGTH || Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stackTraceTag
        } else {
            stackTraceTag.substring(0, MAX_TAG_LENGTH)
        }
    }

fun createInlineStackElementTag(): String {
    val stackTrace = Throwable().stackTrace
    check(stackTrace.size > INLINE_CALL_STACK_INDEX) { "Synthetic stacktrace didn't have enough elements: are you using proguard?" }
    val element = stackTrace[INLINE_CALL_STACK_INDEX]
    var tag = "${element.className.substringAfterLast('.')}.${element.methodName}"
    val m = ANONYMOUS_CLASS.matcher(tag)
    if (m.find()) {
        tag = m.replaceAll("")
    }
    return tag
}
