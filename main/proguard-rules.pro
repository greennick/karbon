#Crashlytics will still function without this rule, but your crash reports will not include proper file names or line numbers.
-keepattributes SourceFile,LineNumberTable
#If you are using custom exceptions, add this line so that custom exception types are skipped during obfuscation
-keep public class * extends java.lang.Exception

-keepnames class org.karbovanets.karbon.screens.** extends  org.karbo.app.arch.views.BaseFragment  { *; }

-keep public class androidx.appcompat.widget.* extends android.view.View { *; }
