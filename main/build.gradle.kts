plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
}

repositories {
    mavenCentral()
    maven("https://raw.github.com/embarkmobile/zxing-android-minimal/mvn-repo/maven-repository/")
    maven("https://jitpack.io")
}

android {
    compileSdkVersion(Sdk.compile)
    defaultConfig {
        minSdkVersion(Sdk.min)
        targetSdkVersion(Sdk.target)
        versionCode = App.code
        versionName = App.name
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isUseProguard = true
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    (kotlinOptions as org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions).apply {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

androidExtensions {
    isExperimental = true
}

dependencies {
    kotlin("kotlin-stdlib-jdk7", Versions.kotlin)
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}")

    implementation(project(":base-app"))
    implementation(project(":node"))
    implementation(project(":wallet"))

    implementation("com.google.android.material:material:${Versions.material}")
    implementation("androidx.appcompat:appcompat:${Versions.appcompat}")
    implementation("androidx.constraintlayout:constraintlayout:${Versions.constraint}")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")

    implementation("com.google.zxing:core:3.3.0")
    implementation("me.dm7.barcodescanner:zxing:1.9.8")

    implementation("ru.terrakok.cicerone:cicerone:${Versions.cicerone}")

    implementation("com.github.green-nick:properties:${Versions.properties}")
    implementation("com.github.green-nick.properties-android:binds:${Versions.propertiesAndr}")
    implementation("com.github.green-nick.properties-android:bindsx:${Versions.propertiesAndr}")

    mockito
    mockitoInline
    mockitoKotlin
    junit
    koinTest
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.3.2")
}
