package org.karbo.main

fun assertFalse(value: Boolean) = assert(!value)

fun assertNull(value: Any?) = assert(value == null)

fun assertNotNull(value: Any?) = assert(value != null)

fun assertEquals(one: Any?, another: Any?) = assert(one == another)

fun assertNotEquals(one: Any?, another: Any?) = assert(one != another)
