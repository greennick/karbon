package org.karbo.main.data

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test
import org.karbo.base.KRBSHIS_IN_KRB
import java.text.DecimalFormat

/**
 * https://trello.com/c/32QMpvKo
 *
 */
class CurrencyUtilsKtTest {

    @Test
    fun amountToStringFormat() {

        val desktopAmount = "2347.63498839"
        val mobileAmount = "2347.6349883899998"

        val parsed: String = desktopAmount.normalizeAmount()
        println("parsed:$parsed")
        val krb = parsed.toDouble().toKrbshi()
        println("krb:$krb")
        val stringAmount: String = krb.toAmount()
        println("stringAmount:$stringAmount")
        assertEquals(stringAmount, desktopAmount)
        assertNotEquals(stringAmount, mobileAmount)
    }

    private fun Long.toAmount(): String = DecimalFormat("0.00##########")
        .format(this * 0.000_000_000_001)
        .replace(',', '.')

    private fun Double.toKrbshi(): Long {
        return (KRBSHIS_IN_KRB * this).toLong()
    }
}
