package org.karbo.main.data

import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

const val SCHEME = "karbowanec"
const val ADDRESS =
    "KfuR9HxeQanAPSDpXDK8PuWWgXq3mdtypFkc3bMxjc2dJ4WpJ1Xw8DyVweuyWoZbucbTBSEejsTo1XSqgq7XN6EUG8RQSKH"

const val PARAM_AMOUNT = "amount"
const val EMPTY_AMOUNT = ""
const val ZERO_AMOUNT = "0"
const val SOME_AMOUNT = "1.04"

const val PARAM_PAYMENT_ID = "payment_id"
const val EMPTY_PAYMENT_ID = ""
const val SOME_PAYMENT_ID = "A3C8B9BE60494B7FC93E6D363FF802AE783E1F6886A14F76CA7EF27DED278D0B"

const val LONG_TEXT =
    """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ornare quam viverra orci sagittis. Pharetra massa massa ultricies mi quis hendrerit dolor magna eget. Eu volutpat odio facilisis mauris. Turpis in eu mi bibendum neque egestas congue quisque egestas. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Felis donec et odio pellentesque diam volutpat commodo. Purus in massa tempor nec feugiat nisl pretium fusce. Nam libero justo laoreet sit. Arcu felis bibendum ut tristique et egestas. Malesuada proin libero nunc consequat interdum varius sit amet mattis. Consequat ac felis donec et odio pellentesque diam volutpat. Quam adipiscing vitae proin sagittis.

Ornare arcu odio ut sem nulla. In aliquam sem fringilla ut morbi tincidunt augue interdum. Ipsum dolor sit amet consectetur adipiscing. Pretium lectus quam id leo. Auctor eu augue ut lectus arcu bibendum. Integer quis auctor elit sed vulputate mi sit. Sem nulla pharetra diam sit. Sed vulputate odio ut enim blandit volutpat maecenas. Volutpat commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend. Elementum sagittis vitae et leo. Scelerisque felis imperdiet proin fermentum leo vel orci porta. Netus et malesuada fames ac turpis egestas sed tempus urna. Accumsan lacus vel facilisis volutpat est.

Vel risus commodo viverra maecenas accumsan lacus vel. Arcu risus quis varius quam quisque id diam vel. Faucibus purus in massa tempor nec feugiat nisl pretium fusce. Mattis aliquam faucibus purus in massa tempor nec. Lorem dolor sed viverra ipsum nunc aliquet bibendum. Porttitor leo a diam sollicitudin tempor id eu nisl nunc. Egestas diam in arcu cursus. Platea dictumst vestibulum rhoncus est pellentesque. Maecenas sed enim ut sem viverra. Cursus euismod quis viverra nibh cras pulvinar. Ut tortor pretium viverra suspendisse. Viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat.

Lacus suspendisse faucibus interdum posuere. Cras adipiscing enim eu turpis egestas pretium aenean pharetra magna. Elit duis tristique sollicitudin nibh sit amet commodo nulla. Etiam erat velit scelerisque in dictum non consectetur. Molestie nunc non blandit massa enim. Elementum facilisis leo vel fringilla est. Augue neque gravida in fermentum et sollicitudin ac orci. Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque. Ipsum dolor sit amet consectetur adipiscing. Faucibus interdum posuere lorem ipsum dolor. Non nisi est sit amet facilisis magna etiam tempor. Massa tempor nec feugiat nisl. Enim neque volutpat ac tincidunt. Hac habitasse platea dictumst quisque sagittis purus.

Ullamcorper eget nulla facilisi etiam. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. Morbi enim nunc faucibus a pellentesque sit amet. Id faucibus nisl tincidunt eget nullam. Blandit cursus risus at ultrices mi tempus imperdiet nulla malesuada. A arcu cursus vitae congue mauris rhoncus aenean. Ipsum consequat nisl vel pretium lectus quam. Dui nunc mattis enim ut tellus elementum sagittis. Tellus in metus vulputate eu scelerisque felis imperdiet proin. Faucibus scelerisque eleifend donec pretium vulputate. Mauris augue neque gravida in fermentum et sollicitudin ac orci. Sem integer vitae justo eget magna. Quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit. Placerat vestibulum lectus mauris ultrices eros in. Ut tristique et egestas quis."""

class SerializationTests {

    @Test
    fun `address only`() {
        val request = KrbRequest(ADDRESS).createRequest()

        println(request)
        Assert.assertEquals(request, "$SCHEME:$ADDRESS")
    }

    @Test
    fun `empty amount`() {
        val request = KrbRequest(ADDRESS, amount = EMPTY_AMOUNT).createRequest()

        println(request)
        Assert.assertEquals(request, "$SCHEME:$ADDRESS")
    }

    @Test
    fun `zero amount`() {
        val request = KrbRequest(ADDRESS, amount = ZERO_AMOUNT).createRequest()

        println(request)
        Assert.assertEquals(request, "$SCHEME:$ADDRESS?$PARAM_AMOUNT=$ZERO_AMOUNT")
    }

    @Test
    fun `some amount`() {
        val request = KrbRequest(ADDRESS, amount = SOME_AMOUNT).createRequest()

        println(request)
        Assert.assertEquals(request, "$SCHEME:$ADDRESS?$PARAM_AMOUNT=$SOME_AMOUNT")
    }

    @Test
    fun `empty payment Id`() {
        val request = KrbRequest(ADDRESS, paymentId = EMPTY_PAYMENT_ID).createRequest()

        println(request)
        Assert.assertEquals(request, "$SCHEME:$ADDRESS")
    }

    @Test
    fun `some payment Id`() {
        val request = KrbRequest(ADDRESS, paymentId = SOME_PAYMENT_ID).createRequest()

        println(request)
        Assert.assertEquals(request, "$SCHEME:$ADDRESS?$PARAM_PAYMENT_ID=$SOME_PAYMENT_ID")
    }

    @Test
    fun `some amount with some payment id`() {
        val request =
            KrbRequest(ADDRESS, paymentId = SOME_PAYMENT_ID, amount = SOME_AMOUNT).createRequest()

        println(request)
        Assert.assertEquals(
            request,
            "$SCHEME:$ADDRESS?$PARAM_AMOUNT=$SOME_AMOUNT&$PARAM_PAYMENT_ID=$SOME_PAYMENT_ID"
        )
    }

    @Test
    fun `some amount with empty payment id`() {
        val request =
            KrbRequest(ADDRESS, paymentId = EMPTY_PAYMENT_ID, amount = SOME_AMOUNT).createRequest()

        println(request)
        Assert.assertEquals(request, "$SCHEME:$ADDRESS?$PARAM_AMOUNT=$SOME_AMOUNT")
    }

    @Test
    fun `zero amount with some payment id`() {
        val request =
            KrbRequest(ADDRESS, paymentId = SOME_PAYMENT_ID, amount = ZERO_AMOUNT).createRequest()

        println(request)
        Assert.assertEquals(
            request,
            "$SCHEME:$ADDRESS?$PARAM_AMOUNT=$ZERO_AMOUNT&$PARAM_PAYMENT_ID=$SOME_PAYMENT_ID"
        )
    }
}

class ValidationTests {

    @Test
    fun `right address`() = runBlocking {
        Assert.assertTrue(ADDRESS.hasAddress())
    }

    @Test
    fun `is address? (right)`() {
        Assert.assertTrue(ADDRESS.isAddress())
    }

    @Test
    fun `is address? (wrong)`() {
        val address = " $ADDRESS."
        Assert.assertFalse(address.isAddress())
    }

    @Test
    fun `few addresses`() = runBlocking {
        val raw = "\n$ADDRESS $ADDRESS \n $ADDRESS"
        Assert.assertTrue(raw.hasAddress())
    }

    @Test
    fun `not just right address`() = runBlocking {
        val raw = "Hello, I'm Petro. This is my KRB address:$ADDRESS. Send me sth"
        Assert.assertTrue(raw.hasAddress())
    }

    @Test
    fun `wrong address`() = runBlocking {
        val raw = ADDRESS.replace('j', 'I')
        Assert.assertFalse(raw.hasAddress())
    }

    @Test
    fun `not just wrong address`() = runBlocking {
        val wrongAddress = ADDRESS.replace('j', 'I')
        val raw = "Hello, I'm Petro. This is my KRB address:$wrongAddress. Send me sth"
        Assert.assertFalse(raw.hasAddress())
    }

    @Test
    fun `txt dns record with address`() = runBlocking {
        val txtrecord =
            "oa1:krb recipient_address=Kdev1L9V5ow3cdKNqDpLcFFxZCqu5W2GE9xMKewsB2pUXWxcXvJaUWHcSrHuZw91eYfQFzRtGfTemReSSMN4kE445i6Etb3; recipient_name=Karbowanec Development; tx_description=Donate to development fund!;"
        Assert.assertTrue(txtrecord.hasAddress())
    }

    @Test
    fun `right payment request with address only`() = runBlocking {
        val request = "$SCHEME:$ADDRESS"
        Assert.assertTrue(request.hasPaymentRequest())
    }

    @Test
    fun `not just right payment request with address only`() = runBlocking {
        val request = "Hello, I'm Petro. This is my request $SCHEME:$ADDRESS, send me sth"
        Assert.assertTrue(request.hasPaymentRequest())
    }

    @Test
    fun `right payment request with address and amount`() = runBlocking {
        val request = "$SCHEME:$ADDRESS?$PARAM_AMOUNT=$SOME_AMOUNT"
        Assert.assertTrue(request.hasPaymentRequest())
    }

    @Test
    fun `not just right payment request with params`() = runBlocking {
        val request =
            "Hello, I'm Petro. This is my request $SCHEME:$ADDRESS?$PARAM_AMOUNT=$SOME_AMOUNT, send me sth"
        Assert.assertTrue(request.hasPaymentRequest())
    }

    @Test
    fun `wrong payment request with address only`() = runBlocking {
        val wrongAddress = ADDRESS.replace('j', 'I')
        val request = "$SCHEME:$wrongAddress"
        Assert.assertFalse(request.hasPaymentRequest())
    }

    @Test
    fun `not just wrong payment request with address only`() = runBlocking {
        val wrongAddress = ADDRESS.replace('j', 'I')
        val request = "Hello, I'm Petro. This is my request $SCHEME:$wrongAddress, send me sth"
        Assert.assertFalse(request.hasPaymentRequest())
    }

    @Test
    fun `wrong payment request with address and amount`() = runBlocking {
        val wrongAddress = ADDRESS.replace('j', 'I')
        val request = "$SCHEME:$wrongAddress?$PARAM_AMOUNT=$SOME_AMOUNT"
        Assert.assertFalse(request.hasPaymentRequest())
    }

    @Test
    fun `not just wrong payment request with address and amount`() = runBlocking {
        val wrongAddress = ADDRESS.replace('j', 'I')
        val request =
            "Hello, I'm Petro. This is my request $SCHEME:$wrongAddress?$PARAM_AMOUNT=$SOME_AMOUNT, send me sth"
        Assert.assertFalse(request.hasPaymentRequest())
    }

    /**
     * We have troubles parsing long texts
     */
    @Test
    fun `long text on address`() {
//        Assert.assertTrue(("$LONG_TEXT $ADDRESS").hasAddress())
    }

    /**
     * We have troubles parsing long texts
     */
    @Test
    fun `long text on request`() {
//        Assert.assertTrue(("$LONG_TEXT $SCHEME:$ADDRESS").hasPaymentRequest())
    }
}

class ParsingTests {

    @Test
    fun `parse just address`() = runBlocking {
        val address = ADDRESS
        println("raw address string: [$address]")
        println("has address? ${address.hasAddress()}")
        val parsed = address.parseAddress()
        println("parsed: [$parsed]")
        Assert.assertEquals(parsed, ADDRESS)
    }

    @Test
    fun `parse address in request`() = runBlocking {
        val request = "$SCHEME:$ADDRESS"
        println("raw request string: [$request]")
        println("has address? ${request.hasAddress()}")
        val parsed = request.parseAddress()
        println("parsed: [$parsed]")
        Assert.assertEquals(parsed, ADDRESS)
    }

    @Test
    fun `parse not just address`() = runBlocking {
        val address = "blablabla /.vs/fbl$ADDRESS sdvfbvdlj"
        println("raw address string: [$address]")
        println("has address? ${address.hasAddress()}")
        val parsed = address.parseAddress()
        println("parsed: [$parsed]")
        Assert.assertEquals(parsed, ADDRESS)
    }

    @Test
    fun `parse few addresses`() = runBlocking {
        val address = "$ADDRESS $ADDRESS \n$ADDRESS"
        println("raw address string: [$address]")
        println("has address? ${address.hasAddress()}")
        val parsed = address.parseAddress()
        println("parsed: [$parsed]")
        Assert.assertEquals(parsed, ADDRESS)
    }

    @Test
    fun `parse txtrecord with address`() = runBlocking {
        val txtrecord =
            "oa1:krb recipient_address=Kdev1L9V5ow3cdKNqDpLcFFxZCqu5W2GE9xMKewsB2pUXWxcXvJaUWHcSrHuZw91eYfQFzRtGfTemReSSMN4kE445i6Etb3; recipient_name=Karbowanec Development; tx_description=Donate to development fund!;"
        println("raw address string: [$txtrecord]")
        println("has address? ${txtrecord.hasAddress()}")
        val parsed = txtrecord.parseAddress()
        println("parsed: [$parsed]")
        Assert.assertEquals(
            parsed,
            "Kdev1L9V5ow3cdKNqDpLcFFxZCqu5W2GE9xMKewsB2pUXWxcXvJaUWHcSrHuZw91eYfQFzRtGfTemReSSMN4kE445i6Etb3"
        )
    }

    @Test
    fun `parse request`() = runBlocking {
        val request = "$SCHEME:$ADDRESS"
        println("raw request string: [$request]")
        println("has request? ${request.hasPaymentRequest()}")
        Assert.assertEquals(request.parsePaymentRequest(), KrbRequest(ADDRESS))
    }

    @Test
    fun `parse few requests`() = runBlocking {
        val request = "$SCHEME:$ADDRESS $SCHEME:$ADDRESS \n$SCHEME:$ADDRESS"
        println("raw request string: [$request]")
        println("has request? ${request.hasPaymentRequest()}")
        Assert.assertEquals(request.parsePaymentRequest(), KrbRequest(ADDRESS))
    }

    @Test
    fun `parse not just request`() = runBlocking {
        val request = "there is example of$SCHEME:$ADDRESS, testing \nit"
        println("raw request string: [$request]")
        println("has request? ${request.hasPaymentRequest()}")
        Assert.assertEquals(request.parsePaymentRequest(), KrbRequest(ADDRESS))
    }

    @Test
    fun `parse request with params`() = runBlocking {
        val request =
            "$SCHEME:$ADDRESS?$PARAM_AMOUNT=$SOME_AMOUNT&$PARAM_PAYMENT_ID=$SOME_PAYMENT_ID"
        println("raw request string: [$request]")
        println("has request? ${request.hasPaymentRequest()}")
        Assert.assertEquals(
            request.parsePaymentRequest(),
            KrbRequest(ADDRESS, SOME_AMOUNT, SOME_PAYMENT_ID)
        )
    }

    @Test
    fun `parse not just request with params`() = runBlocking {
        val request =
            "it's testing string for $SCHEME:$ADDRESS?$PARAM_AMOUNT=$SOME_AMOUNT&$PARAM_PAYMENT_ID=$SOME_PAYMENT_ID, for paying somebody"
        println("raw request string: [$request]")
        println("has request? ${request.hasPaymentRequest()}")
        Assert.assertEquals(
            request.parsePaymentRequest(),
            KrbRequest(ADDRESS, SOME_AMOUNT, SOME_PAYMENT_ID)
        )
    }

    @Test
    fun `parse request with param`() = runBlocking {
        val request = "$SCHEME:$ADDRESS?$PARAM_PAYMENT_ID=$SOME_PAYMENT_ID"
        println("raw request string: [$request]")
        println("has request? ${request.hasPaymentRequest()}")
        Assert.assertEquals(
            request.parsePaymentRequest(),
            KrbRequest(ADDRESS, paymentId = SOME_PAYMENT_ID)
        )
    }

    @Test
    fun `parse not just request with param`() = runBlocking {
        val request =
            "it's testing string for $SCHEME:$ADDRESS?$PARAM_PAYMENT_ID=$SOME_PAYMENT_ID, for paying somebody"
        println("raw request string: [$request]")
        println("has request? ${request.hasPaymentRequest()}")
        Assert.assertEquals(
            request.parsePaymentRequest(),
            KrbRequest(ADDRESS, paymentId = SOME_PAYMENT_ID)
        )
    }
}