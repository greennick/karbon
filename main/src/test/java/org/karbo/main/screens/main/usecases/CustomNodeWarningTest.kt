package org.karbo.main.screens.main.usecases

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.karbo.node.SavedNodeAddressProvider
import org.karbo.node.models.Node

class CustomNodeWarningTest {

    @Test
    fun `returns false when custom node isn't being used`() {
        val provider = mock<SavedNodeAddressProvider> {
            onBlocking { useCustomNode() } doReturn false
        }
        runBlocking {
            assert(!provider.shouldShowCustomNodeWarning(Node("")))
        }
    }

    @Test
    fun `returns false when connected node the same as custom`() {
        val customNodeUrl = "http://some.node.url"
        val provider = mock<SavedNodeAddressProvider> {
            onBlocking { useCustomNode() } doReturn true
            onBlocking { getCustomNodeAddress() } doReturn customNodeUrl
        }
        runBlocking {
            assert(!provider.shouldShowCustomNodeWarning(Node(customNodeUrl)))
        }
    }

    @Test
    fun `returns true when connected node is another then custom`() {
        val provider = mock<SavedNodeAddressProvider> {
            onBlocking { useCustomNode() } doReturn true
            onBlocking { getCustomNodeAddress() } doReturn "http://custom.node.url"
        }
        runBlocking {
            assert(provider.shouldShowCustomNodeWarning(Node("http://another.node.url")))
        }
    }
}
