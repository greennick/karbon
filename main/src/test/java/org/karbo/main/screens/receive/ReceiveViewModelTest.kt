package org.karbo.main.screens.receive

import com.greennick.result.Result
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.karbo.app.navigation.Router
import org.karbo.main.assertEquals
import org.karbo.main.data.KrbRequest
import org.karbo.main.waitForFinish
import java.util.concurrent.Executors

@ExperimentalCoroutinesApi
class ReceiveViewModelTest {
    private val testAddress =
        "KfuR9HxeQanAPSDpXDK8PuWWgXq3mdtypFkc3bMxjc2dJ4WpJ1Xw8DyVweuyWoZbucbTBSEejsTo1XSqgq7XN6EUG8RQSKH"
    private val vm
        get() = ReceiveViewModel(
            router = Router(),
            analytics = mock(),
            clipboardManager = mock(),
            screens = mock(),
            wallet = mock {
                onBlocking { getAddress() } doReturn Result.success(testAddress)
            }
        )

    private val mainThreadSurrogate = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun `address loading at startup`() = with(vm) {
        waitForFinish()
        assertEquals(address.value, testAddress)
    }

    @Test
    fun `qr value after loading`() = with(vm) {
        waitForFinish()
        assertEquals(qr.value, testAddress)
    }

    @Test
    fun `qr value after changing amount`() = with(vm) {
        val testAmount = "362959.235"
        val request = KrbRequest(testAddress, testAmount).createRequest()

        amount.value = testAmount
        waitForFinish()
        assertEquals(qr.value, request)
    }

    @Test
    fun `qr value after changing amount and clearing it back`() = with(vm) {
        amount.value = "498.235"
        amount.value = ""
        waitForFinish()
        assertEquals(qr.value, testAddress)
    }

    @Test
    fun `qr value after changing payment id`() = with(vm) {
        generatePaymentIdClicked()
        val paymentId = paymentId.value
        val request = KrbRequest(testAddress, paymentId = paymentId).createRequest()

        waitForFinish()
        assertEquals(qr.value, request)
    }

    @Test
    fun `qr value after changing payment id and clearing it back`() = with(vm) {
        generatePaymentIdClicked()
        paymentId.value = ""
        waitForFinish()
        assertEquals(qr.value, testAddress)
    }

    @Test
    fun `qr value after changing amount and paymentId`() = with(vm) {
        val testAmount = "2034.3695"
        amount.value = testAmount

        generatePaymentIdClicked()
        val paymentId = paymentId.value

        val request = KrbRequest(testAddress, testAmount, paymentId).createRequest()

        waitForFinish()
        assertEquals(qr.value, request)
    }

    @Test
    fun `qr value after changing amount and paymentId and clearing them back`() = with(vm) {
        amount.value = "168.235"
        generatePaymentIdClicked()

        amount.value = ""
        paymentId.value = ""
        waitForFinish()
        assertEquals(qr.value, testAddress)
    }

    @Test
    fun `nfc request after loading`() = with(vm) {
        val request = KrbRequest(testAddress).createRequest()
        waitForFinish()
        assertEquals(nfc.value, request)
    }

    @Test
    fun `nfc request after changing amount`() = with(vm) {
        val testAmount = "362959.235"
        val request = KrbRequest(testAddress, testAmount).createRequest()

        amount.value = testAmount
        waitForFinish()
        assertEquals(nfc.value, request)
    }

    @Test
    fun `nfc request value after changing amount and clearing it back`() = with(vm) {
        val request = KrbRequest(testAddress).createRequest()

        amount.value = "498.235"
        amount.value = ""
        waitForFinish()
        assertEquals(nfc.value, request)
    }

    @Test
    fun `nfc request after changing payment id`() = with(vm) {
        generatePaymentIdClicked()
        val paymentId = paymentId.value
        val request = KrbRequest(testAddress, paymentId = paymentId).createRequest()

        waitForFinish()
        assertEquals(nfc.value, request)
    }

    @Test
    fun `nfc request after changing payment id and clearing it back`() = with(vm) {
        val request = KrbRequest(testAddress).createRequest()

        generatePaymentIdClicked()
        paymentId.value = ""
        waitForFinish()
        assertEquals(nfc.value, request)
    }

    @Test
    fun `nfc request after changing amount and paymentId`() = with(vm) {
        val testAmount = "2034.3695"
        amount.value = testAmount

        generatePaymentIdClicked()
        val paymentId = paymentId.value

        val request = KrbRequest(testAddress, testAmount, paymentId).createRequest()

        waitForFinish()
        assertEquals(nfc.value, request)
    }

    @Test
    fun `nfc request after changing amount and paymentId and clearing them back`() = with(vm) {
        val request = KrbRequest(testAddress).createRequest()

        amount.value = "168.235"
        generatePaymentIdClicked()

        amount.value = ""
        paymentId.value = ""
        waitForFinish()
        assertEquals(nfc.value, request)
    }
}
