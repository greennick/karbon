package org.karbo.main.screens.main.usecases

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.karbo.wallet.models.WalletState
import kotlin.random.Random

@ExperimentalCoroutinesApi
class WalletStateToProgressMapperTest {

    @Test
    fun `loaded state returns 100% progress`() {
        val progresses = flowOf(WalletState.Loaded).asProgress(0)
        runBlocking {
            assert(progresses.first() == 100)
        }
    }

    @Test
    fun `loaded at 0 returns 0% progress`() {
        val progresses = flowOf(WalletState.Loading(0)).asProgress(0)
        runBlocking {
            assert(progresses.first() == 0)
        }
    }

    @Test
    fun `any loaded returns 0% progress if height is 0`() {
        val progresses = flowOf(WalletState.Loading(Random.nextInt())).asProgress(0)
        runBlocking {
            assert(progresses.first() == 0)
        }
    }

    @Test
    fun `normal progress`() {
        val progresses = flowOf(
            WalletState.Loading(0),
            WalletState.Loading(10),
            WalletState.Loading(20),
            WalletState.Loading(30),
            WalletState.Loading(40),
            WalletState.Loading(50)
        ).asProgress(100)

        runBlocking {
            progresses.collectIndexed { index, value -> // 0 -> 0; 1 -> 10; 2 -> 20; etc.
                assert(value == index * 10)
            }
        }
    }

    @Test
    fun `progress with shift`() {
        val progresses = flowOf(
            WalletState.Loading(100),
            WalletState.Loading(110),
            WalletState.Loading(120),
            WalletState.Loading(130),
            WalletState.Loading(140),
            WalletState.Loading(150)
        ).asProgress(200)

        runBlocking {
            progresses.collectIndexed { index, value -> // 0 -> 0; 1 -> 10; 2 -> 20; etc.
                assert(value == (index * 10))
            }
        }
    }

    @Test
    fun `full progress`() {
        val progresses = flowOf(
            WalletState.Loading(0),
            WalletState.Loading(10),
            WalletState.Loading(20),
            WalletState.Loading(30),
            WalletState.Loading(40),
            WalletState.Loaded
        ).asProgress(100)

        runBlocking {
            progresses.collectIndexed { index, value -> // 0 -> 0; 1 -> 10; 2 -> 20; ... 5 -> 100 (Loaded)
                if (index == 5) {
                    assert(value == 100)
                } else {
                    assert(value == index * 10)
                }
            }
        }
    }
}
