package org.karbo.main

import kotlinx.coroutines.*

fun CoroutineScope.waitForFinish() = runBlocking<Unit> {
    val scope = this@waitForFinish
    scope.coroutineContext[Job]?.children?.forEach { it.join() }
}
