package org.karbo.main.screens.send

import org.karbo.main.screens.IntentDeserializer

class SendArgs(val sendAddress: String = "") {
    companion object {
        const val KEY_SEND_ADDRESS = "KEY_SEND_ADDRESS"

        val deserializer: IntentDeserializer<SendArgs> = {
            SendArgs(getStringExtra(KEY_SEND_ADDRESS) ?: "")
        }
    }
}
