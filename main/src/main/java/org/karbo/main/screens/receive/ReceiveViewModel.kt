package org.karbo.main.screens.receive

import com.github.greennick.properties.emptyProperty
import com.github.greennick.properties.generic.invoke
import com.github.greennick.properties.propertyOf
import com.greennick.result.onError
import com.greennick.result.onSuccess
import kotlinx.coroutines.launch
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.data.analytics.Analytics
import org.karbo.app.navigation.*
import org.karbo.app.utils.log
import org.karbo.main.R
import org.karbo.main.data.*
import org.karbo.wallet.Wallet

class ReceiveViewModel(
    private val wallet: Wallet,
    private val analytics: Analytics,
    private val clipboardManager: ClipboardManager,
    private val router: Router,
    private val screens: ScreensFactory
) : BaseViewModel(router) {
    val showLoading = propertyOf(true)
    val address = propertyOf("")
    val amount = propertyOf("")
    val paymentId = propertyOf("")
    val qr = emptyProperty<String>()
    val nfc = emptyProperty<String>()

    init {
        amount { updateNfcAndQr() }
        paymentId { updateNfcAndQr() }
        loadAddress()
    }

    fun nfcSent() = analytics.addressSentByNfc()

    fun addressClicked() {
        log { "addressClicked" }
        clipboardManager.copy("Address", address.value)
        router.showMessage(R.string.receive_address_copied, Duration.SHORT)
    }

    fun generatePaymentIdClicked() {
        val generated = generatePaymentId()
        log { "generatePaymentIdClicked: $generated" }
        paymentId.value = generated
    }

    fun shareClicked() {
        log { "shareClicked" }
        val request = generateRequest() ?: return

        router.navigateTo(screens.shareText("Karbo Address", request))
    }

    private fun loadAddress() {
        launch {
            wallet.getAddress().onSuccess {
                showLoading.value = false
                address.value = it
                updateNfcAndQr()
            }.onError {
                router.showMessage(R.string.receive_error_address)
                router.exit()
            }
        }
    }

    private fun updateNfcAndQr() {
        if (address.value.isEmpty()) return

        val request = generateRequest()
        request?.let { nfc.value = it }

        if (amount.value.isEmpty() && paymentId.value.isEmpty()) {
            qr.value = address.value
        } else {
            request?.let { qr.value = it }
        }
    }

    private fun generateRequest(): String? {
        val request = KrbRequest(address.value, amount.value, paymentId.value)

        return if (request.isValid) request.createRequest() else null
    }
}
