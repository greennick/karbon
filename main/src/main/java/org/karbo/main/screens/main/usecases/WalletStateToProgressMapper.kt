package org.karbo.main.screens.main.usecases

import kotlinx.coroutines.flow.*
import org.karbo.wallet.models.WalletState

fun Flow<WalletState>.asProgress(blockchainHeight: Int): Flow<Int> {
    var firstBlockHeight = -1

    return filter { it is WalletState.Loading || it is WalletState.Loaded }
        .map {
            when (it) {
                is WalletState.Loading -> {
                    if (firstBlockHeight == -1) {
                        firstBlockHeight = it.progress
                    }
                    calculateProgress(blockchainHeight, it.progress, firstBlockHeight)
                }
                is WalletState.Loaded -> 100
                else -> error("Types should be filtered above")
            }
        }
}

private fun calculateProgress(blockchainHeight: Int, loadedBlock: Int, firstBlockHeight: Int): Int {
    if (blockchainHeight == 0 || loadedBlock == 0) return 0

    val diff = loadedBlock - firstBlockHeight
    if (diff == 0) return 0

    val divider = blockchainHeight - firstBlockHeight
    if (divider == 0) return 1

    val progress = diff.toFloat() / divider

    return (progress * 100).toInt()
}
