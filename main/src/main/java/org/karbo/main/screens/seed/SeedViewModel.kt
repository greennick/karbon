package org.karbo.main.screens.seed

import com.github.greennick.properties.generic.MutableProperty
import com.github.greennick.properties.propertyOf
import com.greennick.result.onError
import com.greennick.result.onSuccess
import kotlinx.coroutines.launch
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.data.passwords.PasswordHolder
import org.karbo.app.navigation.Router
import org.karbo.app.utils.log
import org.karbo.app.utils.loge
import org.karbo.main.R
import org.karbo.main.data.ClipboardManager
import org.karbo.wallet.Wallet

class SeedViewModel(
    private val firstShown: Boolean,
    private val wallet: Wallet,
    private val router: Router,
    private val passwordHolder: PasswordHolder,
    private val clipboardManager: ClipboardManager
) : BaseViewModel(router) {
    val showLoading: MutableProperty<Boolean>
    val showAgreement: MutableProperty<Boolean>
    val showBackButton: MutableProperty<Boolean>
    val showValidation: MutableProperty<Boolean>
    val showCopyWarn = propertyOf(false)
    val agreementEnabled = propertyOf(false)
    val showSeed = propertyOf(false)
    val password = propertyOf("")
    val seed = propertyOf("")

    init {
        log { "init, firstShown: $firstShown" }
        showLoading = propertyOf(firstShown)
        showAgreement = propertyOf(firstShown)

        showBackButton = propertyOf(!firstShown)
        showValidation = propertyOf(!firstShown)

        if (firstShown) {
            loadSeed(true)
        }
    }

    fun agreeClicked() {
        log { "agreeClicked" }
        router.exit()
    }

    override fun backPressed() {
        log { "backClicked: $firstShown" }
        if (firstShown) return
        super.backPressed()
    }

    fun copyClicked() {
        log { "copyClicked" }
        showCopyWarn.value = true
    }

    fun copyMnemonic() {
        log { "copyMnemonic" }
        showCopyWarn.value = false
        clipboardManager.copy("Mnemonic", seed.value)
        router.showMessage(R.string.seed_toast_copied)
    }

    fun validationCancelled() {
        log { "validationCancelled" }
        router.exit()
    }

    fun validatePassword() {
        if (passwordHolder.check(password.value)) {
            showLoading.value = true
            showValidation.value = false
            password.value = ""
            loadSeed(false)
        } else {
            router.showMessage(R.string.error_wrong_password)
            router.exit()
        }
    }

    private fun loadSeed(showAgreement: Boolean) {
        log { "loadSeed, show agreement: $showAgreement" }
        launch {
            wallet.getSeed().onSuccess { seed ->
                log { "received: $seed" }
                agreementEnabled.value = showAgreement
                showLoading.value = false
                showSeed.value = true
                this@SeedViewModel.showAgreement.value = showAgreement
                this@SeedViewModel.seed.value = seed
            }.onError {
                loge(it) { "error getting seed" }
                router.exit()
            }
        }
    }
}
