package org.karbo.main.screens.main

import android.content.DialogInterface
import android.content.Intent
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.SearchView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.greennick.properties.android.onClick
import com.github.greennick.properties.androidx.*
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.screen_main.*
import kotlinx.coroutines.*
import org.karbo.app.arch.views.BaseActivity
import org.karbo.app.data.analytics.Analytics
import org.karbo.app.data.managers.ReceivedLinkHandler
import org.karbo.app.navigation.ScreensFactory
import org.karbo.app.utils.alertDialog
import org.karbo.app.utils.hideKeyboard
import org.karbo.main.R
import org.karbo.main.screens.main.adapter.TransactionsAdapter
import org.karbo.main.screens.start
import org.karbo.main.screens.startArg
import kotlin.math.abs

class MainActivity : BaseActivity() {

    override val viewModel: MainViewModel by inject()

    private val args by startArg(MainArgs.deserializer)
    private val coroutineScope = MainScope()

    private val analytics by inject<Analytics>()
    private val receivedLinkHandler by inject<ReceivedLinkHandler>()
    private val screens by inject<ScreensFactory>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val link = processUri(intent)
        if (link != null) {
            receivedLinkHandler.saveLink(link)
            screens.launch().start(this)
            finish()
            return
        }

        setContentView(R.layout.activity_main)
        setupViews()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.activity_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        if (item.itemId == R.id.action_scan_qr) {
            viewModel.scanQrClicked()
            true
        } else {
            super.onOptionsItemSelected(item)
        }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.screenShown()
    }

    override fun onStop() {
        viewModel.screenHidden()
        super.onStop()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val possibleRequest: String = processUri(intent) ?: return
        viewModel.linkReceived(possibleRequest)
    }

    private fun processUri(intent: Intent) = when (intent.action) {
        NfcAdapter.ACTION_NDEF_DISCOVERED -> {
            intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
                ?.firstOrNull()
                ?.let { it as NdefMessage }
                ?.let(NdefMessage::getRecords)
                ?.firstOrNull()
                ?.also {
                    analytics.addressReceivedByNfc()
                }?.toUri()
                ?.toString()
        }
        Intent.ACTION_VIEW -> {
            analytics.addressReceivedFromOut()
            intent.data?.toString()
        }
        else -> null
    }

    private fun setupViews() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        with(nav_view) {
            menu.findItem(R.id.nav_current_wallet).title = "\"" + args.walletName + "\""
            setNavigationItemSelectedListener(::onNavigationItemSelected)
            getHeaderView(0)
                .findViewById<TextView>(R.id.node_url)
                .text = getString(
                R.string.about_connected_to,
                viewModel.connectedNodeHolder.node?.url.toString()
            )
        }

        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        with(drawer_layout) {
            addDrawerListener(object : DrawerLayout.SimpleDrawerListener() {
                override fun onDrawerOpened(drawerView: View) {
                    drawerView.hideKeyboard()
                }
            })
            addDrawerListener(toggle)
            toggle.syncState()
        }

        app_bar.addOnOffsetChangedListener(
            AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                locked_balance.alpha = 1.0f - abs(
                    verticalOffset / appBarLayout.totalScrollRange.toFloat()
                )
            }
        )

        val adapter: TransactionsAdapter

        with(viewModel) {
            adapter = TransactionsAdapter(
                coroutineScope,
                ::transactionClicked,
                ::transactionLongClicked
            )
            bindAdapter(filteredTransactions, adapter)

            fab_send.onClick(::sendClicked)
            fab_receive.onClick(::receiveClicked)

            bindVisibility(main_loader, closingLoader)
            bindText(locked_balance, lockedBalanceTitle)
            bindProgress(blockchain_height, loadingProgress)
            bind(balanceTitle, collapsing_toolbar::setTitle)

            val heightStatus = nav_view.getHeaderView(0)
                .findViewById<TextView>(R.id.blockchain_height_status)
            bindText(heightStatus, blockchainHeightStatus)

            val snackbar = Snackbar.make(toolbar, R.string.main_public_node_warning, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.button_ok) { showNodeWarning.value = false }
            bindVisibility(snackbar, showNodeWarning)

            val aboutDialog = alertDialog {
                titleId = R.string.menu_about
                message = aboutMessage.value
                positiveDialog(R.string.button_ok, DialogInterface::cancel)
                show = true
            }
            bindVisibilityBidirectionally(aboutDialog, showAbout)
            bind(aboutMessage, aboutDialog::setMessage)

            search_transactions.setQuery(searchQuery.value, true)
            search_transactions.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    searchQuery.value = newText
                    return true
                }
            })
        }

        with(transaction_list) {
            isNestedScrollingEnabled = true
            layoutManager = LinearLayoutManager(this@MainActivity)
            this.adapter = adapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                private val fabs = listOf(fab_receive, fab_send)
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    fabs.forEach { if (dy > 0) it.hide() else it.show() }
                }
            })
        }
    }

    private fun onNavigationItemSelected(item: MenuItem) = with(viewModel) {
        drawer_layout.closeDrawer(GravityCompat.START)

        when (item.itemId) {
            R.id.nav_help -> faqClicked()
            R.id.nav_settings -> settingsClicked()
            R.id.nav_seed -> seedClicked()
            R.id.nav_close -> closeClicked()
            R.id.nav_send -> sendEmailClicked()
            R.id.nav_about -> aboutClicked()
            R.id.nav_donate -> donateClicked()
        }
        true
    }

    override fun onDestroy() {
        coroutineScope.coroutineContext[Job]?.cancelChildren()
        super.onDestroy()
    }
}
