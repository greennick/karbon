package org.karbo.main.screens.seed

import org.karbo.main.screens.IntentDeserializer

class SeedArgs(val firstTime: Boolean = false) {

    companion object {
        const val KEY_FIRST_TIME = "KEY_FIRST_TIME"

        val deserializer: IntentDeserializer<SeedArgs> = {
            SeedArgs(getBooleanExtra(KEY_FIRST_TIME, false))
        }
    }
}
