package org.karbo.main.screens.main.usecases

import org.karbo.node.SavedNodeAddressProvider
import org.karbo.node.models.Node

suspend fun SavedNodeAddressProvider.shouldShowCustomNodeWarning(connectedNode: Node): Boolean {
    if (!useCustomNode()) return false

    val custom = getCustomNodeAddress()
    return connectedNode.url != custom
}
