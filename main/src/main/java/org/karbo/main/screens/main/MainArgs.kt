package org.karbo.main.screens.main

import org.karbo.main.screens.IntentDeserializer

class MainArgs(val walletName: String) {
    companion object {
        const val KEY_WALLET = "KEY_WALLET"

        val deserializer: IntentDeserializer<MainArgs> = {
            MainArgs(getStringExtra(KEY_WALLET) ?: "")
        }
    }
}
