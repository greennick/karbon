package org.karbo.main.screens.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.github.greennick.properties.android.onClick
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.greennick.result.getOrDefault
import kotlinx.coroutines.*
import org.karbo.app.utils.*
import org.karbo.main.R
import org.karbo.main.data.ClipboardManager
import org.karbo.main.data.stringFormat
import org.karbo.wallet.Wallet
import org.karbo.wallet.models.Transaction
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class TransactionDetailsFragment : BottomSheetDialogFragment(), CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext = Dispatchers.Main + job

    private val transaction by lazy { arguments?.getSerializable(KEY_TRANSACTION) as Transaction }
    private val clipboard by inject<ClipboardManager>()

    private val wallet by inject<Wallet>()
    private val txAmount by view<TextView>(R.id.txAmount)

    private val txDate by view<TextView>(R.id.txDate)
    private val txHash by view<TextView>(R.id.txHash)
    private val txPaymentId by view<TextView>(R.id.txPaymentId)
    private val txKey by view<TextView>(R.id.txKey)
    private val txBlock by view<TextView>(R.id.txBlock)

    private val format = SimpleDateFormat("dd.MM.yyyy, HH:mm:ss", Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_transaction_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        log { "tx: $transaction" }
        loadTransfers()

        txHash.text = transaction.txHash
        txHash.onClick {
            clipboard.copyTxHash(transaction.txHash)
            toast(R.string.tx_detail_hash_copied, Toast.LENGTH_SHORT)
        }

        with(txAmount) {
            if (transaction.isOutput) {
                setTextColor(color(R.color.tx_list_text_output))
                val amount = "-" + getString(R.string.karbo_amount, transaction.amount.stringFormat())
                text = amount
            } else {
                text = getString(R.string.karbo_amount, transaction.amount.stringFormat())
                setTextColor(color(R.color.tx_list_text_input))
            }
        }

        txDate.text = if (transaction.time == 0L) {
            "-"
        } else {
            format.format(Date(transaction.time))
        }

        with(txPaymentId) {
            if (transaction.paymentId.isEmpty()) {
                setText(R.string.tx_detail_none)
                onClick(null)
            } else {
                text = transaction.paymentId
                onClick {
                    clipboard.copyPaymentId(transaction.paymentId)
                    toast(R.string.tx_detail_payment_id_copied, Toast.LENGTH_SHORT)
                }
            }
        }

        with(txKey) {
            if (transaction.key.isEmpty()) {
                setText(R.string.tx_detail_none)
                onClick(null)
            } else {
                text = transaction.key
                onClick {
                    clipboard.copyTxKey(transaction.key)
                    toast(R.string.tx_detail_key_copied, Toast.LENGTH_SHORT)
                }
            }
        }

        setBlockText()
    }

    override fun onDestroyView() {
        job.cancelChildren()
        super.onDestroyView()
    }

    @SuppressLint("CheckResult")
    private fun loadTransfers() {
        launch {
            val height = wallet.getBlocksHeight().getOrDefault(0)
            if (isActive) setBlockText(height)
        }
    }

    private fun setBlockText(currentHeight: Int = 0) {
        val blockIndex = transaction.blockIndex
        if (blockIndex == 0xffffffffL) {
            txBlock.setText(R.string.tx_list_unconfirmed_transaction)
        } else {
            val confirmations = currentHeight - blockIndex.toInt() + 1
            txBlock.text = resources.getQuantityString(
                R.plurals.tx_list_blocks,
                confirmations,
                blockIndex.toInt(),
                confirmations
            )
        }
    }

    companion object {
        private const val KEY_TRANSACTION = "KEY_TRANSACTION"

        operator fun invoke(transaction: Transaction): TransactionDetailsFragment =
            TransactionDetailsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(KEY_TRANSACTION, transaction)
                }
            }
    }
}

fun <V : View> Fragment.view(@IdRes viewId: Int) = object : ReadOnlyProperty<Fragment, V> {

    private val lifecycleListener = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_DESTROY) {
            cached = null
        }
    }
    private var cached: V? = null

    override fun getValue(thisRef: Fragment, property: KProperty<*>): V {
        var view = cached
        return if (view == null) {
            resetLifecycleObserver()
            view = thisRef.requireView().findViewById(viewId)
            cached = view
            view
        } else {
            view
        }
    }

    private fun resetLifecycleObserver() {
        viewLifecycleOwner.lifecycle.apply {
            removeObserver(lifecycleListener)
            addObserver(lifecycleListener)
        }
    }
}
