package org.karbo.main.screens.seed

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY
import com.github.greennick.properties.android.onClick
import com.github.greennick.properties.androidx.*
import kotlinx.android.synthetic.main.activity_seed.*
import org.karbo.app.arch.views.BaseActivity
import org.karbo.app.utils.*
import org.karbo.main.R
import org.karbo.main.screens.startArg
import org.koin.core.parameter.parametersOf

class SeedActivity : BaseActivity() {

    override val viewModel: SeedViewModel by inject { parametersOf(args.firstTime) }

    private val args by startArg(SeedArgs.deserializer)

    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // set FLAG_SECURE to prevent screenshots
        window.addFlags(WindowManager.LayoutParams.FLAG_SECURE)

        setContentView(R.layout.activity_seed)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)

        with(agree_checkBox) {
            movementMethod = LinkMovementMethod.getInstance()
            text = HtmlCompat.fromHtml(
                getString(R.string.mnemonic_agreement),
                FROM_HTML_MODE_LEGACY
            )
        }

        with(viewModel) {
            toolbar.navigationClicked(::backPressed)
            agree_checkBox.onClick(::agreeClicked)

            bindVisibility(loader, showLoading)
            bindVisibility(agree_checkBox, showAgreement)
            bindEnabled(agree_checkBox, agreementEnabled)
            bindText(mnemonic_seed, seed)
            bindVisibility(mnemonic_seed, showSeed)

            bind(showBackButton, ::setBackButton)

            alertDialog {
                titleId = R.string.seed_warn_title
                messageId = R.string.seed_warn_message

                positive(R.string.button_yes, viewModel::copyMnemonic)
                negativeDialog(R.string.button_no, DialogInterface::cancel)
            }.let { bindVisibilityBidirectionally(it, showCopyWarn) }

            val validationView = inflate(R.layout.dialog_textfield_view)

            val passwordField = validationView.findViewById<TextView>(R.id.input).apply {
                inputType = EditorInfo.TYPE_TEXT_VARIATION_PASSWORD
                setHint(R.string.prompt_password)
                setImeActionLabel(getString(R.string.unlock), EditorInfo.IME_ACTION_DONE)
            }

            alertDialog {
                titleId = R.string.sensitive_info
                messageId = R.string.provide_password_to_unlock
                view = validationView

                positive(R.string.unlock, viewModel::validatePassword)
                negative(R.string.button_cancel, viewModel::validationCancelled)

                cancelable = false
            }.let { bindVisibility(it, showValidation) }
            bindTextBidirectionally(passwordField, password)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.activity_seed, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        if (item.itemId == R.id.action_copy_seed) {
            viewModel.copyClicked()
            true
        } else {
            super.onOptionsItemSelected(item)
        }

    private fun setBackButton(show: Boolean) {
        supportActionBar?.apply {
            setHomeButtonEnabled(show)
            setDisplayHomeAsUpEnabled(show)
        }
    }
}
