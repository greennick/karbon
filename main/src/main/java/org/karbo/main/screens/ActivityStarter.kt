package org.karbo.main.screens

import android.app.Activity
import android.content.Context
import android.content.Intent
import ru.terrakok.cicerone.Screen
import ru.terrakok.cicerone.android.support.SupportAppScreen
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

typealias IntentDeserializer<T> = Intent.() -> T

fun <T> Activity.startArg(deserializer: IntentDeserializer<T>) =
    object : ReadOnlyProperty<Activity, T> {
        private val args by lazy { deserializer(intent) }

        override fun getValue(thisRef: Activity, property: KProperty<*>) = args
    }

/*
 * temp, while implementing Cicerone everywhere
 */
fun Screen.start(context: Context) {
    (this as SupportAppScreen).getActivityIntent(context)?.apply(context::startActivity)
}
