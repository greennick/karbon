package org.karbo.main.screens.send

import com.github.greennick.properties.generic.*
import com.github.greennick.properties.propertyOf
import com.greennick.result.onError
import com.greennick.result.onSuccess
import kotlinx.coroutines.launch
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.data.ConnectedNodeHolder
import org.karbo.app.data.analytics.Analytics
import org.karbo.app.data.managers.*
import org.karbo.app.navigation.*
import org.karbo.app.utils.*
import org.karbo.base.*
import org.karbo.main.R
import org.karbo.main.data.*
import org.karbo.node.NodeService
import org.karbo.node.models.calculateFeeFor
import org.karbo.node.models.calculateRemForSumAfterFee
import org.karbo.wallet.Wallet
import org.karbo.wallet.errors.MixinTooLowException
import org.karbo.wallet.models.Destination
import kotlin.time.ExperimentalTime
import kotlin.time.minutes

@OptIn(ExperimentalTime::class)
class SendViewModel(
    sendAddress: String,
    private val wallet: Wallet,
    private val clipboardManager: ClipboardManager,
    private val analytics: Analytics,
    private val nodeService: NodeService,
    private val receivedLinkHandler: ReceivedLinkHandler,
    private val screens: ScreensFactory,
    private val stringsRepo: StringsRepo,
    private val router: Router,
    connectedNodeHolder: ConnectedNodeHolder
) : BaseViewModel(router) {

    private val node = connectedNodeHolder.node!!
    private val defaultFee = 0.1.Krbs
    private val networkFee = propertyOf(node.info?.minTxFee ?: defaultFee)
    private val unlockedBalance = propertyOf(Krbshi())

    val receiverAddress = propertyOf("")
    val paymentId = propertyOf("")
    val sendAllChecked = propertyOf(false)
    val availableText = unlockedBalance.map { stringsRepo.string(R.string.send_available, it.stringFormat()) }
    val showLoading = propertyOf(false)
    val showFillFromClipboard = propertyOf(false)
    val sendSum = propertyOf("")
    val sendSumEnabled = !sendAllChecked
    val privacyLevel = propertyOf(5)
    val privacyText = privacyLevel.map { stringsRepo.string(R.string.send_privacy_level, it) }

    private val _amount = sendSum.map(String::toKrbshi)

    val feeText = _amount.zipWith(networkFee, ::getFeeText)

    init {
        handlePaymentRequest(sendAddress)
        scheduleBalanceUpdating()
        scheduleNodeStatusUpdating()
        checkClipboard()

        sendAllChecked.subscribeOnTrue(::onSendAll)
    }

    fun generatePidClicked() {
        paymentId.value = generatePaymentId()
    }

    @OptIn(ExperimentalStdlibApi::class)
    fun sendClicked() {
        val destinationAddress = receiverAddress.value
        if (!destinationAddress.isAddress()) {
            router.showMessage(R.string.send_wrong_address, Duration.SHORT)
            return
        }

        val amount = _amount.value

        //return if amount is 0
        if (amount == 0.Krbshis) {
            router.showMessage(R.string.send_wrong_amount, Duration.SHORT)
            return
        }
        //calculate node fee
        val nodeFee = node.calculateFeeFor(amount)
        val networkFee = networkFee.value
        log { "node fee: $nodeFee" }
        log { "network fee: $networkFee" }
        val total = amount + nodeFee + networkFee
        //return if not enough money to send
        if (total > unlockedBalance.value) {
            router.showMessage(R.string.send_not_enough, Duration.SHORT)
            return
        }

        // Send transaction
        val destinations = buildList {
            add(Destination(amount, destinationAddress))

            if (!node.isFree) {
                // fee for the remote node
                add(Destination(nodeFee, node.feeAddress))
            }
        }

        showLoading.value = true
        launch {
            wallet.send(destinations, paymentId.value, privacyLevel.value, networkFee)
                .onSuccess { tx ->
                    analytics.fundsSent()
                    val message = stringsRepo.string(R.string.send_result_transferred) + " " + tx
                    showLoading.value = false
                    router.showMessage(message)
                    router.exit()
                }.onError(::processError)
        }
    }

    private fun processError(error: Throwable) {
        loge(error) { "error sending funds" }
        showLoading.value = false
        when (error) {
            is MixinTooLowException -> {
                router.showMessage(R.string.send_result_error_low_mixin)
            }
            else -> router.showMessage(R.string.send_result_error)
        }
    }

    fun clipboardFillAlertCancelled() {
        showFillFromClipboard.value = false
    }

    fun fillFromClipboard() {
        showFillFromClipboard.value = false

        val copiedText = clipboardManager.currentValue()
        handlePaymentRequest(copiedText)
    }

    fun screenShown() {
        val linkAction = receivedLinkHandler.consumeSavedLink()
        if (linkAction is LinkAction.Send) {
            handlePaymentRequest(linkAction.request)
        }
    }

    fun onScanQr() = router.navigateTo(screens.qrScanner())

    private fun scheduleBalanceUpdating() = repeatEvery(1.minutes) {
        wallet.getBalance().onSuccess {
            log { "updated balance: ${it.unlocked}" }
            unlockedBalance.value = it.unlocked
        }
    }

    private fun scheduleNodeStatusUpdating() = repeatEvery(1.minutes) {
        networkFee.value = nodeService.getNodeInfo(node.url)?.minTxFee
            ?: node.info?.minTxFee
                    ?: defaultFee
    }

    private fun checkClipboard() {
        val copiedText = clipboardManager.currentValue()
        log { "Copied text: [$copiedText]" }

        launch {
            if (copiedText.hasPaymentRequest() || copiedText.hasAddress()) {
                showFillFromClipboard.value = true
            }
        }
    }

    private fun handlePaymentRequest(request: String) {
        log { "handlePaymentRequest $request" }
        if (request.isEmpty()) return

        launch {
            val parsed = when {
                request.hasPaymentRequest() -> request.parsePaymentRequest()
                request.hasAddress() -> KrbRequest(request.parseAddress())
                else -> return@launch
            }

            if (parsed.isValid) {
                receiverAddress.value = parsed.address
                sendSum.value = parsed.amount
                paymentId.value = parsed.paymentId
            }
        }
    }

    private fun getFeeText(sendAmount: Krbshi, networkFee: Krbshi): String {
        val fee = (node.calculateFeeFor(sendAmount) + networkFee).stringFormat()
        return stringsRepo.string(R.string.send_fee, fee)
    }

    private fun onSendAll() {
        val withoutMinersFee = unlockedBalance.value - networkFee.value
        log { "withoutMinersFee: $withoutMinersFee" }
        if (withoutMinersFee <= Krbshi()) {
            //not enough
            sendSum.value = "0"
            return
        }

        val availableAmount = node.calculateRemForSumAfterFee(withoutMinersFee)
        log { "enabled: $availableAmount" }
        sendSum.value = availableAmount.stringFormat()
    }
}
