package org.karbo.main.screens.send

import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.github.greennick.properties.android.onClick
import com.github.greennick.properties.androidx.*
import kotlinx.android.synthetic.main.activity_send.*
import org.karbo.app.arch.views.BaseActivity
import org.karbo.app.utils.alertDialog
import org.karbo.app.utils.navigationClicked
import org.karbo.main.R
import org.koin.core.parameter.parametersOf

class SendActivity : BaseActivity() {

    override val viewModel: SendViewModel by inject {
        val args = SendArgs.deserializer(intent)
        parametersOf(args.sendAddress)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_send)

        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        toolbar.navigationClicked(viewModel::backPressed)

        with(viewModel) {
            bindTextBidirectionally(address_input, receiverAddress)
            bindTextBidirectionally(amount_input, sendSum)
            bindEnabled(amount_input, sendSumEnabled)
            bindTextBidirectionally(pid_input, paymentId)

            bindText(privacy, privacyText)
            bindProgressBidirectionally(privacy_level, privacyLevel)

            bindText(available_text, availableText)
            bindCheckedBidirectionally(send_all_check, sendAllChecked)
            bindText(fee_text, feeText)

            generate_pid.onClick(::generatePidClicked)
            send.onClick(::sendClicked)

            alertDialog {
                messageId = R.string.send_paste_text_message
                show = true

                positive(R.string.button_yes, ::fillFromClipboard)
                negativeDialog(R.string.button_no, DialogInterface::cancel)

                cancelable = true
                onCancel(::clipboardFillAlertCancelled)
            }.let { bindVisibility(it, showFillFromClipboard) }

            ProgressDialog(this@SendActivity).apply {
                setMessage(getString(R.string.send_sending_money))
                setCancelable(false)
            }.let { bindVisibility(it, showLoading) }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.screenShown()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.activity_send, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = if (item.itemId == R.id.action_scan_qr) {
        viewModel.onScanQr()
        true
    } else {
        super.onOptionsItemSelected(item)
    }
}
