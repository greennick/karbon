package org.karbo.main.screens.main

import com.github.greennick.properties.generic.invoke
import com.github.greennick.properties.generic.plus
import com.github.greennick.properties.propertyOf
import com.greennick.result.onSuccess
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.filterIsInstance
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.data.ConnectedNodeHolder
import org.karbo.app.data.analytics.Analytics
import org.karbo.app.data.managers.*
import org.karbo.app.data.passwords.PasswordHolder
import org.karbo.app.navigation.*
import org.karbo.app.navigation.Duration
import org.karbo.app.utils.*
import org.karbo.base.Krbshi
import org.karbo.main.BuildConfig
import org.karbo.main.R
import org.karbo.main.data.*
import org.karbo.main.screens.main.usecases.asProgress
import org.karbo.main.screens.main.usecases.shouldShowCustomNodeWarning
import org.karbo.node.SavedNodeAddressProvider
import org.karbo.wallet.Wallet
import org.karbo.wallet.WalletLoader
import org.karbo.wallet.models.Transaction
import org.karbo.wallet.models.WalletState
import kotlin.time.*

private const val DONATE_ADDRESS =
    "KdEvELRMJMo2SbKVG1SGvB9YxW95UkC8qhxvK1kiVESwVjhcXseQ9PeULxahLPzv6d2jVmmnmRR7xcyKqEsXigdjU3Rd2AQ"

@OptIn(ExperimentalTime::class)
class MainViewModel(
    val connectedNodeHolder: ConnectedNodeHolder,
    private val walletLoader: WalletLoader,
    private val clipboard: ClipboardManager,
    private val analytics: Analytics,
    private val savedNodeProvider: SavedNodeAddressProvider,
    private val passwordHolder: PasswordHolder,
    private val receivedLinkHandler: ReceivedLinkHandler,
    private val walletLoadingService: WalletLoadingService,
    private val wallet: Wallet,
    private val screens: ScreensFactory,
    private val strings: StringsRepo,
    private val router: Router,
    private val walletSavingService: WalletSavingService
) : BaseViewModel(router) {
    val balanceTitle = propertyOf("")
    val lockedBalanceTitle = propertyOf("")
    val searchQuery = propertyOf("")
    val closingLoader = propertyOf(false)
    val showNodeWarning = propertyOf(false)
    val filteredTransactions = propertyOf(emptyList<Transaction>())
    val loadingProgress = propertyOf(0)
    val blockchainHeightStatus = propertyOf("")
    val showAbout = propertyOf(false)
    val aboutMessage = propertyOf("")

    private var filterJob: Job? = null
    private val updatesJob = SupervisorJob()
    private val transactions = propertyOf(emptyList<Transaction>())
    private val blockchainHeight = propertyOf(connectedNodeHolder.node?.info?.lastKnownBlockIndex ?: 0)

    init {
        blockchainHeight {
            blockchainHeightStatus.value = strings.string(R.string.about_height, it)
            updateAboutMessage(it)
        }
        (transactions + searchQuery).subscribe { filterTransactions(it.first, it.second) }
        updateBalance(Krbshi())
        updateLockedBalance(Krbshi())
        checkCustomNode()
        walletLoadingService.show()

        walletLoader.states()
            .filterIsInstance<WalletState.Disconnected>()
            .collectIn(this) {
                log { "Disconnected" }
                router.showMessage(R.string.status_wallet_closed, Duration.SHORT)
                router.replaceScreen(screens.launch())
            }
    }

    fun transactionClicked(transaction: Transaction) =
        router.showDialog(screens.transactionDetails(transaction))

    fun transactionLongClicked(transaction: Transaction) {
        clipboard.copyTxHash(transaction.txHash)
        router.showMessage(R.string.tx_detail_hash_copied, Duration.SHORT)
    }

    fun screenShown() {
        checkReceivedLink()
        repeatEvery(15.seconds, context = updatesJob) {
            wallet.getBalance().onSuccess {
                log { it }
                updateBalance(it.unlocked)
                updateLockedBalance(it.locked)
            }
            wallet.getTransactions().onSuccess {
                transactions.value = it.asReversed()
            }
        }
        repeatEvery(1.minutes, context = updatesJob) {
            wallet.getBlocksHeight().onSuccess { blockchainHeight.value = it }
        }
        walletLoader.states()
            .asProgress(blockchainHeight.value)
            .collectIn(this, updatesJob) { loadingProgress.value = it }
    }

    fun screenHidden() {
        updatesJob.cancelChildren()
        walletSavingService.save()
    }

    fun scanQrClicked() = router.navigateTo(screens.qrScanner())

    override fun backPressed() {
        if (closingLoader.value) return

        closeClicked()
    }

    fun linkReceived(link: String) = router.navigateTo(screens.sending(link))

    fun faqClicked() {
        router.navigateTo(screens.faq())
        analytics.faqClicked()
    }

    fun settingsClicked() = router.navigateTo(screens.settings())

    fun seedClicked() = router.navigateTo(screens.mnemonicSeed())

    fun closeClicked() {
        closingLoader.value = true

        launch { walletLoader.stop() }
    }

    fun sendEmailClicked() = router.showChooser(
        strings.string(R.string.title_send_mail),
        screens.email()
    )

    fun aboutClicked() = showAbout.set(true)

    fun donateClicked() {
        router.navigateTo(screens.sending(DONATE_ADDRESS))
        analytics.donateClicked()
    }

    fun sendClicked() = router.navigateTo(screens.sending())

    fun receiveClicked() = router.navigateTo(screens.receiving())

    override fun onDestroy() {
        log { "onDestroy" }
        walletLoadingService.hide()
        passwordHolder.reset()
    }

    private fun updateBalance(balance: Krbshi) =
        balanceTitle.set(strings.string(R.string.karbo_amount, balance.stringFormat()))

    private fun updateLockedBalance(balance: Krbshi) =
        lockedBalanceTitle.set(strings.string(R.string.karbo_amount, balance.stringFormat()))

    private fun checkCustomNode() {
        launch {
            val connectedNode = connectedNodeHolder.node ?: return@launch

            showNodeWarning.value = savedNodeProvider.shouldShowCustomNodeWarning(connectedNode)
        }
    }

    private fun checkReceivedLink() {
        if (connectedNodeHolder.node == null) return

        val linkAction = receivedLinkHandler.consumeSavedLink()
        if (linkAction is LinkAction.Send) {
            linkReceived(linkAction.request)
        }
    }

    private fun updateAboutMessage(blockchainHeight: Int) = with(strings) {
        val version = BuildConfig.VERSION_NAME
        val developers = strings(R.array.developers).joinToString(separator = "\n")
        val sb = StringBuilder()
            .append("v.").append(version).append("\n")
            .append(string(R.string.about_height, blockchainHeight)).append("\n")
            .append(string(R.string.about_connected_to, connectedNodeHolder.node?.url.toString()))
            .append("\n\n")
            .append(string(R.string.about_devs)).append(":\n")
            .append(developers).append("\n\n")
            .append(string(R.string.about_license)).append("\n")
            .append(string(R.string.mit_license))

        aboutMessage.value = sb.toString()
    }

    private fun filterTransactions(transactions: List<Transaction>, query: String) {
        filterJob?.cancel()
        filterJob = launch {
            filteredTransactions.value = withContext(Dispatchers.Main) { transactions.filterBy(query).sorted() }
        }
    }
}
