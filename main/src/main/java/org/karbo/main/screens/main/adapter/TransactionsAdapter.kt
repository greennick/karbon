package org.karbo.main.screens.main.adapter

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.github.greennick.properties.android.BoundAdapter
import com.github.greennick.properties.android.onClick
import kotlinx.coroutines.*
import org.karbo.app.utils.color
import org.karbo.app.utils.inflate
import org.karbo.main.R
import org.karbo.main.data.stringFormat
import org.karbo.wallet.models.Transaction
import java.text.SimpleDateFormat
import java.util.*

/*
 * Created by Green-Nick on 20.11.2017.
 */

class TransactionsAdapter(
    private val scope: CoroutineScope,
    private val onClickListener: (Transaction) -> Unit,
    private val onLongClickListener: (Transaction) -> Unit
) : Adapter<RecyclerView.ViewHolder>(), BoundAdapter<Transaction> {

    private val dateFormat = SimpleDateFormat("dd.MM.yyyy, HH:mm", Locale.getDefault())
    private val date = Date()
    private var transactions = emptyList<Transaction>()

    private val empty get() = transactions.isEmpty()

    @SuppressLint("CheckResult")
    override fun update(newItems: List<Transaction>) {
        scope.launch(Dispatchers.IO) {
            val diff = TransactionsDiffUtils(transactions, newItems).let(DiffUtil::calculateDiff)

            withContext(Dispatchers.Main) {
                transactions = newItems
                diff.dispatchUpdatesTo(this@TransactionsAdapter)
            }
        }
    }

    override fun getItemViewType(position: Int) =
        if (empty) Types.EMPTY.layoutId else Types.TRANSACTION.layoutId

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = viewType(viewType).viewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (viewType(getItemViewType(position)) == Types.TRANSACTION) {
            val vh = holder as TransactionViewHolder
            val transaction = transactions[position]

            vh.apply {
                val context = itemView.context

                itemView.onClick { onClickListener(transaction) }
                itemView.setOnLongClickListener {
                    onLongClickListener(transaction)
                    true
                }

                amount.text = if (transaction.isOutput) {
                    icon.setImageResource(R.drawable.ic_tx_out)
                    amount.setTextColor(context.color(R.color.tx_list_text_output))
                    "-" + context.getString(R.string.karbo_amount, transaction.amount.stringFormat())
                } else {
                    icon.setImageResource(R.drawable.ic_tx_in)
                    amount.setTextColor(context.color(R.color.tx_list_text_input))
                    context.getString(R.string.karbo_amount, transaction.amount.stringFormat())
                }

                hash.text = transaction.txHash
                txDate.text = if (transaction.time == 0L) {
                    "-"
                } else {
                    date.time = transaction.time
                    dateFormat.format(date)
                }
            }
        }
    }

    override fun getItemCount() = if (empty) 1 else transactions.size

    private fun viewType(intType: Int) = Types.values().first { intType == it.layoutId }
}

private class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val txDate: TextView = itemView.findViewById(R.id.txDate)
    val amount: TextView = itemView.findViewById(R.id.txAmount)
    val hash: TextView = itemView.findViewById(R.id.txHash)
    val icon: ImageView = itemView.findViewById(R.id.txIcon)
}

private class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

private enum class Types(val layoutId: Int) {
    EMPTY(R.layout.list_item_empty) {
        override fun viewHolder(parent: ViewGroup) = EmptyViewHolder(inflate(layoutId, parent))
    },
    TRANSACTION(R.layout.list_item_transaction) {
        override fun viewHolder(parent: ViewGroup) = TransactionViewHolder(inflate(layoutId, parent))
    };

    abstract fun viewHolder(parent: ViewGroup): RecyclerView.ViewHolder
}
