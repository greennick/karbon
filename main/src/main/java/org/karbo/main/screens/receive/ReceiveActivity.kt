package org.karbo.main.screens.receive

import android.nfc.*
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.github.greennick.properties.android.onClick
import com.github.greennick.properties.androidx.*
import kotlinx.android.synthetic.main.activity_receive.*
import kotlinx.coroutines.*
import org.karbo.app.arch.views.BaseActivity
import org.karbo.app.utils.navigationClicked
import org.karbo.main.R
import org.karbo.main.data.encodeQr

class ReceiveActivity : BaseActivity() {

    override val viewModel: ReceiveViewModel by inject()

    private val nfcAdapter: NfcAdapter? by lazy { NfcAdapter.getDefaultAdapter(this) }

    private val coroutineScope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_receive)

        nfcAdapter?.setOnNdefPushCompleteCallback(
            NfcAdapter.OnNdefPushCompleteCallback { viewModel.nfcSent() },
            this
        )

        toolbar.apply {
            setSupportActionBar(this)
            navigationClicked(viewModel::backPressed)
        }

        supportActionBar?.apply {
            setDisplayShowTitleEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        with(viewModel) {
            wallet_address.onClick(::addressClicked)
            receive_gen_pid.onClick(::generatePaymentIdClicked)

            bindTextBidirectionally(receive_amount, amount)
            bindTextBidirectionally(receive_pid, paymentId)

            bindVisibility(loader_view, showLoading)

            bindText(wallet_address, address)

            bindNonNull(nfc, ::setNfc)
            bindNonNull(qr, ::drawQr)
        }
    }

    override fun onDestroy() {
        coroutineScope.coroutineContext[Job]?.cancel()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.activity_receive, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        if (item.itemId == R.id.action_addr_share) {
            viewModel.shareClicked()
            true
        } else {
            super.onOptionsItemSelected(item)
        }

    private fun drawQr(request: String) {
        coroutineScope.launch {
            qr_code_view.setImageBitmap(request.encodeQr(550))
        }
    }

    private fun setNfc(request: String) {
        nfcAdapter?.apply {
            val requestRecord = NdefRecord.createUri(request)
            val message = NdefMessage(requestRecord)
            setNdefPushMessage(message, this@ReceiveActivity)
        }
    }
}
