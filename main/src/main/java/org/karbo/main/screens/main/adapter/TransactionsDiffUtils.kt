package org.karbo.main.screens.main.adapter

import androidx.recyclerview.widget.DiffUtil
import org.karbo.wallet.models.Transaction

class TransactionsDiffUtils(
    private val old: List<Transaction>,
    private val new: List<Transaction>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        if (old.isEmpty()) return false
        return old[oldItemPosition] == new[newItemPosition]
    }

    override fun getOldListSize() = if (old.isEmpty()) 1 else old.size

    override fun getNewListSize() = new.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        if (old.isEmpty()) return false
        val oldTx = old[oldItemPosition]
        val newTx = new[newItemPosition]
        return oldTx.isOutput == newTx.isOutput &&
                oldTx.time == newTx.time &&
                oldTx.amount == newTx.amount &&
                oldTx.txHash == newTx.txHash
    }
}