package org.karbo.main.screens.qr

import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.github.greennick.properties.android.onClick
import com.google.zxing.BarcodeFormat
import kotlinx.android.synthetic.main.activity_qr_scanner.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.karbo.app.data.analytics.Analytics
import org.karbo.app.data.managers.ReceivedLinkHandler
import org.karbo.app.utils.*
import org.karbo.main.R
import org.koin.android.ext.android.inject

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class QrScannerActivity : AppCompatActivity() {
    private val scannerView by lazy(LazyThreadSafetyMode.NONE) { ZXingScannerView(this) }
    private val analytics by inject<Analytics>()
    private val receivedLink by inject<ReceivedLinkHandler>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr_scanner)

        if (hasCameraPermission) {
            initQrScanner()
        } else {
            requestCamera()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (hasCameraPermission) {
            initQrScanner()
        } else {
            toast(R.string.permission_rationale_camera_message)
            finish()
        }
    }

    private fun initQrScanner() {
        scannerView.setResultHandler { result ->
            log { result.text }
            log { result.barcodeFormat }

            receivedLink.saveLink(result.text)

            analytics.qrCodeScanned()
            finish()
        }
        scannerView.setFormats(listOf(BarcodeFormat.QR_CODE))

        frame.addView(scannerView, 0)

        flash_button.onClick {
            try {
                scannerView.toggleFlash()
                flash_button.setImageDrawable(drawable(if (scannerView.flash) R.drawable.ic_flash_off else R.drawable.ic_flash_on))
            } catch (ignore: RuntimeException) {
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (hasCameraPermission) {
            scannerView.startCamera()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (hasCameraPermission) {
            scannerView.stopCamera()
        }
    }

    private fun drawable(@DrawableRes id: Int) = ContextCompat.getDrawable(this, id)
}
