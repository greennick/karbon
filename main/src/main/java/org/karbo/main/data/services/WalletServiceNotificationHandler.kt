package org.karbo.main.data.services

import android.app.*
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import org.karbo.main.R
import org.karbo.main.screens.main.MainActivity

class WalletServiceNotificationHandler(private val context: Context) {
    private val notificationManager =
        context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    private val deletePendingIntent =
        PendingIntent.getBroadcast(context, 0, Intent(ACTION_CLOSE_WALLET), 0)

    private fun notificationBuilder(): NotificationCompat.Builder {
        val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationCompat.Builder(context, createNotificationChannel())
        } else {
            NotificationCompat.Builder(context, "main")
        }.apply {
            setDeleteIntent(deletePendingIntent)
        }

        return builder.setContentTitle(context.getString(R.string.notification_title))
            .setSmallIcon(R.drawable.ic_krb_notification_icon)
    }

    fun getSavingNotification(): Notification = foregroundNotificationBuilder()
        .run {
            setContentText(context.getString(R.string.notification_status_saving))
            indeterminateProgress()
            build()
        }

    fun getLoadingNotification(): Notification = foregroundNotificationBuilder()
        .run {
            val pendingIntent = PendingIntent.getActivity(
                context,
                0,
                Intent(context, MainActivity::class.java),
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            setContentIntent(pendingIntent)

            setContentText(context.getString(R.string.notification_status_loading))
            indeterminateProgress()
            build()
        }

    fun getLoadingNotification(currentBlock: Int, height: Int): Notification =
        foregroundNotificationBuilder()
            .run {
                val pendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    Intent(context, MainActivity::class.java),
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
                setContentIntent(pendingIntent)

                val blocksStatus = "$currentBlock/$height"
                setContentText(context.getString(R.string.status_loading_format, blocksStatus))
                setProgress(height, currentBlock, false)
                build()
            }

    fun getClosingNotification(): Notification = foregroundNotificationBuilder()
        .run {
            setContentText(context.getString(R.string.notification_status_closing))
            indeterminateProgress()
            build()
        }

    private fun foregroundNotificationBuilder(): NotificationCompat.Builder =
        notificationBuilder()
            .setContentText(context.getString(R.string.notification_message))
            .setTicker(context.getString(R.string.ticker_text))

    private fun NotificationCompat.Builder.indeterminateProgress() {
        setProgress(100, 50, true)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String {
        val channelId = "wallet_service"
        val channelName = "Karbo Wallet Service"
        val channel =
            NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW)
                .apply { lockscreenVisibility = Notification.VISIBILITY_PRIVATE }

        notificationManager.createNotificationChannel(channel)

        return channelId
    }

    companion object {
        const val ACTION_CLOSE_WALLET = "org.karbowanets.karbon.service.ACTION_CLOSE_WALLET"
    }
}