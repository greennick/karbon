package org.karbo.main.data.services

import android.app.Service
import android.content.*
import android.os.Binder
import android.os.IBinder
import com.greennick.result.getOrDefault
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.karbo.app.utils.log
import org.karbo.wallet.Wallet
import org.karbo.wallet.WalletLoader
import org.karbo.wallet.models.WalletState
import org.koin.android.ext.android.inject

private const val NOTIFICATION_ID = 202

class LoadingService : Service(), CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext = Dispatchers.Main + job
    private val notificationHandler by inject<WalletServiceNotificationHandler>()
    private val walletLoader by inject<WalletLoader>()
    private val wallet by inject<Wallet>()

    private var height = 0

    override fun onBind(intent: Intent): IBinder {
        height = 0
        startForeground(NOTIFICATION_ID, notificationHandler.getLoadingNotification())
        launch {
            height = wallet.getBlocksHeight().getOrDefault(0)
            walletLoader.states()
                .onEach { state ->
                    log { "state: $state" }
                    if (state is WalletState.Loaded || state is WalletState.Disconnected) {
                        stop()
                    }
                }.filterIsInstance<WalletState.Loading>()
                .map { it.progress }
                .collect {
                    startForeground(NOTIFICATION_ID, notificationHandler.getLoadingNotification(it, height))
                }
        }

        return Binder()
    }

    private fun stop() {
        stopForeground(true)
        stopSelf()
    }

    override fun onUnbind(intent: Intent): Boolean {
        log { "onUnbind" }
        job.cancelChildren()
        stop()
        return false
    }

    companion object {
        private val connection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {}

            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {}
        }

        fun bind(context: Context) {
            val intent = Intent(context, LoadingService::class.java)
            context.bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }

        fun unbind(context: Context) {
            try {
                context.unbindService(connection)
            } catch (ignore: IllegalArgumentException) {
            }
        }
    }
}
