package org.karbo.main.data

import android.content.ClipData
import android.content.Context
import org.karbo.app.data.analytics.Analytics

/*
 * Created by Green-Nick on 20.11.2017.
 */

interface ClipboardManager {
    fun copy(label: String, text: String)

    fun copyTxHash(hash: String)

    fun copyTxKey(key: String)

    fun copyPaymentId(paymentId: String)

    fun currentValue(): String
}

class ClipboardManagerImpl(context: Context, private val analytics: Analytics) : ClipboardManager {
    private val manager = context.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager?

    override fun copy(label: String, text: String) {
        manager?.primaryClip = ClipData.newPlainText(label, text)
    }

    override fun copyTxHash(hash: String) {
        copy("Transaction hash", hash)
        analytics.txHashCopied()
    }

    override fun copyTxKey(key: String) {
        copy("Transaction key", key)
    }

    override fun copyPaymentId(paymentId: String) {
        copy("Payment ID", paymentId)
        analytics.paymentIdCopied()
    }

    override fun currentValue() = manager?.primaryClip
        ?.getItemAt(0)
        ?.text
        ?.toString()
        ?: ""
}
