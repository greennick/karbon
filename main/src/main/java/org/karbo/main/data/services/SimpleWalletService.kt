package org.karbo.main.data.services

import android.app.Service
import android.content.Context
import android.content.Intent
import kotlinx.coroutines.*
import org.karbo.app.utils.log
import org.karbo.wallet.Wallet
import org.koin.android.ext.android.inject

/*
 * Created by greennick on 28.01.2018.
 */

private const val ACTION_SAVE_WALLET = "ACTION_SAVE_WALLET"
private const val NOTIFICATION_ID = 201

class SimpleWalletService : Service(), CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext = Dispatchers.Main + job
    private val notificationHandler by inject<WalletServiceNotificationHandler>()
    private val wallet by inject<Wallet>()

    override fun onBind(intent: Intent?) = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        log { "$intent, $flags, $startId" }

        if (intent?.action == ACTION_SAVE_WALLET) {
            saveWallet()
        } else {
            stopSelf()
        }

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        log { "onDestroy" }
        job.cancelChildren()
    }

    private fun saveWallet() {
        log { "saveWallet" }
        startForeground(NOTIFICATION_ID, notificationHandler.getSavingNotification())
        launch {
            wallet.save()
            stopSelf()
        }
    }

    companion object {

        fun saveWallet(context: Context) {
            Intent(context, SimpleWalletService::class.java).apply {
                action = ACTION_SAVE_WALLET
                log { "starting service with $this" }
                context.startService(this)
            }
        }
    }
}
