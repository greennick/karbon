package org.karbo.main.data

import android.content.Context
import org.karbo.main.data.services.LoadingService

interface WalletLoadingService {

    fun show()

    fun hide()
}

class WalletLoadingServiceImpl(private val context: Context) : WalletLoadingService {
    override fun show() = LoadingService.bind(context)

    override fun hide() = LoadingService.unbind(context)
}
