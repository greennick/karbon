package org.karbo.main.data

import android.content.Context
import org.karbo.main.data.services.SimpleWalletService

interface WalletSavingService {
    fun save()
}

class WalletSavingServiceImpl(private val context: Context) : WalletSavingService {

    override fun save() = SimpleWalletService.saveWallet(context)
}
