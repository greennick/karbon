package org.karbo.main.data

import org.karbo.wallet.models.Transaction

fun List<Transaction>.filterBy(query: String) = filter {
    it.txHash.contains(query, true) or it.paymentId.contains(query, true)
}
