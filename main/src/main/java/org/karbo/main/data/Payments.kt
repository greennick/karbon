@file:Suppress("ConvertToStringTemplate")

package org.karbo.main.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val KRB_SCHEME = "karbowanec"
private const val PARAM_AMOUNT = "amount"
private const val PARAM_PAYMENT_ID = "payment_id"

private const val ADDRESS_REGEXP =
    "K[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{94}"
private const val HAS_ADDRESS_REGEXP = "(.|\\s)*" + ADDRESS_REGEXP + "(.|\\s)*"

private const val REQUEST_REGEXP = KRB_SCHEME + ":" + ADDRESS_REGEXP
private const val HAS_REQUEST_REGEXP = "(.|\\s)*" + REQUEST_REGEXP + "(.|\\s)*"

private const val AMOUNT_PARAM_REGEXP = PARAM_AMOUNT + "=([0-9]+\\.[0-9]+|[1-9][0-9]*|[0-9])"
private const val PAYMENT_ID_PARAM_REGEXP = PARAM_PAYMENT_ID + "=([0-9]|[a-z]|[A-Z]){64}"

class AddressRequest(
    private val scheme: String,
    private val address: String,
    params: Map<String, Any?> = emptyMap()
) {

    private val cleanedParams = params
        .filterValues { value -> value != null }
        .mapValues { (_, value) -> value.toString() }
        .filterValues { value -> value.isNotEmpty() }
        .map { (key, value) -> "$key=$value" }

    fun createRequest(): String {
        val sb = StringBuilder("$scheme:$address")

        cleanedParams.apply {
            if (isNotEmpty()) joinTo(sb, prefix = "?", separator = "&")
        }

        return sb.toString()
    }
}

data class KrbRequest(
    val address: String = "",
    val amount: String = "",
    val paymentId: String = ""
) {
    val isValid
        get() = address.isNotEmpty() and address.matches(ADDRESS_REGEXP.toRegex())

    val hasAmount
        get() = amount.isNotEmpty()

    val hasPaymentId
        get() = paymentId.isNotEmpty()

    fun createRequest() = AddressRequest(
        KRB_SCHEME, address, mapOf(
            PARAM_AMOUNT to amount,
            PARAM_PAYMENT_ID to paymentId
        )
    ).createRequest()
}

suspend fun String.hasAddress(): Boolean = withContext(Dispatchers.Default) {
    matches(HAS_ADDRESS_REGEXP.toRegex())
}

suspend fun String.hasPaymentRequest(): Boolean = withContext(Dispatchers.Default) {
    matches(HAS_REQUEST_REGEXP.toRegex())
}

fun String.parseAddress(): String =
    ADDRESS_REGEXP.toRegex()
        .find(this)
        ?.groupValues
        ?.get(0)
        ?: ""

fun String.isAddress() = matches(ADDRESS_REGEXP.toRegex())

fun String.parsePaymentRequest(): KrbRequest {
    val matchResult = REQUEST_REGEXP.toRegex().find(this) ?: return KrbRequest()

    val address = matchResult.groupValues[0].parseAddress()
    val lastPosition = matchResult.range.last
    val startParamPosition = lastPosition + 1

    return when {
        length == (lastPosition + 1) -> KrbRequest(address)
        this[startParamPosition] != '?' -> KrbRequest(address)
        this[startParamPosition] == '?' && length == (startParamPosition + 1) -> KrbRequest(address)
        else -> {
            val possibleParams = substring((startParamPosition + 1), length)
            val endOfParams = possibleParams.indexOf(' ')
            val cleared = if (endOfParams < 0) {
                possibleParams
            } else {
                possibleParams.substring(0, endOfParams)
            }
            val paramsArray = cleared.split('&')
            val amountParam = paramsArray
                .filter { AMOUNT_PARAM_REGEXP.toRegex().find(it) != null }
                .map {
                    AMOUNT_PARAM_REGEXP.toRegex()
                        .find(it)
                        ?.groupValues
                        ?.get(0)
                }
                .firstOrNull() ?: ""
            val paymentIdParam = paramsArray
                .filter { PAYMENT_ID_PARAM_REGEXP.toRegex().find(it) != null }
                .map {
                    PAYMENT_ID_PARAM_REGEXP.toRegex()
                        .find(it)
                        ?.groupValues
                        ?.get(0)
                }
                .firstOrNull() ?: ""

            val amount = if (amountParam.isNotEmpty()) {
                val divider = amountParam.indexOf('=') + 1
                amountParam.substring(divider, amountParam.length)
            } else {
                ""
            }

            val paymentId = if (paymentIdParam.isNotEmpty()) {
                val divider = paymentIdParam.indexOf('=') + 1
                paymentIdParam.substring(divider, paymentIdParam.length)
            } else {
                ""
            }

            KrbRequest(address, amount, paymentId)
        }
    }
}
