package org.karbo.main.data

/*
 * Created by Green-Nick on 23.11.2017.
 */

import android.graphics.Bitmap
import android.graphics.Color

import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import kotlinx.coroutines.*

private const val QR_CODE_SIZE = 400

suspend fun String.encodeQr(size: Int = QR_CODE_SIZE) = withContext<Bitmap>(Dispatchers.Default) {
    val bitMatrix = MultiFormatWriter().encode(this@encodeQr, BarcodeFormat.QR_CODE, size, size, null)

    val bitMatrixWidth = bitMatrix.width

    val bitMatrixHeight = bitMatrix.height

    val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

    for (y in 0 until bitMatrixHeight) {
        val offset = y * bitMatrixWidth

        for (x in 0 until bitMatrixWidth) {

            pixels[offset + x] = if (bitMatrix.get(x, y))
                Color.BLACK
            else
                Color.WHITE
        }

        ensureActive()
    }

    ensureActive()

    Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.RGB_565).apply {
        setPixels(pixels, 0, size, 0, 0, bitMatrixWidth, bitMatrixHeight)
    }
}
