@file:JvmName("WalletUtils")

package org.karbo.main.data

/*
 * Created by Green-Nick on 17.11.2017.
 */

import org.karbo.base.*
import java.security.SecureRandom
import java.text.DecimalFormat

fun Krbshi.stringFormat() = DecimalFormat("0.00##########")
    .format(amount.toDouble() / KRBSHIS_IN_KRB)
    .replace(',', '.')

fun generatePaymentId() = ByteArray(32)
    .also { SecureRandom().nextBytes(it) }
    .joinTo(StringBuilder(), separator = "") { byte -> String.format("%02X", byte) }
    .toString()

fun String.normalizeAmount(): String = replace(',', '.')
    .toDoubleSafe()
    .let { DecimalFormat("0.############").format(it) }
    .replace(',', '.')


fun String.toKrbshi() = run { if (isEmpty()) "0" else this }
    .run(String::normalizeAmount)
    .run { toDoubleSafe() }
    .run { Krbs }

private fun String.toDoubleSafe(defaultValue: Double = 0.0) = toDoubleOrNull() ?: defaultValue
