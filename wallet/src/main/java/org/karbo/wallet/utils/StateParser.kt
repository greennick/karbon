package org.karbo.wallet.utils

import org.karbo.wallet.models.WalletState

internal fun String.parseInputAsState() = when {
    "Shortest chain" in this -> WalletState.Loading(toProgress())
    "No new blocks received" in this -> WalletState.Loaded
    else -> null
}

/**
 * Formatting line like "2017-Dec-30 19:38:48.307321 DEBUG   Shortest chain size 1354"
 * into progress integer 1354
 *
 * @receiver input from core;
 * @return progress;
 */
private fun String.toProgress() =
    substringAfter("Shortest")// "Shortest chain size 1354"
        .split(' ') // ["Shortest", "chain", "size", "1354"]
        .getOrNull(3) // "1354"
        ?.toIntOrNull() ?: 0 // 1354
