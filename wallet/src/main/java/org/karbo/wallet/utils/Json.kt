package org.karbo.wallet.utils

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import org.karbo.wallet.rpc.RpcMethod
import org.karbo.wallet.rpc.requests.RpcRequest
import org.karbo.wallet.rpc.responses.ResponseResult
import org.karbo.wallet.rpc.responses.RpcResponse

private val moshi = Moshi.Builder().build()

internal fun RpcMethod.json() = RpcRequest<Any>(this.method).let {
    moshi.adapter<RpcRequest<Any>>(
        Types.newParameterizedType(
            RpcRequest::class.java,
            Any::class.java
        )
    ).toJson(it)
}

internal inline fun <reified T> RpcMethod.json(params: T) =
    RpcRequest(this.method, params).let {
        moshi.adapter<RpcRequest<T>>(
            Types.newParameterizedType(
                RpcRequest::class.java,
                T::class.java
            )
        ).toJson(it)
    }

internal inline fun <reified T : ResponseResult> String.fromJson(): RpcResponse<T> =
    moshi.adapter<RpcResponse<T>>(
        Types.newParameterizedType(
            RpcResponse::class.java,
            T::class.java
        )
    ).fromJson(this)!!
