package org.karbo.wallet

import org.karbo.base.Logger
import org.karbo.wallet.configs.RpcConfig
import org.karbo.wallet.configs.WalletConfig
import java.io.File

class WalletHolder(val wallet: Wallet, val loader: WalletLoader)

fun initWalletHolder(configDir: File, coreFile: File, logsDir: File, logger: Logger): WalletHolder {
    val rpc = RpcConfig()
    val config = WalletConfig(configDir, rpc)
    val wallet = WalletImpl(rpc, logger)
    val loader = WalletLoaderImpl(wallet, coreFile, logsDir, rpc, config, logger)
    return WalletHolder(wallet, loader)
}
