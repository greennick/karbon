package org.karbo.wallet

import com.greennick.result.Result
import kotlinx.coroutines.flow.Flow
import org.karbo.wallet.models.WalletFile
import org.karbo.wallet.models.WalletState

interface WalletLoader {

    val currentState: WalletState

    suspend fun create(nodeUrl: String, wallet: WalletFile, password: String): Result<Unit, CreateError>

    suspend fun restore(nodeUrl: String, wallet: WalletFile, password: String, seed: String): Result<Unit, RestoreError>

    suspend fun init(nodeUrl: String, wallet: WalletFile, password: String): Result<Unit, InitError>

    suspend fun stop()

    fun states(): Flow<WalletState>
}

sealed class CreateError {

    object Error : CreateError()

    object CoreNotFound : CreateError()
}

sealed class RestoreError {

    object InvalidRecoveryPhrase : RestoreError()

    object NoNodeConnection : RestoreError()

    object CoreNotFound : RestoreError()
}

sealed class InitError {

    object AlreadyRunning : InitError()

    object NoNodeConnection : InitError()

    object InvalidPassword : InitError()

    object WalletNotFound : InitError()

    object CoreNotFound : InitError()
}
