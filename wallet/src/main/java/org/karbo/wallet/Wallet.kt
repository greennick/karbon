package org.karbo.wallet

import com.greennick.result.Result
import org.karbo.base.Krbshi
import org.karbo.wallet.models.Balance
import org.karbo.wallet.models.Destination
import org.karbo.wallet.models.Transaction

interface Wallet {

    suspend fun getAddress(): Result<String, Exception>

    suspend fun getBlocksHeight(): Result<Int, Exception>

    suspend fun getBalance(): Result<Balance, Exception>

    suspend fun getTransactions(): Result<List<Transaction>, Exception>

    suspend fun getTransaction(txHash: String): Result<Transaction, Exception>

    suspend fun getSeed(): Result<String, Exception>

    suspend fun send(
        destinations: List<Destination>,
        paymentId: String,
        mixin: Int,
        networkFee: Krbshi
    ): Result<String, Exception>

    suspend fun save(): Result<Unit, Exception>
}
