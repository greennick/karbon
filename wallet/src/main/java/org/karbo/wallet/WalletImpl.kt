package org.karbo.wallet

import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.greennick.result.get
import com.greennick.result.map
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.karbo.base.Krbshi
import org.karbo.base.Logger
import org.karbo.wallet.configs.RpcConfig
import org.karbo.wallet.errors.MixinTooLowException
import org.karbo.wallet.models.*
import org.karbo.wallet.rpc.RPC_API
import org.karbo.wallet.rpc.RpcMethod.*
import org.karbo.wallet.rpc.requests.*
import org.karbo.wallet.rpc.responses.*
import org.karbo.wallet.utils.fromJson
import org.karbo.wallet.utils.json

internal class WalletImpl(private val rpcConfig: RpcConfig, private val logger: Logger) : Wallet {
    private val address = "http://${rpcConfig.address}:${rpcConfig.port}/$RPC_API"

    override suspend fun getAddress(): com.greennick.result.Result<String, Exception> =
        GET_ADDRESS.json()
            .post<AddressResponse>()
            .map(AddressResponse::address)

    override suspend fun getBlocksHeight(): com.greennick.result.Result<Int, Exception> =
        GET_HEIGHT.json()
            .post<HeightResponse>()
            .map(HeightResponse::height)

    override suspend fun getBalance(): com.greennick.result.Result<Balance, Exception> =
        GET_BALANCE.json()
            .post<BalanceResponse>()
            .map(BalanceResponse::toBalance)

    override suspend fun getTransactions(): com.greennick.result.Result<List<Transaction>, Exception> =
        GET_TRANSACTIONS.json()
            .post<TransactionsResponse>()
            .map { it.transfers.map(TransactionResponse::toTransaction) }

    override suspend fun getTransaction(txHash: String): com.greennick.result.Result<Transaction, Exception> =
        GET_TRANSACTION.json(TransactionRequest(txHash))
            .post<TransactionDetailsResponse>()
            .map { it.transaction_details.toTransaction() }

    override suspend fun getSeed(): com.greennick.result.Result<String, Exception> =
        GET_SEED.json(SeedRequest())
            .post<SeedResponse>()
            .map(SeedResponse::key)

    override suspend fun send(
        destinations: List<Destination>,
        paymentId: String,
        mixin: Int,
        networkFee: Krbshi
    ): com.greennick.result.Result<String, Exception> {
        val request = TransferRequest(
            destinations = destinations.toRequest(),
            payment_id = paymentId,
            mixin = mixin,
            fee = networkFee.amount
        )

        return SEND.json(request)
            .post<TransferResponse>()
            .map(TransferResponse::tx_hash)
    }

    override suspend fun save(): com.greennick.result.Result<Unit, Exception> {
        val trySave = suspend {
            SAVE.json()
                .post<SaveResponse>()
                .map(SaveResponse::stored)
        }

        var result = trySave()

        repeat(3) {
            if (result.get() == true) {
                return com.greennick.result.Result.success()
            }

            result = trySave()
        }

        return result.map { Unit }
    }

    private suspend inline fun <reified T : ResponseResult> String.post(): com.greennick.result.Result<T, Exception> {
        val (request, response, result) = withContext(Dispatchers.IO) {
            address.httpPost()
                .authentication()
                .basic(rpcConfig.username, rpcConfig.password)
                .jsonBody(this@post)
                .responseString()
        }

        logger.d(request.toString())
        logger.d(response.toString())

        return when (result) {
            is Result.Success -> {
                logger.d(result.value)
                getResult(result.value.fromJson())
            }
            is Result.Failure -> {
                logger.e("error sending post", result.error.exception)
                com.greennick.result.Result.error(result.error)
            }
        }
    }

    private fun <T : ResponseResult> getResult(response: RpcResponse<T>): com.greennick.result.Result<T, Exception> {
        val error = response.error
        return when {
            error != null && error.code == -6 -> com.greennick.result.Result.error(MixinTooLowException())
            error != null -> com.greennick.result.Result.error(IllegalStateException("got error: ${error.code} ${error.message}"))
            else -> com.greennick.result.Result.success(response.result!!)
        }
    }
}
