@file:Suppress("unused")

package org.karbo.wallet.rpc.requests

import com.squareup.moshi.JsonClass
import org.karbo.wallet.rpc.RPC_ID
import org.karbo.wallet.rpc.RPC_VERSION

@JsonClass(generateAdapter = true)
internal class RpcRequest<out T>(
    val method: String,
    val params: T? = null,
    val id: String = RPC_ID,
    val jsonrpc: String = RPC_VERSION
)