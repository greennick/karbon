package org.karbo.wallet.rpc.responses

import com.squareup.moshi.JsonClass
import org.karbo.base.Krbshi
import org.karbo.wallet.models.Balance

@JsonClass(generateAdapter = true)
internal class BalanceResponse(val available_balance: Long, val locked_amount: Long) : ResponseResult

internal fun BalanceResponse.toBalance() = Balance(
    total = Krbshi(available_balance + locked_amount),
    locked = Krbshi(locked_amount),
    unlocked = Krbshi(available_balance)
)
