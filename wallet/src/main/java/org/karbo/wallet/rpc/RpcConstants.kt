package org.karbo.wallet.rpc

internal const val RPC_VERSION = "2.0"
internal const val RPC_ID = "EWF8aIFX0y9w"
internal const val RPC_API = "json_rpc"

internal enum class RpcMethod(val method: String) {
    SAVE("store"),
    GET_ADDRESS("get_address"),
    GET_HEIGHT("get_height"),
    GET_BALANCE("getbalance"),
    GET_TRANSACTIONS("get_transfers"),
    GET_TRANSACTION("get_transaction"),
    GET_SEED("query_key"),
    SEND("transfer")
}