package org.karbo.wallet.rpc.responses

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class SeedResponse(val key: String) : ResponseResult
