@file:Suppress("unused", "PropertyName")

package org.karbo.wallet.rpc.requests

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class SeedRequest(val key_type: String = "mnemonic")