@file:Suppress("unused")

package org.karbo.wallet.rpc.requests

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class TransactionRequest(val tx_hash: String)