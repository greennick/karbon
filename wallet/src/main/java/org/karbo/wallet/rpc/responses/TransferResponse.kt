package org.karbo.wallet.rpc.responses

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class TransferResponse(val tx_hash: String) : ResponseResult
