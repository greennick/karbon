package org.karbo.wallet.rpc.responses

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class HeightResponse(val height: Int) : ResponseResult
