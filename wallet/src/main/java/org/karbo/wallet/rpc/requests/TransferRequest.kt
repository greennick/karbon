@file:Suppress("unused")

package org.karbo.wallet.rpc.requests

import com.squareup.moshi.JsonClass
import org.karbo.wallet.models.Destination

@JsonClass(generateAdapter = true)
internal class TransferRequest(
    val destinations: List<DestinationRequest>,
    val fee: Long,
    val mixin: Int,
    val unlock_time: Long = 0,
    val payment_id: String
)

@JsonClass(generateAdapter = true)
internal class DestinationRequest(val amount: Long, val address: String)

internal fun List<Destination>.toRequest() =
    map { DestinationRequest(it.amount.amount, it.address) }
