package org.karbo.wallet.rpc.responses

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class AddressResponse(val address: String) : ResponseResult
