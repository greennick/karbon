package org.karbo.wallet.rpc.responses

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class RpcError(val code: Int, val message: String)
