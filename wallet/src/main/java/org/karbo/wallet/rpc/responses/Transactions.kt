package org.karbo.wallet.rpc.responses

import com.squareup.moshi.JsonClass
import org.karbo.base.Krbshis
import org.karbo.wallet.models.Transaction

@JsonClass(generateAdapter = true)
internal class TransactionResponse(
    val address: String,
    val amount: Long,
    val blockIndex: Long,
    val fee: Long,
    val output: Boolean,
    val paymentId: String,
    val time: Long,
    val transactionHash: String,
    val txKey: String,
    val unlockTime: Long
)

@JsonClass(generateAdapter = true)
internal class TransactionsResponse(val transfers: List<TransactionResponse>) : ResponseResult

@JsonClass(generateAdapter = true)
internal class TransactionDetailsResponse(
    val transaction_details: TransactionResponse
) : ResponseResult

internal fun TransactionResponse.toTransaction() = Transaction(
    address,
    amount.Krbshis,
    blockIndex,
    fee.Krbshis,
    output,
    paymentId,
    time * 1000,
    transactionHash,
    txKey,
    unlockTime
)
