package org.karbo.wallet.configs

import java.io.File
import java.io.IOException
import java.util.*

internal class WalletConfig(
    private val directory: File,
    private val rpcConfig: RpcConfig
) {

    private var confFile: File? = null

    fun generate(password: String) = buildConfigFile(password)
        .also { confFile = it }

    fun reset() {
        confFile?.resetConfig()
    }

    private fun buildConfigFile(password: String): File {
        val randomName = UUID.randomUUID().toString()
        val f = File(directory, randomName)
        //just to sleep calmly
        f.resetConfig()

        val props = Properties().apply {
            put("password", password)
            put("rpc-user", rpcConfig.username)
            put("rpc-password", rpcConfig.password)
        }
        //use and close stream
        f.outputStream().use { outputStream ->
            props.store(outputStream, "")
        }

        return f
    }

    private fun File.resetConfig() {
        confFile = null
        try {
            delete()
            createNewFile()
            delete()
        } catch (ignore: IOException) {
        }
    }
}
