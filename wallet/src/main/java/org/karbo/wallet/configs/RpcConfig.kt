package org.karbo.wallet.configs

import java.util.*

internal class RpcConfig(
    val address: String = "127.0.0.1",
    val port: String = "15000",
    val username: String = UUID.randomUUID().toString(),
    val password: String = UUID.randomUUID().toString()
)
