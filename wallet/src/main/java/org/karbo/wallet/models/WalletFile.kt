package org.karbo.wallet.models

import java.io.File
import java.io.Serializable

data class WalletFile(
    val dir: String,
    val name: String,
    val extension: String = "wallet"
) : Serializable {
    val path: String = "$dir${File.separatorChar}$name.$extension"

    val exists: Boolean
        get() {
            val file = File(path)
            return file.exists() && file.length() > 0
        }
}
