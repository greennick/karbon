package org.karbo.wallet.models

import org.karbo.base.Krbshi
import java.io.Serializable

data class Transaction(
    val address: String,
    val amount: Krbshi,
    val blockIndex: Long,
    val fee: Krbshi,
    val isOutput: Boolean,
    val paymentId: String,
    val time: Long,
    val txHash: String,
    val key: String,
    val unlockTime: Long
) : Comparable<Transaction>, Serializable {

    override fun compareTo(other: Transaction) = when {
        time == 0L -> -1
        other.time == 0L -> 1
        time > other.time -> -1
        time < other.time -> 1
        else -> 0
    }
}
