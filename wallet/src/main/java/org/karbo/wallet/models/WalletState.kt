package org.karbo.wallet.models

sealed class WalletState {

    /**
     * Wallet is loaded successfully
     */
    object Initialized : WalletState()

    /**
     * Loading blockchain
     */
    data class Loading(val progress: Int) : WalletState()

    /**
     * Loaded ok. Received from daemon or loading finished
     */
    object Loaded : WalletState()

    /**
     * State to inform that daemon has not started or was forced to stop
     */
    object Disconnected : WalletState()
}
