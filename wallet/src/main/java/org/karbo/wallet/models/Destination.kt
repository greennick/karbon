package org.karbo.wallet.models

import org.karbo.base.Krbshi

/*
 * Created by Green-Nick on 16.11.2017.
 */

data class Destination(val amount: Krbshi, val address: String)
