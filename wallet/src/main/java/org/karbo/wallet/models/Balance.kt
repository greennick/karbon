package org.karbo.wallet.models

import org.karbo.base.Krbshi

data class Balance(
    val total: Krbshi,
    val locked: Krbshi,
    val unlocked: Krbshi
)
