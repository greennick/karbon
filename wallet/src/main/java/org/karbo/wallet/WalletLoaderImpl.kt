package org.karbo.wallet

import com.greennick.result.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import org.karbo.base.Logger
import org.karbo.wallet.configs.RpcConfig
import org.karbo.wallet.configs.WalletConfig
import org.karbo.wallet.models.WalletFile
import org.karbo.wallet.models.WalletState
import org.karbo.wallet.utils.parseInputAsState
import java.io.*
import java.util.concurrent.Executors
import kotlin.concurrent.thread
import kotlin.coroutines.resume

@FlowPreview
@ExperimentalCoroutinesApi
@Suppress("BlockingMethodInNonBlockingContext")
internal class WalletLoaderImpl(
    private val wallet: Wallet,
    private val coreFile: File,
    private val logsDir: File,
    private val rpcConfig: RpcConfig,
    private val walletConfig: WalletConfig,
    private val logger: Logger
) : WalletLoader {

    private val executorDispatcher = Executors.newCachedThreadPool().asCoroutineDispatcher()
    private val fileSeparator = File.separatorChar
    private val channel = ConflatedBroadcastChannel<WalletState>(WalletState.Disconnected)

    private var walletProcess: Process? = null
    override val currentState: WalletState get() = channel.value

    override suspend fun create(nodeUrl: String, wallet: WalletFile, password: String): Result<Unit, CreateError> =
        withContext(executorDispatcher) {
            logger.d("wallet name: ${wallet.name}")
            logger.d("password: $password")

            if (coreFile.invalid) return@withContext Result.error(CreateError.CoreNotFound)

            val params = arrayOf(
                coreFile.path,
                "--daemon-address", nodeUrl,
                "--log-file", "${logsDir.path}${fileSeparator}wallet.log",
                "--log-level", "4",
                "--generate-new-wallet", wallet.path,
                "--password", password
            )

            logger.d("params: ${params.contentToString()}")
            val process = Runtime.getRuntime().exec(params)
            val reader = InputStreamReader(process.inputStream)
            var error: Result<Unit, CreateError>? = null
            logger.d("create output:")
            reader.forEachLineSafe { line ->
                logger.d(line)
                when {
                    "Your wallet has been generated." in line -> process.destroy()
                    "Failed to initialize wallet" in line -> {
                        error = Result.error(CreateError.Error)
                        return@forEachLineSafe
                    }
                }
            }

            error ?: Result.success()
        }.doIfSuccess { logger.d("creating completed") }
            .onError { logger.e("error while creating: $it") }

    override suspend fun restore(
        nodeUrl: String,
        wallet: WalletFile,
        password: String,
        seed: String
    ): Result<Unit, RestoreError> = withContext(executorDispatcher) {
        logger.d("node url: $nodeUrl")
        logger.d("wallet name: ${wallet.name}")
        logger.d("password: $password")
        logger.d("seed: $seed")

        if (coreFile.invalid) return@withContext Result.error(RestoreError.CoreNotFound)

        val params = arrayOf(
            coreFile.path,
            "--daemon-address", nodeUrl,
            "--log-file", "${logsDir.path}${fileSeparator}wallet.log",
            "--log-level", "4",
            "--restore-deterministic-wallet",
            "--wallet-file", wallet.path,
            "--password", password,
            "--mnemonic-seed", seed
        )

        logger.d("params: ${params.contentToString()}")
        val process = Runtime.getRuntime().exec(params)
        val reader = InputStreamReader(process.inputStream)
        var error: Result<Unit, RestoreError>? = null
        logger.d("restore output:")
        reader.forEachLineSafe { line ->
            logger.d(line)
            when {
                "Your wallet has been generated." in line || "Wallet seems to be Ok." in line -> process.destroy()
                "Electrum-style word list failed verification" in line -> {
                    error = Result.error(RestoreError.InvalidRecoveryPhrase)
                    return@forEachLineSafe
                }
                "Wallet initialize failed: transaction parse error" in line ||
                        "Error: wallet failed to connect to daemon" in line -> {
                    error = Result.error(RestoreError.NoNodeConnection)
                    return@forEachLineSafe
                }
            }
        }

        error ?: Result.success()
    }.doIfSuccess { logger.d("restoring completed") }
        .onError { logger.e("error while restoring: $it") }

    override suspend fun init(nodeUrl: String, wallet: WalletFile, password: String): Result<Unit, InitError> =
        suspendCancellableCoroutine<Result<Unit, InitError>> { cont ->
            logger.d("node url: $nodeUrl")
            logger.d("wallet name: ${wallet.name}")
            logger.d("password: $password")

            if (walletProcess != null || currentState != WalletState.Disconnected) {
                cont.resume(Result.error(InitError.AlreadyRunning))
                return@suspendCancellableCoroutine
            }

            if (coreFile.invalid) {
                cont.resume(Result.error(InitError.CoreNotFound))
                return@suspendCancellableCoroutine
            }

            val configFile = walletConfig.generate(password)

            val params = arrayOf(
                coreFile.path,
                "--daemon-address", nodeUrl,
                "--log-file", "${logsDir.path}${fileSeparator}wallet.log",
                "--log-level", "4",
                "--wallet-file", wallet.path,
                "--config-file=${configFile.absolutePath}",
                "--rpc-bind-ip=${rpcConfig.address}",
                "--rpc-bind-port=${rpcConfig.port}"
            )

            logger.d("params: ${params.contentToString()}")

            thread {
                val process = Runtime.getRuntime().exec(params)
                walletProcess = process
                val reader = InputStreamReader(process.inputStream)
                logger.d("init output:")
                reader.forEachLineSafe { line ->
                    line.parseInputAsState()?.let(channel::offer)
                    logger.d(line)

                    when {
                        "Loaded ok" in line -> cont.resume(Result.success())
                        "Can't connect to daemon" in line -> {
                            process.destroy()
                            cont.resume(Result.error(InitError.NoNodeConnection))
                        }
                        ("Wallet initialize failed: can't load wallet file" in line &&
                                "check password" in line) ||
                                "Wallet initialize failed: The password is wrong" in line ||
                                "Wallet password not set" in line -> {
                            process.destroy()
                            cont.resume(Result.error(InitError.InvalidPassword))
                        }
                        "Wallet initialize failed" in line && "is not found" in line -> {
                            process.destroy()
                            cont.resume(Result.error(InitError.WalletNotFound))
                        }
                    }
                }
            }
        }.also { walletConfig.reset() }
            .doIfSuccess {
                channel.offer(WalletState.Initialized)
                logger.d("init completed")
            }.onError {
                setStopped()
                logger.e("error while init: $it")
            }

    override suspend fun stop() {
        if (currentState != WalletState.Disconnected) {
            wallet.save()
            walletProcess?.destroy()
            setStopped()
        }
    }

    override fun states() = channel.asFlow().distinctUntilChanged()

    private fun setStopped() {
        logger.d("setStopped, state: $currentState")
        walletProcess = null
        channel.offer(WalletState.Disconnected)
    }

    private val File.invalid get() = !exists() || length() == 0L

    private fun Reader.forEachLineSafe(
        onException: (IOException) -> Unit = {},
        action: (String) -> Unit
    ) {
        try {
            forEachLine(action)
        } catch (e: IOException) {
            onException(e)
        }
    }
}
