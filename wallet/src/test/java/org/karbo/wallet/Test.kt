package org.karbo.wallet

import org.junit.Test
import org.karbo.wallet.rpc.RpcMethod.*
import org.karbo.wallet.rpc.requests.TransactionRequest
import org.karbo.wallet.utils.json

class Test {
    @Test
    fun `just rpc method`() {
        val json = GET_ADDRESS.json()
        println("json: [$json]")
        assert("\"method\":\"${GET_ADDRESS.method}\"" in json)
    }

    @Test
    fun `rpc method with request`() {
        val txHash = "1654dfbgdf"
        val json = GET_TRANSACTION.json(TransactionRequest(txHash))
        println("json: [$json]")
        assert("\"method\":\"${GET_TRANSACTION.method}\"" in json)
        assert("\"params\":" in json)
        assert("\"tx_hash\":\"$txHash\"" in json)
    }
}