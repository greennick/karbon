import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm")
    kotlin("kapt")
}

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    kotlin("kotlin-stdlib-jdk7", Versions.kotlin)

    kapt("com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}")

    implementation(project(":base"))

    implementation("com.squareup.moshi:moshi:${Versions.moshi}")
    implementation("com.github.kittinunf.fuel:fuel:${Versions.fuel}")
    api("com.github.green-nick:Result:1.1")

    junit
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
