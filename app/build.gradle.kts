plugins {
    id("com.android.application")
    id("io.fabric")
    id("kotlin-android")
}

repositories {
    maven("https://jitpack.io")
}

android {
    compileSdkVersion(Sdk.compile)
    defaultConfig {
        applicationId = App.id
        minSdkVersion(Sdk.min)
        targetSdkVersion(Sdk.target)
        versionCode = App.code
        versionName = App.name
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        setProperty("archivesBaseName", "karbo-${App.name}")
        buildConfigFields
    }
    signingConfigs {
        getByName("debug") {
            keyAlias = "karbon_alias"
            keyPassword = "karbon123"
            storeFile = file("karbon_keys.jks")
            storePassword = "karbon"
        }
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            isShrinkResources = true
            isUseProguard = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
            signingConfig = null
            ext {
                set("alwaysUpdateBuildId", true)
                set("enableCrashlytics", true)
            }
        }
        getByName("debug") {
            signingConfig = null
            ext {
                set("alwaysUpdateBuildId", false)
                set("enableCrashlytics", false)
            }
        }
    }
    sourceSets.getByName("main") {
        jniLibs.srcDirs("lib")
    }

    flavorDimensions(Flavor.SIGNING, Flavor.ABI)

    productFlavors {
        create("signed") {
            setDimension(Flavor.SIGNING)
            signingConfig = signingConfigs.getByName("debug")
        }
        create("unsigned") {
            setDimension(Flavor.SIGNING)
            signingConfig = null
        }
        create("arm") {
            setDimension(Flavor.ABI)
            ndk {
                abiFilter("armeabi-v7a")
            }
            versionNameSuffix = "-arm_v7a:${App.code}"
        }
        create("arm64") {
            setDimension(Flavor.ABI)
            ndk {
                abiFilter("arm64-v8a")
            }
            versionCode = 10000 + App.code
            versionNameSuffix = "-arm64_v8a:$versionCode"
        }
        create("x86") {
            setDimension(Flavor.ABI)
            ndk {
                abiFilter("x86")
            }
            versionCode = 20000 + App.code
            versionNameSuffix = "-x86:$versionCode"
        }
        create("x86_64") {
            setDimension(Flavor.ABI)
            ndk {
                abiFilter("x86_64")
            }
            versionCode = 30000 + App.code
            versionNameSuffix = "-x86_64:$versionCode"
        }
    }
    variantFilter {
        val buildType = buildType.name
        val flavors = flavors.map { it.name.toLowerCase() }
        if ("unsigned" in flavors && buildType == "debug") {
            setIgnore(true)
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    (kotlinOptions as org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions).apply {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
    testOptions {
        unitTests.isIncludeAndroidResources = true
        unitTests.isReturnDefaultValues = true
    }
    packagingOptions {
        exclude("META-INF/result.kotlin_module")
    }
}

dependencies {
    kotlin("kotlin-stdlib-jdk7", Versions.kotlin)

    implementation(project(":base-app"))
    implementation(project(":main"))
    implementation(project(":launch"))
    implementation(project(":settings"))
    implementation(project(":node"))
    implementation(project(":wallet"))

    implementation("com.google.android.material:material:${Versions.material}")
    implementation("androidx.appcompat:appcompat:${Versions.appcompat}")
    implementation("androidx.work:work-runtime-ktx:${Versions.work}")

    implementation("ru.terrakok.cicerone:cicerone:${Versions.cicerone}")

    implementation("com.crashlytics.sdk.android:crashlytics:2.9.4@aar") {
        isTransitive = true
    }
}
