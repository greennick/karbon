package org.karbovanets.karbon.data.analytics

import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.karbo.app.data.analytics.Analytics
import org.karbo.node.SavedNodeAddressProvider
import org.karbo.node.models.Node
import org.karbo.app.data.UserSettings

/*
 * Created by greennick on 03.02.2018.
 */

class AnalyticsImpl(
    private val nodeAddressProvider: SavedNodeAddressProvider,
    private val settings: UserSettings
) : Analytics {

    override fun connectedToNode(node: Node) {
        GlobalScope.launch {
            val event = if (nodeAddressProvider.useCustomNode()) {
                getCustomNodeEvent()
            } else {
                getConnectedNodeEvent(node)
            }
            event.sendEvent()
        }
    }

    override fun restoredByMnemonic() = CustomEvent("restoring_by_mnemonic").sendEvent()

    override fun addressReceivedFromOut() = CustomEvent("uri_received").sendEvent()

    override fun addressReceivedByNfc() = CustomEvent("nfc_address_received").sendEvent()

    override fun addressSentByNfc() = CustomEvent("nfc_address_sent").sendEvent()

    override fun qrCodeScanned() = CustomEvent("qr_address_scanned").sendEvent()

    override fun fundsSent() = CustomEvent("funds_sent").sendEvent()

    override fun txHashCopied() = CustomEvent("tx_hash_copied").sendEvent()

    override fun paymentIdCopied() = CustomEvent("payment_id_copied").sendEvent()

    override fun faqClicked() = CustomEvent("help_clicked").sendEvent()

    override fun donateClicked() = CustomEvent("donate_clicked").sendEvent()

    private fun getConnectedNodeEvent(node: Node) = CustomEvent("connected_node")
        .putCustomAttribute("node_address", node.url)

    private fun getCustomNodeEvent() = CustomEvent("using_custom_node")

    private fun CustomEvent.sendEvent() {
        if (settings.allowSendingAnalytics()) {
            Answers.getInstance().logCustom(this)
        }
    }
}
