package org.karbovanets.karbon

import android.content.Context
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric

class ReleaseBuildTypeResolver : BuildTypeResolver {

    override fun resolve(context: Context) {
        Fabric.with(context, Crashlytics())
    }
}