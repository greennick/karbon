package org.karbovanets.karbon

import org.karbo.base.Logger
import org.karbo.app.data.analytics.Analytics
import org.karbovanets.karbon.data.analytics.AnalyticsImpl
import org.karbo.app.utils.ReleaseLogger
import org.koin.dsl.module

val buildVarModule = module {
    single<Analytics> {
        AnalyticsImpl(nodeAddressProvider = get(), settings = get())
    }
    single<BuildTypeResolver> { ReleaseBuildTypeResolver() }
    single<Logger> { ReleaseLogger() }
}
