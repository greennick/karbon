package org.karbovanets.karbon.utils

import org.koin.android.logger.AndroidLogger
import org.koin.core.logger.Level

val koinLogger = AndroidLogger(Level.DEBUG)