package org.karbovanets.karbon

import org.karbo.app.data.analytics.Analytics
import org.karbo.app.utils.AndroidDebugLogger
import org.karbo.base.Logger
import org.koin.dsl.module

val buildVarModule = module {
    single<Analytics> { object : Analytics {} }
    single<BuildTypeResolver> { object : BuildTypeResolver {} }
    single<Logger> { AndroidDebugLogger() }
}
