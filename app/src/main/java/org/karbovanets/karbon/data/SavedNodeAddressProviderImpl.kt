package org.karbovanets.karbon.data

/*
 * Created by Green-Nick on 11.11.2017.
 */

import android.annotation.SuppressLint
import android.content.SharedPreferences

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.karbo.app.data.UserSettings
import org.karbo.base.Logger
import org.karbo.node.SavedNodeAddressProvider

private const val KEY_SAVED_NODE_ADDRESS = "KEY_SAVED_NODE_ADDRESS"

class SavedNodeAddressProviderImpl(
    private val logger: Logger,
    private val preferences: SharedPreferences,
    private val settings: UserSettings
) : SavedNodeAddressProvider {

    override suspend fun getNodeAddress(): String? = withContext(Dispatchers.IO) {
        val address = if (useCustomNode()) {
            logger.d("use custom node")
            getCustomNodeAddress()
        } else {
            logger.d("use saved node")
            preferences.getString(KEY_SAVED_NODE_ADDRESS, "")
        }

        if (address.isEmpty()) null else address
    }

    override suspend fun useCustomNode() = withContext(Dispatchers.IO) {
        settings.useCustomNode()
    }

    override suspend fun getCustomNodeAddress() = withContext(Dispatchers.IO) {
        val host = settings.customNodeHost()
        val port = settings.customNodePort()
        when {
            host.isEmpty() -> ""
            port.isEmpty() -> host
            else -> "$host:$port"
        }
    }

    @SuppressLint("ApplySharedPref")
    override suspend fun saveAddress(address: String) = withContext<Unit>(Dispatchers.IO) {
        logger.d("saveAddress: $address")
        preferences.edit()
            .putString(KEY_SAVED_NODE_ADDRESS, address)
            .commit()
    }
}
