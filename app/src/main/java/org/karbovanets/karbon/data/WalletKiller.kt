package org.karbovanets.karbon.data

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.work.*
import org.karbo.app.data.UserSettings
import org.karbo.app.utils.log
import org.karbo.app.utils.loge
import org.karbo.launch.LaunchActivity
import org.karbo.wallet.WalletLoader
import org.karbovanets.karbon.SimpleActivityLifecycleListener
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.concurrent.TimeUnit

private const val WALLET_KILLER = "KILLER_TAG"

fun Application.registerKiller() {
    registerActivityLifecycleCallbacks(WalletKiller())
}

class WalletKiller : SimpleActivityLifecycleListener(), KoinComponent {
    private val killWalletRequest = OneTimeWorkRequest.Builder(WalletKillerWorker::class.java)
        .setInitialDelay(5, TimeUnit.MINUTES)
        .build()

    private var visibleActivities = 0

    override fun onActivityStarted(activity: Activity) {
        visibleActivities++
        log { "started ${activity::class.java.simpleName}, visible act: $visibleActivities" }
        WorkManager.getInstance(activity).cancelUniqueWork(WALLET_KILLER)
    }

    override fun onActivityStopped(activity: Activity) {
        visibleActivities--
        log { "stopped ${activity::class.java.simpleName}, visible act: $visibleActivities" }
        if (visibleActivities == 0) {
            if (activity.isFinishing) {
                log { "Closing application" }
            } else {
                log { "Application goes in background" }
            }
        }
        if (
            visibleActivities == 0
            && !activity.isFinishing
            && activity !is LaunchActivity
        ) {
            log { "working activity is no longer visible (stopped) but not finishing" }
            WorkManager.getInstance(activity)
                .enqueueUniqueWork(WALLET_KILLER, ExistingWorkPolicy.REPLACE, killWalletRequest)
        }
    }
}

class WalletKillerWorker(context: Context, workerParams: WorkerParameters) :
    CoroutineWorker(context, workerParams), KoinComponent {

    private val settings by inject<UserSettings>()
    private val walletLoader by inject<WalletLoader>()

    override suspend fun doWork(): Result {
        if (settings.useWalletKiller()) {
            loge { "Will stop wallet now!" }
            walletLoader.stop()
        } else {
            log { "Wallet killer is disabled" }
        }

        return Result.success()
    }
}
