package org.karbovanets.karbon

import android.content.Context

interface BuildTypeResolver {
    fun resolve(context: Context) {}
}