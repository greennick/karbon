package org.karbovanets.karbon

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import org.karbo.app.navigation.ScreensFactory
import org.karbo.launch.LaunchActivity
import org.karbo.launch.creation.CreateWalletFragment
import org.karbo.launch.loading.LoadingFragment
import org.karbo.launch.login.LoginFragment
import org.karbo.main.screens.main.*
import org.karbo.main.screens.qr.QrScannerActivity
import org.karbo.main.screens.receive.ReceiveActivity
import org.karbo.main.screens.seed.SeedActivity
import org.karbo.main.screens.seed.SeedArgs
import org.karbo.main.screens.send.SendActivity
import org.karbo.main.screens.send.SendArgs
import org.karbo.settings.UserSettingsActivity
import org.karbo.wallet.models.Transaction
import org.karbo.wallet.models.WalletFile
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.terrakok.cicerone.android.support.SupportAppScreen
import java.io.File

class ScreensFactoryImpl : ScreensFactory, KoinComponent {

    override fun launch() = activity<LaunchActivity>()

    override fun login() = fragment { get<LoginFragment>() }

    override fun walletCreation() = fragment { get<CreateWalletFragment>() }

    override fun exportWallet(wallet: WalletFile) = activity {
        val file = File(wallet.path)
        val uri = FileProvider.getUriForFile(
            it, it.applicationContext.packageName + ".walletprovider", file
        )
        Intent(Intent.ACTION_SEND).apply {
            type = "application/krb"
            putExtra(Intent.EXTRA_STREAM, uri)
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
    }

    override fun loading(wallet: WalletFile, password: String, seed: String) = fragment {
        LoadingFragment(wallet, password, seed)
    }

    override fun main(wallet: WalletFile) = activity<MainActivity> {
        putExtra(MainArgs.KEY_WALLET, wallet.name)
    }

    override fun mnemonicSeed(firstOpening: Boolean) = activity<SeedActivity> {
        putExtra(SeedArgs.KEY_FIRST_TIME, firstOpening)
    }

    override fun settings() = activity<UserSettingsActivity>()

    override fun qrScanner() = activity<QrScannerActivity>()

    override fun receiving() = activity<ReceiveActivity>()

    override fun sending(sendAddress: String) = activity<SendActivity> {
        putExtra(SendArgs.KEY_SEND_ADDRESS, sendAddress)
    }

    override fun shareText(subject: String, text: String) = activity {
        val sharingIntent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_SUBJECT, subject)
            putExtra(Intent.EXTRA_TEXT, text)
        }

        Intent.createChooser(sharingIntent, it.getText(R.string.button_receive_share))
    }

    override fun applicationSettings() = activity {
        Intent().apply {
            action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            data = Uri.fromParts("package", it.packageName, null)
        }
    }

    override fun faq() = activity {
        Intent(Intent.ACTION_VIEW, Uri.parse("http://karbo.cloud/help"))
    }

    override fun email() = activity {
        Intent(Intent.ACTION_SENDTO)
            .setData(Uri.parse("mailto:krbcoin@ukr.net"))
            .putExtra(Intent.EXTRA_SUBJECT, "Karbo Android App")
    }

    override fun transactionDetails(transaction: Transaction) = fragment {
        TransactionDetailsFragment(transaction)
    }

    @JvmName("filledIntent")
    private inline fun <reified T : Activity> activity(crossinline block: Intent.() -> Unit = {}) =
        activity { Intent(it, T::class.java).apply(block) }

    private fun activity(block: (Context) -> Intent) = object : SupportAppScreen() {

        override fun getActivityIntent(context: Context) = block(context)
    }

    private fun fragment(block: () -> Fragment) = object : SupportAppScreen() {

        override fun getFragment() = block()
    }
}
