package org.karbovanets.karbon

import android.app.Application
import org.karbovanets.karbon.data.registerKiller
import org.karbovanets.karbon.di.setupDI
import org.koin.android.ext.android.inject

/*
 * Created by pavlo.grynyk on 07-Nov-17.
 */

@Suppress("unused")
class KarboApp : Application() {
    private val resolver by inject<BuildTypeResolver>()

    override fun onCreate() {
        super.onCreate()
        setupDI()

        resolver.resolve(this)
        registerKiller()
    }
}
