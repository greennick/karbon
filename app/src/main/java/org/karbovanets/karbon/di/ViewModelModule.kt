package org.karbovanets.karbon.di

import org.karbo.launch.LaunchActivity
import org.karbo.launch.LaunchViewModel
import org.karbo.launch.creation.CreateWalletFragment
import org.karbo.launch.creation.CreateWalletViewModel
import org.karbo.launch.loading.LoadingFragment
import org.karbo.launch.loading.LoadingViewModel
import org.karbo.launch.login.LoginFragment
import org.karbo.launch.login.LoginViewModel
import org.karbo.main.screens.receive.ReceiveActivity
import org.karbo.main.screens.receive.ReceiveViewModel
import org.karbo.main.screens.seed.SeedActivity
import org.karbo.main.screens.seed.SeedViewModel
import org.karbo.main.screens.send.SendActivity
import org.karbo.main.screens.send.SendViewModel
import org.karbo.wallet.models.WalletFile
import org.koin.core.qualifier.named
import org.koin.dsl.module

val viewModelsModule = module {
    scopeFor(named<SeedActivity>()) { (showValidation: Boolean) ->
        SeedViewModel(
            firstShown = showValidation,
            wallet = get(),
            router = get(),
            passwordHolder = get(),
            clipboardManager = get()
        )
    }
    scopeFor(named<ReceiveActivity>()) {
        ReceiveViewModel(
            wallet = get(),
            router = get(),
            screens = get(),
            analytics = get(),
            clipboardManager = get()
        )
    }
    scopeFor(named<CreateWalletFragment>()) {
        CreateWalletViewModel(
            router = get(),
            strings = get(),
            screens = get(),
            walletsFoldersProvider = get()
        )
    }
    scopeFor(named<LoadingFragment>()) { (wallet: WalletFile, password: String, seed: String) ->
        LoadingViewModel(
            wallet = wallet,
            password = password,
            seed = seed,
            router = get(),
            screens = get(),
            analytics = get(),
            persistence = get(),
            nodeProvider = get(),
            receivedLink = get(),
            walletLoader = get(),
            passwordHolder = get(),
            networkManager = get(),
            connectedNodeHolder = getKoin().getOrCreateScope(
                CONNECTED_NODE_SCOPE.toString(),
                CONNECTED_NODE_SCOPE
            ).get()
        )
    }
    scopeFor(named<LoginFragment>()) {
        LoginViewModel(
            router = get(),
            strings = get(),
            screens = get(),
            walletsFoldersProvider = get(),
            selectedWallet = get(),
            persistenceManager = get()
        )
    }
    scopeFor(named<LaunchActivity>()) {
        LaunchViewModel(
            router = get(),
            screens = get(),
            coreInitializer = get(),
            persistenceManager = get()
        )
    }
    scopeFor(named<SendActivity>()) { (sendAddress: String) ->
        SendViewModel(
            sendAddress = sendAddress,
            wallet = get(),
            router = get(),
            screens = get(),
            analytics = get(),
            nodeService = get(),
            stringsRepo = get(),
            clipboardManager = get(),
            connectedNodeHolder = getKoin().getOrCreateScope(
                CONNECTED_NODE_SCOPE.toString(),
                CONNECTED_NODE_SCOPE
            ).get(),
            receivedLinkHandler = get()
        )
    }
}
