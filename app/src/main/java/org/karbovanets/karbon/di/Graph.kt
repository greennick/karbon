package org.karbovanets.karbon.di

import android.preference.PreferenceManager
import org.karbo.app.data.ConnectedNodeHolder
import org.karbo.app.data.UserSettings
import org.karbo.app.data.managers.*
import org.karbo.app.data.passwords.PasswordHolder
import org.karbo.app.navigation.Router
import org.karbo.app.navigation.ScreensFactory
import org.karbo.launch.data.NetworkManager
import org.karbo.launch.data.NetworkManagerImpl
import org.karbo.launch.data.wallet.*
import org.karbo.main.data.ClipboardManager
import org.karbo.main.data.ClipboardManagerImpl
import org.karbo.main.data.services.WalletServiceNotificationHandler
import org.karbo.node.NodeDI
import org.karbo.node.SavedNodeAddressProvider
import org.karbo.settings.data.UserSettingsImpl
import org.karbo.wallet.WalletHolder
import org.karbo.wallet.initWalletHolder
import org.karbovanets.karbon.ScreensFactoryImpl
import org.karbovanets.karbon.buildVarModule
import org.karbovanets.karbon.data.SavedNodeAddressProviderImpl
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone
import java.io.File

val CONNECTED_NODE_SCOPE = named("CONNECTED_NODE_SCOPE")

val modules = listOf(
    buildVarModule,
    NodeDI.nodeModule,
    module {
        single<SavedNodeAddressProvider> {
            SavedNodeAddressProviderImpl(logger = get(), preferences = get(), settings = get())
        }
    },
    module {
        single {
            val configDir = File(androidContext().applicationInfo.dataDir)
            val coreFile = get<WalletCoreInitializer>().coreFile
            val logsDir = get<WalletsFoldersProvider>().walletsFolder
            initWalletHolder(configDir, coreFile, logsDir, logger = get())
        }
        factory { get<WalletHolder>().wallet }
        factory { get<WalletHolder>().loader }
    },
    module {
        single<Cicerone<Router>> { Cicerone.create(Router()) }
        single { get<Cicerone<Router>>().navigatorHolder }
        single { get<Cicerone<Router>>().router }
        single<ScreensFactory> { ScreensFactoryImpl() }
    },
    module {
        single { PasswordHolder() }
        single { WalletsFoldersProvider(androidContext()) }
        single<PersistenceWalletManager> {
            PersistenceWalletManagerImpl(walletsFolderProvider = get())
        }
        single { WalletCoreInitializer(androidContext()) }
    },
    module {
        single<NetworkManager> { NetworkManagerImpl(androidContext()) }
        single { ReceivedLinkHandler() }
        single<StringsRepo> { StringsRepoImpl(androidContext()) }
        single { PreferenceManager.getDefaultSharedPreferences(androidContext()) }
        single<SelectedWallet> { SelectedWalletImpl(sharedPreferences = get()) }
        single<UserSettings> { UserSettingsImpl(androidContext()) }
        single<ClipboardManager> { ClipboardManagerImpl(androidContext(), analytics = get()) }
        single { WalletServiceNotificationHandler(androidContext()) }
    },
    viewModelsModule,
    fragmentsModule,
    mainScreenModule,
    module {
        scopeFor(CONNECTED_NODE_SCOPE) {
            ConnectedNodeHolder()
        }
    }
)
