package org.karbovanets.karbon.di

import org.karbo.main.data.*
import org.karbo.main.screens.main.MainActivity
import org.karbo.main.screens.main.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

val mainScreenModule = module {
    scope(named<MainActivity>()) {
        scoped<WalletLoadingService> { WalletLoadingServiceImpl(androidContext()) }
        scoped<WalletSavingService> { WalletSavingServiceImpl(androidContext()) }
        scoped {
            MainViewModel(
                router = get(),
                screens = get(),
                strings = get(),
                analytics = get(),
                clipboard = get(),
                walletLoader = get(),
                passwordHolder = get(),
                savedNodeProvider = get(),
                receivedLinkHandler = get(),
                walletSavingService = get(),
                connectedNodeHolder = getKoin().getOrCreateScope(
                    CONNECTED_NODE_SCOPE.toString(),
                    CONNECTED_NODE_SCOPE
                ).get(),
                walletLoadingService = get(),
                wallet = get()
            )
        }
    }
}
