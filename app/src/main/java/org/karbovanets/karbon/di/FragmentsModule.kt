package org.karbovanets.karbon.di

import org.karbo.app.arch.BaseViewModel
import org.karbo.app.arch.views.BaseFragment
import org.karbo.launch.creation.CreateWalletFragment
import org.karbo.launch.login.LoginFragment
import org.koin.core.qualifier.TypeQualifier
import org.koin.core.scope.Scope
import org.koin.dsl.module
import kotlin.reflect.KClass

val fragmentsModule = module {
    factory { LoginFragment(getVMFor(LoginFragment::class)) }
    factory { CreateWalletFragment(getVMFor(CreateWalletFragment::class)) }
}

private inline fun <reified T : BaseViewModel> Scope.getVMFor(clazz: KClass<out BaseFragment>): T =
    getKoin().getOrCreateScope(clazz.java.simpleName, TypeQualifier(clazz)).get()
