package org.karbovanets.karbon.di

import android.app.Application
import org.karbovanets.karbon.utils.koinLogger
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.definition.Definition
import org.koin.core.module.Module
import org.koin.core.qualifier.Qualifier

fun Application.setupDI() {
    startKoin {
        logger(koinLogger)
        androidContext(this@setupDI)
        modules(modules)
    }
    registerActivityLifecycleCallbacks(ConnectedNodeScope())
}

inline fun <reified T> Module.scopeFor(
    scopeName: Qualifier,
    qualifier: Qualifier? = null,
    override: Boolean = false,
    noinline definition: Definition<T>
) {
    scope(scopeName) {
        scoped(qualifier, override, definition)
    }
}
