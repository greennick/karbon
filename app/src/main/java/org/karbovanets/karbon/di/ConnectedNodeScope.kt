package org.karbovanets.karbon.di

import android.app.Activity
import org.karbo.app.utils.log
import org.karbo.main.screens.main.MainActivity
import org.karbovanets.karbon.SimpleActivityLifecycleListener
import org.koin.core.KoinComponent

class ConnectedNodeScope : SimpleActivityLifecycleListener(), KoinComponent {

    override fun onActivityDestroyed(activity: Activity) {
        log { "for activity $activity, finishing: ${activity.isFinishing}" }

        if (activity is MainActivity && activity.isFinishing) {
            getKoin().getOrCreateScope(CONNECTED_NODE_SCOPE.toString(), CONNECTED_NODE_SCOPE)
                .close()
        }
    }
}
