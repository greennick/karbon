plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

repositories {
    mavenCentral()
}

android {
    compileSdkVersion(Sdk.compile)
    defaultConfig {
        minSdkVersion(Sdk.min)
        targetSdkVersion(Sdk.target)
        versionCode = App.code
        versionName = App.name

        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isUseProguard = true
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    kotlin("kotlin-stdlib-jdk7", Versions.kotlin)

    kapt("com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}")

    implementation(project(":base"))

    implementation("org.koin:koin-android:${Versions.koin}")
    implementation("com.squareup.moshi:moshi:${Versions.moshi}")
    implementation("com.github.kittinunf.fuel:fuel:${Versions.fuel}")

    junit
}
