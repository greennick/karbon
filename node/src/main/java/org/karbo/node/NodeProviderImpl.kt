package org.karbo.node

/*
 * Created by Green-Nick on 11.11.2017.
 */

import org.karbo.base.Logger
import org.karbo.node.models.Node
import org.karbo.node.networking.NodeConnectionTester

internal class NodeProviderImpl(
    private val savedNodeAddressProvider: SavedNodeAddressProvider,
    private val addressesProvider: AddressesProvider,
    private val nodeService: NodeService,
    private val nodeTester: NodeConnectionTester,
    private val logger: Logger
) : NodeProvider {

    override suspend fun getNode(): Node? {
        val savedAddress = savedNodeAddressProvider.getNodeAddress()
        logger.d("saved node address: $savedAddress")

        val address: String = when {
            savedAddress == null -> {
                logger.e("error getting saved, load from provider")
                getAvailableNodeAddressFromProvider() ?: return null
            }
            nodeTester.isAvailable(savedAddress) -> savedAddress
            else -> getAvailableNodeAddressFromProvider() ?: return null
        }

        save(address)
        val node = nodeService.getNodeWithFee(address) ?: return null
        val nodeInfo = nodeService.getNodeInfo(node.url) ?: return null
        return node.copy(info = nodeInfo)
    }

    override suspend fun clearCache() {
        save("")
    }

    private suspend fun getAvailableNodeAddressFromProvider(): String? =
        addressesProvider.addresses().find { nodeTester.isAvailable(it) }

    private suspend fun save(address: String) {
        logger.d("save node: $address")
        savedNodeAddressProvider.saveAddress(address)
    }
}
