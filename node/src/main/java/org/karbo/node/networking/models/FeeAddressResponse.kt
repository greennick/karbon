package org.karbo.node.networking.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class FeeAddressResponse(
    val status: String,
    val fee_address: String
)
