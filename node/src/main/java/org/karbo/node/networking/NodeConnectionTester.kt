package org.karbo.node.networking

/*
 * Created by Green-Nick on 11.11.2017.
 */

import org.karbo.node.NodeService

internal class NodeConnectionTester(private val nodeService: NodeService) {

    suspend fun isAvailable(address: String): Boolean = nodeService.getNodeInfo(address) != null
}
