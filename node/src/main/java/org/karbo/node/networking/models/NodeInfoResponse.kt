package org.karbo.node.networking.models

import com.squareup.moshi.JsonClass
import org.karbo.base.Krbshi
import org.karbo.node.models.NodeInfo

@JsonClass(generateAdapter = true)
internal class NodeInfoResponse(
    val status: String,
    val last_known_block_index: Int = 0,
    val min_tx_fee: Long? = null,
    val min_fee: Long? = null
) {
    val isOk get() = status.isNotEmpty() && status.equals("ok", true)
}

internal fun NodeInfoResponse.toNodeInfo() = NodeInfo(
    lastKnownBlockIndex = last_known_block_index,
    minTxFee = Krbshi(min_tx_fee ?: min_fee ?: 0) * 2
)
