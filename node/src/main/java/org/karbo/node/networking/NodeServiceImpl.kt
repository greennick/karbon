package org.karbo.node.networking

/*
 * Created by Green-Nick on 02.12.2017.
 */

import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.core.response
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.karbo.base.Logger
import org.karbo.node.NodeService
import org.karbo.node.models.Node
import org.karbo.node.models.NodeInfo
import org.karbo.node.networking.models.FeeAddressResponse
import org.karbo.node.networking.models.NodeInfoResponse
import org.karbo.node.networking.models.toNodeInfo
import org.karbo.node.utils.moshi

internal class NodeServiceImpl(private val logger: Logger) : NodeService {

    override suspend fun getNodeWithFee(nodeAddress: String): Node? {
        val (request, response, result) = withContext(Dispatchers.IO) {
            "http://$nodeAddress/feeaddress".httpGet()
                .response(moshi<FeeAddressResponse>())
        }

        log(request, response, result)

        return when (result) {
            is Result.Success -> Node(nodeAddress, result.value.fee_address)
            is Result.Failure -> null
        }
    }

    override suspend fun getNodeInfo(nodeAddress: String): NodeInfo? {
        val (request, response, result) = try {
            withContext(Dispatchers.IO) {
                "http://$nodeAddress/getinfo".httpGet().response(moshi<NodeInfoResponse>())
            }
        } catch (e: Exception) {
            return null
        }

        log(request, response, result)

        return when (result) {
            is Result.Success -> if (result.value.isOk) result.value.toNodeInfo() else null
            is Result.Failure -> null
        }
    }

    private fun <V : Any> log(request: Request, response: Response, result: Result<V, FuelError>) {
        logger.d(request.toString())
        logger.d(response.toString())

        when (result) {
            is Result.Success -> logger.d("success: ${result.value}")
            is Result.Failure -> logger.e("error sending get", result.error.exception)
        }
    }
}
