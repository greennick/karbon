package org.karbo.node.utils

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.squareup.moshi.Moshi

internal val moshi: Moshi = Moshi.Builder().build()

internal inline fun <reified T : Any> moshi() = object : ResponseDeserializable<T> {

    override fun deserialize(content: String) = moshi.adapter(T::class.java).fromJson(content)
}
