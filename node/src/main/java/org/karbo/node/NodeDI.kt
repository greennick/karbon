package org.karbo.node

import org.karbo.node.networking.NodeConnectionTester
import org.karbo.node.networking.NodeServiceImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

object NodeDI {

    val nodeModule = module {
        single<NodeService> { NodeServiceImpl(logger = get()) }
        single { NodeConnectionTester(nodeService = get()) }
        single { AddressesProvider(androidContext(), logger = get()) }
        single<NodeProvider> {
            NodeProviderImpl(
                savedNodeAddressProvider = get(),
                addressesProvider = get(),
                nodeService = get(),
                nodeTester = get(),
                logger = get()
            )
        }
    }
}
