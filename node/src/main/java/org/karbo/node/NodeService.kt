package org.karbo.node

import org.karbo.node.models.Node
import org.karbo.node.models.NodeInfo

interface NodeService {

    suspend fun getNodeWithFee(nodeAddress: String): Node?

    suspend fun getNodeInfo(nodeAddress: String): NodeInfo?
}
