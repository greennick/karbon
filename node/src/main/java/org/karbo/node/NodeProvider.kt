package org.karbo.node

/*
 * Created by Green-Nick on 11.11.2017.
 */

import org.karbo.node.models.Node

interface NodeProvider {

    suspend fun getNode(): Node?

    suspend fun clearCache()
}
