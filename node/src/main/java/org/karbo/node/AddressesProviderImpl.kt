package org.karbo.node

import android.content.Context
import com.github.kittinunf.fuel.httpGet
import com.squareup.moshi.Types
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.karbo.base.Logger
import org.karbo.node.utils.moshi

private const val REMOTE_NODES_LIST_URL = "https://karbo-mobile.firebaseio.com/list.json"

internal class AddressesProvider(val context: Context, val logger: Logger) {
    private val adapter = moshi.adapter<List<String?>>(Types.newParameterizedType(List::class.java, String::class.java))

    suspend fun addresses(): List<String> {
        val local = loadLocal()

        return try {
            val result = withContext(Dispatchers.IO) { loadFromFirebase() }
                .also { logger.d("nodes from Firebase: $it") }
                .toSet() + local

            result.toList()
        } catch (e: Exception) {
            logger.e("something went wrong", e)
            local
        }
    }

    private fun loadLocal() = context.resources
        .getStringArray(R.array.servers)
        .toList()
        .shuffled()

    private fun loadFromFirebase(): List<String> {
        val (_, _, remoteRaw) = REMOTE_NODES_LIST_URL.httpGet().responseString()
        return (adapter.fromJson(remoteRaw.get()) ?: emptyList())
            .filterNotNull()
            .filter(String::isNotEmpty)
            .shuffled()
    }
}
