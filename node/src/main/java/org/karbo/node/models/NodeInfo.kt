package org.karbo.node.models

import org.karbo.base.Krbshi
import java.io.Serializable

/**
 * Copyright by Karbo(ex Karbovanets)
 * @author bazted
 */
data class NodeInfo(
    val lastKnownBlockIndex: Int,
    val minTxFee: Krbshi
) : Serializable
