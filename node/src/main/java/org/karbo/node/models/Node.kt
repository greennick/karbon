package org.karbo.node.models

import org.karbo.base.*
import java.io.Serializable
import kotlin.math.roundToLong

val MAX_NODE_FEE = 10.0.Krbs
private const val NODE_FEE_PERCENT = 0.0025 // 0.25%

data class Node(
    val url: String,
    val feeAddress: String = "",
    val info: NodeInfo? = null
) : Serializable {

    val isFree get() = feeAddress.isEmpty()
}

fun Node.calculateFeeFor(sum: Krbshi): Krbshi =
    if (isFree) 0.Krbshis else min(sum * NODE_FEE_PERCENT, MAX_NODE_FEE)

fun Node.calculateRemForSumAfterFee(sum: Krbshi): Krbshi {
    if (isFree) return sum

    val rem = (sum.amount / (1 + NODE_FEE_PERCENT)).roundToLong().Krbshis
    val remAfterMaxFee = sum - MAX_NODE_FEE

    return when {
        remAfterMaxFee > rem -> remAfterMaxFee
        rem + calculateFeeFor(rem) > sum -> Krbshi(rem.amount - 1) //due to rounding precision
        else -> rem
    }
}
