package org.karbo.node

/*
 * Created by Green-Nick on 11.11.2017.
 */

interface SavedNodeAddressProvider {

    /**
     * Will try to get custom or last saved node address
     * depending on #useCustomNode
     *
     * @return source of node address
     */
    suspend fun getNodeAddress(): String?

    suspend fun getCustomNodeAddress(): String

    suspend fun useCustomNode(): Boolean

    suspend fun saveAddress(address: String)
}
