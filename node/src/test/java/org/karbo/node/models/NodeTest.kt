package org.karbo.node.models

import org.junit.Test
import org.karbo.base.*
import kotlin.random.Random

class NodeTest {
    private val freeNode get() = Node("")
    private val feeNode get() = Node("", "fee address")

    @Test
    fun `free node returns 0 fee`() = assert(freeNode.calculateFeeFor(1.0.Krbs) == 0.Krbshis)

    @Test
    fun `node with fee returns 0,25% fee`() {
        val sum = 1.0.Krbs
        val fee = feeNode.calculateFeeFor(sum)
        println("fee: ${fee.stringAsKarbo()}")

        assert(sum * 0.0025 == fee)
    }

    @Test
    fun `node with fee cannot have fee more than 10 Krb`() {
        val sum = 10_000.0.Krbs
        val fee = feeNode.calculateFeeFor(sum)
        println("fee: ${fee.stringAsKarbo()}")

        assert(fee == MAX_NODE_FEE)
    }

    @Test
    fun `free node returns remaining equal to sum`() {
        val sum = 1.0.Krbs
        val rem = freeNode.calculateRemForSumAfterFee(sum)
        assert(rem == sum)
    }

    @Test
    fun `node with fee returns remaining which with fee returns sum`() {
        repeat(100) {
            val sum = Random.nextDouble(0.001, 100_000.0).Krbs
            println("sum: ${sum.stringAsKarbo()}")

            val rem = feeNode.calculateRemForSumAfterFee(sum)
            println("rem: ${rem.stringAsKarbo()}")

            val total = rem + feeNode.calculateFeeFor(rem)
            println("tot: ${total.stringAsKarbo()}")
            println()

            assert(total == sum)
        }
    }
}
