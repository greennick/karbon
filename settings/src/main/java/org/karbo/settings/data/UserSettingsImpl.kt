package org.karbo.settings.data

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import org.karbo.app.data.UserSettings
import org.karbo.settings.R

class UserSettingsImpl(private val context: Context) : UserSettings {

    private val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    override fun allowSendingAnalytics(): Boolean =
        preferences.getBoolean(R.string.KEY_ALLOW_ANALYTICS.asString(), true)

    override fun useWalletKiller(): Boolean =
        preferences.getBoolean(R.string.KEY_USE_WALLET_KILLER.asString(), true)

    override fun useCustomNode(): Boolean =
        preferences.getBoolean(R.string.KEY_NODE_CUSTOM_USE.asString(), false)

    override fun customNodeHost(): String =
        preferences.getString(R.string.KEY_NODE_CUSTOM_IP.asString(), "") as String

    override fun customNodePort(): String =
        preferences.getString(R.string.KEY_NODE_CUSTOM_PORT.asString(), "") as String

    private fun Int.asString() = context.getString(this)
}
