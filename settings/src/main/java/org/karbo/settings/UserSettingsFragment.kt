package org.karbo.settings

import android.content.SharedPreferences
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import org.karbo.app.utils.log
import org.karbo.app.utils.toast
import org.karbo.wallet.WalletLoader
import org.karbo.wallet.models.WalletState
import org.koin.android.ext.android.inject

class UserSettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    private val walletLoader by inject<WalletLoader>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        updateSummaryFor(R.string.KEY_NODE_CUSTOM_IP)
        updateSummaryFor(R.string.KEY_NODE_CUSTOM_PORT)
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        log { "for $key is ${sharedPreferences.all}" }
        when (key) {
            R.string.KEY_NODE_CUSTOM_IP.string() -> updateSummaryFor(R.string.KEY_NODE_CUSTOM_IP)
            R.string.KEY_NODE_CUSTOM_PORT.string() -> updateSummaryFor(R.string.KEY_NODE_CUSTOM_PORT)
        }
        checkForRestarting(sharedPreferences, key)
    }

    private fun updateSummaryFor(@StringRes stringId: Int) {
        val key = getString(stringId)
        findPreference<Preference>(key)?.apply {
            summary = sharedPreferences?.getString(key, "")
        }
    }

    private fun checkForRestarting(sharedPreferences: SharedPreferences, key: String) {
        val walletState = walletLoader.currentState
        log { "wallet state: $walletState" }
        if (walletState == WalletState.Disconnected) return

        val useCustomNode = sharedPreferences
            .getBoolean(R.string.KEY_NODE_CUSTOM_USE.string(), false)

        if (key == R.string.KEY_NODE_CUSTOM_USE.string() || (key.startsWith("KEY_NODE") && useCustomNode)) {
            toast(R.string.pref_reopen_to_apply)
        }
    }

    private fun Int.string() = getString(this)
}
