package org.karbo.base

import org.junit.Test

class CurrencyTest {

    @Test
    fun `default Krbshi constructor matches 0 amount`() = assert(Krbshi().amount == 0L)

    @Test
    fun `1 as Double matches 1 Krb (1 000 000 000 000 Krbshis)`() = assert(1.0.Krbs == KRBSHIS_IN_KRB.Krbshis)
}
