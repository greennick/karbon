package org.karbo.base

import kotlin.math.roundToLong

const val KRBSHIS_IN_KRB = 1_000_000_000_000L

inline class Krbshi(val amount: Long = 0) {

    operator fun plus(another: Krbshi) = Krbshi(amount + another.amount)

    operator fun minus(another: Krbshi) = Krbshi(amount - another.amount)

    operator fun times(multiplier: Long) = Krbshi(amount * multiplier)

    operator fun times(multiplier: Int) = Krbshi(amount * multiplier)

    operator fun times(multiplier: Float) = Krbshi((amount * multiplier).roundToLong())

    operator fun times(multiplier: Double) = Krbshi((amount * multiplier).roundToLong())

    operator fun compareTo(another: Krbshi) = amount.compareTo(another.amount)
}

val Int.Krbshis get() = Krbshi(this.toLong())

val Long.Krbshis get() = Krbshi(this)

val Float.Krbs get() = (this * KRBSHIS_IN_KRB).roundToLong().Krbshis

val Double.Krbs get() = (this * KRBSHIS_IN_KRB).roundToLong().Krbshis

fun max(one: Krbshi, another: Krbshi): Krbshi = kotlin.math.max(one.amount, another.amount).Krbshis

fun min(one: Krbshi, another: Krbshi): Krbshi = kotlin.math.min(one.amount, another.amount).Krbshis

fun Krbshi.stringAsKarbo() = (amount / KRBSHIS_IN_KRB.toDouble()).toString()
