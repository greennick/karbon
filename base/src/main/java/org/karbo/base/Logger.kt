package org.karbo.base

interface Logger {
    fun log(message: String) {}

    fun d(message: String) {}

    fun e(message: String) {}

    fun e(message: String, error: Throwable) {}
}
