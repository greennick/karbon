import org.gradle.kotlin.dsl.DependencyHandlerScope

fun DependencyHandlerScope.testImpl(dependency: Any) =
    add("testImplementation", dependency)

fun DependencyHandlerScope.kapt(dependency: Any) =
    add("kapt", dependency)