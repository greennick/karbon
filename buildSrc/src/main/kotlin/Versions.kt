@file:Suppress("unused")

object Versions {
    const val kotlin = "1.3.72"
    const val coroutines = "1.3.4"
    const val koin = "2.1.3"
    const val moshi = "1.8.0"
    const val properties = "1.11"
    const val propertiesAndr = "3.0"
    const val cicerone = "5.1.0"
    const val appcompat = "1.1.0"
    const val fragment = "1.2.2"
    const val constraint = "2.0.0-beta4"
    const val material = "1.2.0-alpha05"
    const val work = "2.3.0"
    const val mockito = "2.23.4"
    const val fuel = "2.2.1"
}

object Sdk {
    const val compile = 28
    const val min = 21
    const val target = 28
}

object App {
    const val code = 22
    const val id = "org.karbo.karbon"
    const val name = "1.4.1"
}

object Flavor {
    const val SIGNING = "signing"
    const val ABI = "abi"
}
