import org.gradle.kotlin.dsl.DependencyHandlerScope

val DependencyHandlerScope.junit get() = testImpl("junit:junit:4.12")

val DependencyHandlerScope.mockito get() = testImpl("org.mockito:mockito-core:${Versions.mockito}")

val DependencyHandlerScope.mockitoInline get() = testImpl("org.mockito:mockito-inline:${Versions.mockito}")

val DependencyHandlerScope.mockitoKotlin get() = testImpl("com.nhaarman.mockitokotlin2:mockito-kotlin:2.1.0")

val DependencyHandlerScope.koinTest get() = testImpl("org.koin:koin-test:${Versions.koin}")
