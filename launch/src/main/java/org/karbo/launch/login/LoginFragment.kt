package org.karbo.launch.login

import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearSnapHelper
import com.github.greennick.properties.android.actionListener
import com.github.greennick.properties.android.onClick
import com.github.greennick.properties.androidx.*
import com.github.greennick.properties.generic.MutableProperty
import com.github.greennick.properties.generic.map
import kotlinx.android.synthetic.main.fragment_login.*
import org.karbo.app.arch.views.BaseFragment
import org.karbo.app.utils.*
import org.karbo.launch.R

class LoginFragment(override val viewModel: LoginViewModel) : BaseFragment(R.layout.fragment_login) {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        requireActivity().window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val layoutManager = PickerLayoutManager(requireContext()).apply { scaleDownBy = 0.5f }

        with(viewModel) {
            val scrollOffset = R.dimen.wallet_list_height.asPx() / 2 -
                    R.dimen.wallet_list_item_height.asPx() / 2 -
                    R.dimen.wallet_list_padding.asPx()
            bindScrollPosition(layoutManager, scrollOffset, selectedWalletIndex)

            open_wallet.onClick(::loginClicked)
            field_password.actionListener {
                if (it == EditorInfo.IME_ACTION_GO) loginClicked()
            }

            bindTextBidirectionally(field_password, password)
            bindError(field_password_wrapper, passwordError)

            val adapter = WalletAdapter(selectedWalletIndex::set)
            bindAdapter(wallets, adapter)

            LinearSnapHelper().attachToRecyclerView(wallets_list)
            wallets_list.layoutManager = layoutManager
            wallets_list.adapter = adapter

            setupDeleteDialog()

            doIfDebug {
                debug_info.visibility = View.VISIBLE
                bindText(
                    debug_info,
                    selectedWalletName.map { "position: ${selectedWalletIndex.value}, selected: [$it]" })
            }
        }

        field_password.requestFocus()
        menu.setOnClickListener(::showMenu)
    }

    private fun setupDeleteDialog() = with(viewModel) {
        val validationView = inflate(R.layout.dialog_textfield_view)
        val input = validationView.findViewById<TextView>(R.id.input)
        bindTextBidirectionally(input, enteredWalletName)

        val dialog = alertDialog {
            titleId = R.string.action_delete_wallet
            messageId = R.string.delete_wallet_warning
            this.view = validationView
            positive(R.string.delete_wallet_positive, ::removeWalletAgreed)
            negative(R.string.delete_wallet_negative, ::removeWalletDenied)
        }
        bindNonNull(selectedWalletName) {
            dialog.setMessage(requireContext().getString(R.string.delete_wallet_warning, it))
        }
        bindVisibilityBidirectionally(dialog, showDeleteDialog)
    }

    private fun showMenu(view: View) {
        PopupMenu(requireContext(), view).apply {
            val inflater: MenuInflater = menuInflater
            inflater.inflate(R.menu.screen_login, menu)
            doIfDebug {
                menu.findItem(R.id.action_generate_wallet).isVisible = true
            }
            setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.action_create_wallet -> viewModel.createWalletClicked()
                    R.id.action_export_wallet -> viewModel.exportWalletClicked()
                    R.id.action_remove_wallet -> viewModel.removeWalletClicked()
                    R.id.action_settings -> viewModel.settingsClicked()
                    R.id.action_generate_wallet -> viewModel.generateWallet()
                }
                true
            }
            show()
        }
    }

    private fun Int.asPx(): Int = resources.getDimensionPixelSize(this)

    private fun bindScrollPosition(
        layoutManager: PickerLayoutManager,
        scrollOffset: Int = 0,
        property: MutableProperty<Int?>
    ) {
        layoutManager.onScrollStopListener = {
            property.value = layoutManager.getPosition(it)
        }
        bindNonNull(property) {
            if (layoutManager.lastScrolledPosition == it) return@bindNonNull
            layoutManager.scrollToPositionWithOffset(it, scrollOffset)
        }
    }
}
