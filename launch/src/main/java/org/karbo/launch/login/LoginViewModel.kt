package org.karbo.launch.login

import com.github.greennick.properties.emptyProperty
import com.github.greennick.properties.generic.*
import com.github.greennick.properties.propertyOf
import com.greennick.result.onError
import com.greennick.result.onSuccess
import kotlinx.coroutines.launch
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.data.managers.StringsRepo
import org.karbo.app.navigation.Router
import org.karbo.app.navigation.ScreensFactory
import org.karbo.app.utils.doIfDebug
import org.karbo.app.utils.log
import org.karbo.launch.R
import org.karbo.launch.data.*
import org.karbo.launch.data.wallet.*
import org.karbo.wallet.models.WalletFile
import kotlin.random.Random

class LoginViewModel(
    private val persistenceManager: PersistenceWalletManager,
    private val screens: ScreensFactory,
    private val selectedWallet: SelectedWallet,
    private val walletsFoldersProvider: WalletsFoldersProvider,
    private val router: Router,
    private val strings: StringsRepo
) : BaseViewModel(router) {

    val wallets = propertyOf(emptyList<WalletFile>())
    val selectedWalletIndex = emptyProperty<Int>()
    val password = propertyOf("")
    val passwordError = emptyProperty<String>()
    val showDeleteDialog = propertyOf(false)
    val enteredWalletName = propertyOf("")
    val selectedWalletName = wallets.zipWith(selectedWalletIndex) { wallets, index ->
        if (index == null || wallets.size <= index) "" else wallets[index].name
    }

    override fun screenOpened() {
        loadWallets()
        showDeleteDialog.subscribeOnFalse { enteredWalletName.value = "" }
        doIfDebug {
            selectedWalletName {
                log { "selected position: ${selectedWalletIndex.value} = $it" }
                setTestPassword(it)
            }
        }
    }

    private fun loadWallets() = launch {
        val loaded = persistenceManager.getWallets()
        log { "loaded wallets: $loaded" }
        if (loaded.isEmpty()) {
            router.replaceScreen(screens.walletCreation())
            return@launch
        }
        val previousWallets = wallets.value
        wallets.value = loaded
        if (previousWallets.isNotEmpty()) {
            selectedWalletIndex.value = 0
        } else {
            setSelection(loaded)
        }
    }

    private suspend fun setSelection(loaded: List<WalletFile>) {
        val saved = selectedWallet.selectedWalletName()
        log { "previously saved: [$saved]" }
        val selected = loaded.indexOfFirst { it.name == saved }
        selectedWalletIndex.value = if (selected != -1) selected else 0
    }

    private fun setTestPassword(walletName: String) {
        password.value = if (walletName == "test") "qwerty" else ""
    }

    fun createWalletClicked() = router.navigateTo(screens.walletCreation())

    fun settingsClicked() = router.navigateTo(screens.settings())

    fun exportWalletClicked() {
        val wallet = wallets[selectedWalletIndex.value ?: return]
        router.showChooser(strings.string(R.string.title_export_wallet), screens.exportWallet(wallet))
    }

    fun removeWalletClicked() {
        showDeleteDialog.value = true
    }

    fun removeWalletDenied() {
        showDeleteDialog.value = false
    }

    fun removeWalletAgreed() {
        val selected = wallets[selectedWalletIndex.value ?: return]
        if (enteredWalletName.value == selected.name) {
            persistenceManager.removeWallet(selected)
            router.showMessage(R.string.delete_wallet_done)
            loadWallets()
        }
        showDeleteDialog.value = false
    }

    fun loginClicked() {
        passwordError.value = null
        val selectedWallet = wallets[selectedWalletIndex.value ?: return]
        launch {
            this@LoginViewModel.selectedWallet.saveSelectedWalletName(selectedWallet.name)
        }

        log { "will try to login with $selectedWallet and [${password.value}]" }

        verifyPassword(password.value)
            .onSuccess { performLogin(selectedWallet, it) }
            .onError {
                passwordError.value = when (it) {
                    BadPassword.EmptyPassword -> string(R.string.error_password_empty)
                    BadPassword.TooShort -> string(R.string.error_password_short)
                    BadPassword.DeniedSymbols -> deniedSymbolsErrorMessage(strings)
                }
            }
    }

    fun generateWallet() = doIfDebug {
        var name: String
        do {
            name = "My wallet ${Random.nextInt(100_000)}"
        } while (wallets.value.any { it.name == name })
        router.navigateTo(screens.loading(WalletFile(walletsFoldersProvider.walletsFolder.path, name), "1234"))
    }

    private fun performLogin(wallet: WalletFile, password: String) {
        val screen = screens.loading(wallet, password)
        router.navigateTo(screen)
    }

    private fun string(id: Int) = strings.string(id)
}
