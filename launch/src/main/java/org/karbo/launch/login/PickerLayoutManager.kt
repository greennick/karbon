package org.karbo.launch.login

import android.annotation.SuppressLint
import android.content.Context
import android.view.View

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.abs
import kotlin.math.min

@SuppressLint("WrongConstant")
class PickerLayoutManager(
    context: Context,
    @RecyclerView.Orientation orientation: Int = VERTICAL,
    reverseLayout: Boolean = false
) : LinearLayoutManager(context, orientation, reverseLayout) {

    var scaleDownBy = 0.66f
    var scaleDownDistance = 0.9f
    var isChangeAlpha = true
    var lastScrolledPosition = 0
        private set

    var onScrollStopListener: (View) -> Unit = {}

    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State) {
        super.onLayoutChildren(recycler, state)

        if (orientation == HORIZONTAL) {
            scaleDownViewHorizontal()
        } else {
            scaleDownViewVertical()
        }
    }

    override fun scrollHorizontallyBy(dx: Int, recycler: RecyclerView.Recycler?, state: RecyclerView.State?): Int =
        if (orientation == HORIZONTAL) {
            val scrolled = super.scrollHorizontallyBy(dx, recycler, state)
            scaleDownViewHorizontal()
            scrolled
        } else 0

    override fun scrollVerticallyBy(dy: Int, recycler: RecyclerView.Recycler?, state: RecyclerView.State?): Int =
        if (orientation == VERTICAL) {
            val scrolled = super.scrollVerticallyBy(dy, recycler, state)
            scaleDownViewVertical()
            scrolled
        } else 0

    override fun onScrollStateChanged(state: Int) {
        super.onScrollStateChanged(state)
        if (state != 0) return
        var selected = 0
        var lastHeight = 0f
        for (i in 0 until childCount) {
            if (lastHeight < requireChild(i).scaleY) {
                lastHeight = requireChild(i).scaleY
                selected = i
            }
        }
        lastScrolledPosition = selected
        onScrollStopListener(requireChild(selected))
    }

    override fun scrollToPositionWithOffset(position: Int, offset: Int) {
        lastScrolledPosition = position
        super.scrollToPositionWithOffset(position, offset)
    }

    private fun scaleDownViewHorizontal() {
        val mid = width / 2.0f
        val unitScaleDownDist = scaleDownDistance * mid
        for (i in 0 until childCount) {
            val child = requireChild(i)
            val childMid = (getDecoratedLeft(child) + getDecoratedRight(child)) / 2.0f
            val scale =
                1.0f + -1 * scaleDownBy * min(unitScaleDownDist, abs(mid - childMid)) / unitScaleDownDist
            child.scaleX = scale
            child.scaleY = scale
            if (isChangeAlpha) {
                child.alpha = scale
            }
        }
    }

    private fun scaleDownViewVertical() {
        val mid = height / 2.0f
        val unitScaleDownDist = scaleDownDistance * mid
        for (i in 0 until childCount) {
            val child = requireChild(i)
            val childMid = (getDecoratedTop(child) + getDecoratedBottom(child)) / 2.0f
            val scale =
                1.0f + -1 * scaleDownBy * min(unitScaleDownDist, abs(mid - childMid)) / unitScaleDownDist
            child.scaleX = scale
            child.scaleY = scale
            if (isChangeAlpha) {
                child.alpha = scale
            }
        }
    }

    private fun requireChild(position: Int) = checkNotNull(getChildAt(position)) { "No view at position: $position" }
}
