package org.karbo.launch.login

import android.view.*
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.greennick.properties.android.BoundAdapter
import com.github.greennick.properties.android.onClick
import org.karbo.launch.R
import org.karbo.wallet.models.WalletFile

class WalletAdapter(private val positionClicked: (Int) -> Unit) :
    RecyclerView.Adapter<ViewHolder>(), BoundAdapter<WalletFile> {

    private var wallets = emptyList<WalletFile>()

    override fun update(newItems: List<WalletFile>) {
        wallets = newItems
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_wallet, parent, false)
            .run(::ViewHolder)

    override fun getItemCount() = wallets.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = wallets[position].name
        holder.name.onClick { positionClicked(position) }
    }
}

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val name: TextView = itemView.findViewById(R.id.wallet_name)
}
