package org.karbo.launch.data.wallet

import androidx.annotation.VisibleForTesting
import org.jetbrains.annotations.TestOnly
import org.karbo.app.utils.log
import org.karbo.wallet.models.WalletFile
import java.io.File

class WalletRestoreAction(wallet: WalletFile) {

    private val walletDir = File(wallet.dir)
    private val walletName = wallet.name

    fun canRestore(): Boolean {
        val restoreSrcTpFile = restoreSrcTmpFile()
        val walletFile = walletFile
        if (walletFile.exists()) {
            log { "wallet exists" }
            if (walletFile.length() == 0L && restoreSrcTpFile != null) {
                log { "$walletFile isEmpty. Should be marked for restoration" }
                return true
            }
            log { "wallet has length > 0 or no restore src" }
        }
        log { "Wallet seems to be Ok. Restoration is not needed" }
        //does not exist so nothing to restore
        return false
    }

    @VisibleForTesting
    internal val walletFile
        get() = File(walletDir, "$walletName.wallet")

    @VisibleForTesting
    @TestOnly
    internal fun restoreSrcTmpFile(): File? {
        return walletDir.listFiles()
            .filter { it.name.contains("$walletName.wallet.tmp") }.maxBy(File::length)
    }

    operator fun invoke() {
        restore()
    }

    @VisibleForTesting
    @TestOnly
    internal fun restore(): Boolean {
        if (canRestore()) {
            val srcFile = restoreSrcTmpFile()
            if (srcFile != null) {
                val deleted = walletFile.delete()
                val renamed = srcFile.renameTo(walletFile)
                return deleted && renamed
            } else {
                log { "No restoration src file" }
            }
        } else {
            log { "Restoring $walletName wallet which cant be restored" }
        }
        return false
    }
}
