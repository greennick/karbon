package org.karbo.launch.data

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager

interface NetworkManager {
    val networkAvailable: Boolean
}

class NetworkManagerImpl(context: Context) : NetworkManager {
    private val connectivityService = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

    override val networkAvailable
        @SuppressLint("MissingPermission")
        get() = connectivityService
            ?.activeNetworkInfo
            ?.isConnected
            ?: false
}
