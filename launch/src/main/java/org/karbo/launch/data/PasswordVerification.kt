package org.karbo.launch.data

import com.greennick.result.Result
import org.karbo.app.data.managers.StringsRepo
import org.karbo.launch.R

/*
 * Created by greennick on 13.02.2018.
 */
sealed class BadPassword {
    object EmptyPassword : BadPassword()

    object TooShort : BadPassword()

    object DeniedSymbols : BadPassword() {
        val symbols = charArrayOf('#', ' ') //denied symbols in password string
    }
}

fun verifyPassword(password: String): Result<String, BadPassword> = when {
    password.isEmpty() -> Result.error(BadPassword.EmptyPassword)
    password.length < 4 -> Result.error(BadPassword.TooShort)
    password.containsDeniedSymbols() -> Result.error(BadPassword.DeniedSymbols)
    else -> Result.success(password)
}

private fun String.containsDeniedSymbols() = BadPassword.DeniedSymbols.symbols
    .any { denied -> denied in this }

fun deniedSymbolsErrorMessage(repo: StringsRepo): String {
    val error = StringBuilder(repo.string(R.string.error_password_denied_symbols))
    BadPassword.DeniedSymbols.symbols.forEach {
        error.append(if (it == ' ') repo.string(R.string.error_password_denied_symbol_space) else "'$it'")
        error.append(", ")
    }
    return error.dropLast(2).toString()
}
