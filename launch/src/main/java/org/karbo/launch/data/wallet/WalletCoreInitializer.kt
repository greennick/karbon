package org.karbo.launch.data.wallet

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.karbo.app.utils.log
import org.karbo.launch.BuildConfig
import java.io.File
import java.io.FileOutputStream

private const val CORE_NAME_BASE = "wallet_core"
private const val CORE_NAME_EXTENSION = ".core"

class WalletCoreInitializer(private val context: Context) {
    private val workDir = File(context.applicationInfo.dataDir, "core")
    val coreFile = File(workDir, "$CORE_NAME_BASE-${BuildConfig.VERSION_CODE}$CORE_NAME_EXTENSION")

    suspend fun init() {
        if (!coreFile.exists() || coreFile.length() == 0L) {
            log { "core doesn't exists" }
            withContext(Dispatchers.IO) {
                clearDir()
                createCore()
            }
        } else {
            log { "core exists" }
        }
    }

    private fun clearDir() {
        if (!workDir.exists()) {
            workDir.mkdir()
            log { "create work dir" }
        }

        workDir.listFiles { file, name ->
            log { "file $name founded" }
            file.length() > 0 && name.startsWith(CORE_NAME_BASE) && name.endsWith(CORE_NAME_EXTENSION)
        }.forEach { it.delete() }
    }

    private fun createCore() {
        context.assets.open(CORE_NAME_BASE).use { assets ->
            FileOutputStream(coreFile).use { file ->
                val size = assets.copyTo(file)
                log { "core copied from assets, size: $size, exists: ${coreFile.exists()}" }
            }
        }
        val result = coreFile.setExecutable(true)
        log { "now executable: $result" }
    }
}
