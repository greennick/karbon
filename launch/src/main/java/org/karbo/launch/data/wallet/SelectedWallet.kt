package org.karbo.launch.data.wallet

import android.annotation.SuppressLint
import android.content.SharedPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface SelectedWallet {

    suspend fun selectedWalletName(): String

    suspend fun saveSelectedWalletName(name: String)
}

private const val SELECTED_WALLET = "SELECTED_WALLET"

class SelectedWalletImpl(private val sharedPreferences: SharedPreferences) : SelectedWallet {

    override suspend fun selectedWalletName() = withContext(Dispatchers.IO) {
        sharedPreferences.getString(SELECTED_WALLET, "") as String
    }

    @SuppressLint("ApplySharedPref")
    override suspend fun saveSelectedWalletName(name: String) = withContext<Unit>(Dispatchers.IO) {
        sharedPreferences.edit()
            .putString(SELECTED_WALLET, name)
            .commit()
    }
}
