package org.karbo.launch.data.wallet

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.karbo.wallet.models.WalletFile
import java.io.File

interface PersistenceWalletManager {

    suspend fun getWallets(): List<WalletFile>

    suspend fun hasWallets(): Boolean

    suspend fun prepareWalletFolder()

    suspend fun restoreAll()

    fun removeWallet(wallet: WalletFile)
}

class PersistenceWalletManagerImpl(walletsFolderProvider: WalletsFoldersProvider) : PersistenceWalletManager {

    private val walletsFolder = walletsFolderProvider.walletsFolder

    override suspend fun getWallets(): List<WalletFile> = withContext(Dispatchers.IO) { scanForWallets() }

    override suspend fun hasWallets() = getWallets().isNotEmpty()

    override suspend fun prepareWalletFolder() = withContext(Dispatchers.IO) {
        createWalletFolder()
        clearWalletLogs()
    }

    override fun removeWallet(wallet: WalletFile) {
        File(wallet.dir).listFiles()
            .filter { it.nameWithoutExtension == wallet.name || it.nameWithoutExtension == wallet.name + ".wallet" /* $WalletName.wallet.address*/ }
            .forEach { it.delete() }
    }

    override suspend fun restoreAll() = withContext(Dispatchers.IO) {
        getWallets().map(::WalletRestoreAction)
            .filter(WalletRestoreAction::canRestore)
            .forEach(WalletRestoreAction::invoke)
    }

    private fun scanForWallets() = walletsFolder.listFiles()
        .filter { it.extension == "keys" || it.extension == "wallet" }
        .map { file -> WalletFile(walletsFolder.path, file.nameWithoutExtension, file.extension) }
        .sortedBy { it.name }

    private fun createWalletFolder() {
        if (!walletsFolder.exists()) walletsFolder.mkdirs()
    }

    private fun clearWalletLogs() {
        val log = File(walletsFolder, "wallet.log")
        if (log.exists()) log.delete()
    }
}
