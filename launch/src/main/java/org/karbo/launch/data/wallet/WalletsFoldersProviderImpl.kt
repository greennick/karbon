package org.karbo.launch.data.wallet

import android.content.Context
import android.os.Environment
import org.karbo.launch.R
import java.io.File

class WalletsFoldersProvider(context: Context) {
    val walletsFolder = File(
        Environment.getExternalStorageDirectory(),
        context.getString(R.string.WALLETS_FOLDER_NAME)
    )
}
