package org.karbo.launch.creation

import com.github.greennick.properties.emptyProperty
import com.github.greennick.properties.propertyOf
import com.greennick.result.onError
import com.greennick.result.onSuccess
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.data.managers.StringsRepo
import org.karbo.app.navigation.Router
import org.karbo.app.navigation.ScreensFactory
import org.karbo.app.utils.log
import org.karbo.launch.R
import org.karbo.launch.data.*
import org.karbo.launch.data.wallet.WalletsFoldersProvider
import org.karbo.wallet.models.WalletFile

private const val SEED_SIZE = 25

class CreateWalletViewModel(
    private val strings: StringsRepo,
    private val walletsFoldersProvider: WalletsFoldersProvider,
    private val router: Router,
    private val screens: ScreensFactory
) : BaseViewModel(router) {
    private val deniedNameChars = charArrayOf('#', '?', '"', '*', '|', '/', '\\', '<', '>')

    val name = propertyOf(strings.string(R.string.default_wallet_name))
    val password = propertyOf("")
    val confirmPassword = propertyOf("")
    val seed = propertyOf("")
    val seedChecked = propertyOf(false)
    val seedVisible = seedChecked
    val nameError = emptyProperty<Int>()
    val passwordError = emptyProperty<String>()
    val confirmPasswordError = emptyProperty<Int>()
    val seedError = emptyProperty<Int>()

    val filterWalletName = { input: CharSequence ->
        input.filterNot { it in deniedNameChars }
    }

    fun createClicked() {
        nameError.value = null
        passwordError.value = null
        confirmPasswordError.value = null
        seedError.value = null

        val name = this.name.value.trim()
        val password = this.password.value
        val confirmPassword = this.confirmPassword.value
        val seed = this.seed.value
        val hasSeed = seedChecked.value
        log { "create with name: $name, password: $password, use seed: $hasSeed, seed: $seed" }

        when {
            name.isEmpty() -> nameError.value = R.string.create_error_name_empty
            getWallet(name).exists -> nameError.value = R.string.create_error_wallet_exists
            hasSeed && seed.wordsCount() != SEED_SIZE -> seedError.value = R.string.create_error_seed_wrong
            confirmPassword != password -> confirmPasswordError.value = R.string.create_error_confirm_password
            else -> verifyPassword(password)
                .onSuccess { createWallet(name, it, hasSeed, seed) }
                .onError {
                    passwordError.value = when (it) {
                        BadPassword.EmptyPassword -> strings.string(R.string.error_password_empty)
                        BadPassword.TooShort -> strings.string(R.string.error_password_short)
                        BadPassword.DeniedSymbols -> deniedSymbolsErrorMessage(strings)
                    }
                }
        }
    }

    fun settingsClicked() {
        log { "settings" }
        router.navigateTo(screens.settings())
    }

    private fun String.wordsCount() = split(' ').size

    private fun createWallet(name: String, password: String, hasSeed: Boolean, seed: String) {
        val screen = if (hasSeed) {
            screens.loading(getWallet(name), password, seed)
        } else {
            screens.loading(getWallet(name), password)
        }
        router.navigateTo(screen)
    }

    private fun getWallet(name: String) = WalletFile(walletsFoldersProvider.walletsFolder.path, name)
}
