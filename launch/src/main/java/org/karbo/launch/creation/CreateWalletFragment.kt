package org.karbo.launch.creation

import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.view.WindowManager
import com.github.greennick.properties.android.onClick
import com.github.greennick.properties.androidx.*
import kotlinx.android.synthetic.main.fragment_create_wallet.*
import org.karbo.app.arch.views.BaseFragment
import org.karbo.launch.R

class CreateWalletFragment(
    override val viewModel: CreateWalletViewModel
) : BaseFragment(R.layout.fragment_create_wallet) {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(viewModel) {

            button_create_wallet.onClick(::createClicked)
            button_settings.onClick(::settingsClicked)

            bindTextBidirectionally(field_name, name)

            bindTextBidirectionally(field_password, password)
            bindTextBidirectionally(field_password_confirm, confirmPassword)
            bindTextBidirectionally(field_seed, seed)

            bindCheckedBidirectionally(check_box_import_seed, seedChecked)
            bindVisibility(field_seed_wrapper, seedVisible)

            bindError(field_password_wrapper, passwordError)
            bindError(field_name_wrapper, nameError)
            bindError(field_password_confirm_wrapper, confirmPasswordError)
            bindError(field_seed_wrapper, seedError)

            field_name.filters = arrayOf(
                InputFilter.LengthFilter(25),
                InputFilter { input, _, _, _, _, _ -> filterWalletName(input) }
            )
        }

        field_name_wrapper.requestFocus()
    }
}
