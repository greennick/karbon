package org.karbo.launch.loading

import com.github.greennick.properties.propertyOf
import com.greennick.result.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.data.ConnectedNodeHolder
import org.karbo.app.data.analytics.Analytics
import org.karbo.app.data.managers.LinkAction
import org.karbo.app.data.managers.ReceivedLinkHandler
import org.karbo.app.data.passwords.PasswordHolder
import org.karbo.app.navigation.Router
import org.karbo.app.navigation.ScreensFactory
import org.karbo.app.utils.log
import org.karbo.app.utils.loge
import org.karbo.base.required
import org.karbo.launch.R
import org.karbo.launch.data.NetworkManager
import org.karbo.launch.data.wallet.PersistenceWalletManager
import org.karbo.node.NodeProvider
import org.karbo.node.models.Node
import org.karbo.wallet.*
import org.karbo.wallet.models.WalletFile

class LoadingViewModel(
    private val wallet: WalletFile,
    private val password: String,
    private val seed: String,
    private val networkManager: NetworkManager,
    private val walletLoader: WalletLoader,
    private val nodeProvider: NodeProvider,
    private val analytics: Analytics,
    private val persistence: PersistenceWalletManager,
    private val passwordHolder: PasswordHolder,
    private val receivedLink: ReceivedLinkHandler,
    private val router: Router,
    private val screens: ScreensFactory,
    private val connectedNodeHolder: ConnectedNodeHolder
) : BaseViewModel(router) {
    val showNetworkError = propertyOf(false)

    init {
        passwordHolder.reset()
    }

    override fun backPressed() {}

    override fun screenOpened() = tryConnect()

    fun tryConnect() {
        log { "tryConnect" }
        showNetworkError.value = false
        if (networkManager.networkAvailable) {
            loadNode()
        } else {
            // workaround for dialogs that we don't want to be dismissed
            launch {
                delay(100)
                showNetworkError.value = true
            }
        }
    }

    fun close() {
        log { "close" }
        router.exit()
    }

    private fun loadNode() {
        log { "loadNode" }
        launch {
            val node = nodeProvider.getNode()
            if (node != null) {
                checkWalletFile()
                processWallet(node)
            } else {
                criticalError(R.string.error_node_not_found)
            }
        }
    }

    private suspend fun checkWalletFile() {
        persistence.prepareWalletFolder()
        persistence.restoreAll()
    }

    private suspend fun processWallet(node: Node) {
        log { "process wallet $node" }
        when {
            wallet.exists -> initWallet(node, false)
            seed.isNotEmpty() -> restoreWallet(node)
            else -> createWallet(node)
        }
    }

    private suspend fun initWallet(node: Node, firstLaunch: Boolean = false) {
        log { "initWallet $node, $firstLaunch" }
        walletLoader.init(node.url, wallet, password)
            .onSuccess {
                analytics.connectedToNode(node)
                connectedNodeHolder.node = node
                passwordHolder.save(password)
                val linkAction = receivedLink.consumeSavedLink()
                router.replaceScreen(screens.main(wallet))
                when {
                    firstLaunch -> screens.mnemonicSeed(true)
                    linkAction is LinkAction.Send -> screens.sending(linkAction.request)
                    linkAction is LinkAction.ScanQr -> screens.qrScanner()
                    linkAction is LinkAction.Receive -> screens.receiving()
                    else -> null
                }?.apply(router::navigateTo)
            }.onError { processInitErrors(it) }
    }

    private suspend fun restoreWallet(node: Node) {
        log { "restoreWallet $node" }
        walletLoader.restore(node.url, wallet, password, seed)
            .doIfSuccess {
                analytics.restoredByMnemonic()
                initWallet(node, true)
            }.onError {
                when (it) {
                    RestoreError.InvalidRecoveryPhrase -> criticalError(R.string.error_wrong_seed)
                    RestoreError.NoNodeConnection -> {
                        router.showMessage(R.string.status_node_reconnection)
                        nodeProvider.clearCache()
                        tryConnect()
                    }
                    RestoreError.CoreNotFound -> Unit // skip
                }.required
            }
    }

    private suspend fun createWallet(node: Node) {
        log { "createWallet $node" }
        walletLoader.create(node.url, wallet, password)
            .doIfSuccess { initWallet(node, true) }
            .doIfError { criticalError(R.string.error_generic) }
    }

    private suspend fun processInitErrors(error: InitError) {
        loge { "error connecting: $error" }
        when (error) {
            InitError.AlreadyRunning -> {
                walletLoader.stop()
                loadNode()
            }
            InitError.NoNodeConnection -> {
                router.showMessage(R.string.status_node_reconnection)
                nodeProvider.clearCache()
                tryConnect()
            }
            InitError.InvalidPassword -> {
                router.showMessage(R.string.error_wrong_password)
                router.exit()
            }
            InitError.WalletNotFound, InitError.CoreNotFound -> criticalError(R.string.error_generic)
        }.required
    }

    private fun criticalError(messageId: Int) {
        log { "criticalError" }
        router.showMessage(messageId)
        router.exit()
    }
}
