package org.karbo.launch.loading

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import com.github.greennick.properties.androidx.bindVisibility
import kotlinx.android.synthetic.main.fragment_loading.*
import org.karbo.app.arch.views.BaseFragment
import org.karbo.app.utils.alertDialog
import org.karbo.launch.R
import org.karbo.wallet.models.WalletFile
import org.koin.core.parameter.parametersOf

class LoadingFragment : BaseFragment(R.layout.fragment_loading) {

    override val viewModel by scope.inject<LoadingViewModel> {
        parametersOf(
            requireArguments().getSerializable(KEY_WALLET) as WalletFile,
            requireArguments().getString(KEY_PASSWORD),
            requireArguments().getString(KEY_MNEMONIC_SEED)
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        requireActivity().setTheme(R.style.AppTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val rotation = AnimationUtils.loadAnimation(context, R.anim.rotation_clockwise)
        logo.startAnimation(rotation)

        with(viewModel) {
            val dialog = alertDialog {
                cancelable = false

                titleId = R.string.error_no_network_title
                messageId = R.string.error_no_network_message

                positive(R.string.error_no_network_try_again, ::tryConnect)
                negative(R.string.error_no_network_exit, ::close)

            }
            bindVisibility(dialog, showNetworkError)
        }
    }

    companion object {
        const val KEY_WALLET = "KEY_WALLET"
        const val KEY_PASSWORD = "KEY_PASSWORD"
        const val KEY_MNEMONIC_SEED = "KEY_MNEMONIC_SEED"

        operator fun invoke(wallet: WalletFile, password: String, mnemonicSeed: String = "") =
            LoadingFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(KEY_WALLET, wallet)
                    putString(KEY_PASSWORD, password)
                    putString(KEY_MNEMONIC_SEED, mnemonicSeed)
                }
            }
    }
}
