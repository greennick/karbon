package org.karbo.launch

import com.github.greennick.properties.generic.subscribeOnTrue
import com.github.greennick.properties.generic.zipWith
import com.github.greennick.properties.propertyOf
import kotlinx.coroutines.launch
import org.karbo.app.arch.BaseViewModel
import org.karbo.app.navigation.*
import org.karbo.app.utils.log
import org.karbo.app.utils.loge
import org.karbo.launch.data.wallet.PersistenceWalletManager
import org.karbo.launch.data.wallet.WalletCoreInitializer

class LaunchViewModel(
    coreInitializer: WalletCoreInitializer,
    private val router: Router,
    private val screens: ScreensFactory,
    private val persistenceManager: PersistenceWalletManager
) : BaseViewModel(router) {
    val showRationale = propertyOf(true)
    val showDeniedForeverWarning = propertyOf(false)

    private val storageAllowed = propertyOf(false)
    private val coreInitialized = propertyOf(false)
    private val hasStorageAndCore = storageAllowed.zipWith(coreInitialized) { allowed, initialized ->
        allowed && initialized
    }

    init {
        log { "init call" }
        hasStorageAndCore.subscribeOnTrue(::prepareWallets)

        launch {
            coreInitializer.init()
            log { "core initialized" }
            coreInitialized.value = true
        }
    }

    fun requestingStorage() {
        log { "storage requesting" }
        showRationale.value = false
    }

    fun storageAllowed() {
        log { "storage allowed" }
        showRationale.value = false
        storageAllowed.value = true
    }

    fun storageDenied() {
        loge { "storage denied" }
        showRationale.value = false
        storageAllowed.value = false
        router.showMessage(R.string.permission_denied_storage_message, Duration.LONG)
        router.exit()
    }

    fun storageDeniedForever() {
        loge { "storage denied forever" }
        showRationale.value = false
        showDeniedForeverWarning.value = true
        storageAllowed.value = false
    }

    fun openAppSettings() {
        log { "open application settings" }
        router.navigateTo(screens.applicationSettings())
    }

    fun deniedForeverCanceled() {
        showDeniedForeverWarning.value = false
        router.showMessage(R.string.permission_denied_storage_message, Duration.LONG)
    }

    private fun prepareWallets() {
        log { "preparing wallets" }
        launch {
            try {
                val hasWallets = persistenceManager.run {
                    prepareWalletFolder()
                    restoreAll()
                    hasWallets()
                }
                processWalletsExisting(hasWallets)
            } catch (error: Throwable) {
                loge(error) { "error checking existing wallets" }
                router.exit()
            }
        }
    }

    private fun processWalletsExisting(hasWallets: Boolean) {
        log { "has wallets: $hasWallets" }
        if (hasWallets) {
            router.replaceScreen(screens.login())
        } else {
            router.replaceScreen(screens.walletCreation())
        }
    }
}
