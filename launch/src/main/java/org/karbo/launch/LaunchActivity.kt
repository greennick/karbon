package org.karbo.launch

import android.os.Bundle
import android.widget.FrameLayout
import com.github.greennick.properties.androidx.bindVisibility
import org.karbo.app.arch.views.BaseActivity
import org.karbo.app.utils.*

class LaunchActivity : BaseActivity() {

    override val viewModel: LaunchViewModel by inject()
    override val containerId = 1001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(FrameLayout(this).apply {
            id = containerId
        })

        with(viewModel) {
            alertDialog {
                titleId = R.string.permission_rationale_title
                messageId = R.string.permission_rationale_storage_message

                positive(R.string.permission_rationale_allow) {
                    requestStorage()
                    viewModel.requestingStorage()
                }
                negative(R.string.permission_rationale_deny, viewModel::storageDenied)

                cancelable = false
            }.let { bindVisibility(it, showRationale) }

            alertDialog {
                titleId = R.string.permission_denied_forewer_title
                messageId = R.string.permission_denied_forewer_message

                positive(R.string.permission_denied_forewer_settings, viewModel::openAppSettings)
                negative(R.string.permission_rationale_deny, viewModel::deniedForeverCanceled)

                cancelable = false
            }.let { bindVisibility(it, showDeniedForeverWarning) }
        }
    }

    override fun onStart() {
        super.onStart()
        if (hasStoragePermission) {
            viewModel.storageAllowed()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        with(viewModel) {
            when {
                hasStoragePermission -> storageAllowed()
                storageDeniedForever -> storageDeniedForever()
                else -> storageDenied()
            }
        }
    }
}
