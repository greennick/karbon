package org.karbo.launch.data

import com.greennick.result.error
import com.greennick.result.get
import org.junit.Test

class PasswordVerificationTest {

    @Test
    fun `empty string causes EmptyPassword error`() {
        val result = verifyPassword("")
        assert(result.isFailure)
        assert(result.error() is BadPassword.EmptyPassword)
    }

    @Test
    fun `short string causes TooShort error`() {
        val result = verifyPassword("123")
        assert(result.isFailure)
        assert(result.error() is BadPassword.TooShort)
    }

    @Test
    fun `string with spaces causes DeniedSymbols error`() {
        val result = verifyPassword("invalidPassword with spaces")
        assert(result.isFailure)
        assert(result.error() is BadPassword.DeniedSymbols)
    }

    @Test
    fun `valid password pass verification`() {
        val password = "somValidPassword"
        val result = verifyPassword(password)
        assert(result.isSuccess)
        assert(result.get() == password)
    }
}
